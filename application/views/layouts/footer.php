    <!-- /.content-wrapper -->
    <footer class="main-footer">
            <div class="pull-right hidden-xs">
                <b>Version</b> 2.0.0
            </div>
            <strong><?php echo date("Y"); ?> &copy; <a href="http://colegionocturnosalamanca.com" target="_blank"><?php echo $nombreInstitucion; ?></a>.</strong> Todos los derechos reservados.
        </footer>
    </div>
    <!-- ./wrapper -->
<!-- jQuery 3 -->
<script src="<?php echo base_url();?>assets/template/jquery/jquery.min.js"></script>
<!-- Alertify -->
<script src="<?php echo base_url(); ?>assets/template/alertify/lib/alertify.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url();?>assets/template/bootstrap/js/bootstrap.min.js"></script>
<!-- Sweetalert -->
<script src="<?php echo base_url(); ?>assets/template/sweetalert/dist/sweetalert.js"></script>
<!-- jquery-ui -->
<script src="<?php echo base_url();?>assets/template/jquery-ui/jquery-ui.js"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url();?>assets/template/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url();?>assets/template/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url();?>assets/template/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url();?>assets/template/dist/js/demo.js"></script>
<!-- plotly -->
<script src="<?php echo base_url(); ?>js/plotly-latest.min.js"></script>    

<?php if($this->uri->segment(1)=='periodos_lectivos') { ?>
    <script src="<?php echo base_url();?>js/periodos_lectivos.js"></script>
<?php } ?>
<?php if($this->uri->segment(1)=='perfiles') { ?>
    <script src="<?php echo base_url();?>js/perfiles.js"></script>
<?php } ?>
<?php if($this->uri->segment(1)=='menus') { ?>
    <script src="<?php echo base_url();?>js/menus.js"></script>
<?php } ?>
<?php if($this->uri->segment(1)=='usuarios') { ?>
    <script src="<?php echo base_url();?>js/usuarios.js"></script>
<?php } ?>
<?php if($this->uri->segment(1)=='niveles_educacion') { ?>
    <script src="<?php echo base_url();?>js/niveles_educacion.js"></script>
<?php } ?>
<?php if($this->uri->segment(1)=='especialidades') { ?>
    <script src="<?php echo base_url();?>js/especialidades.js"></script>
<?php } ?>
<?php if($this->uri->segment(1)=='cursos') { ?>
    <script src="<?php echo base_url();?>js/cursos.js"></script>
<?php } ?>
<?php if($this->uri->segment(1)=='paralelos') { ?>
    <script src="<?php echo base_url();?>js/paralelos.js"></script>
<?php } ?>
<?php if($this->uri->segment(1)=='areas') { ?>
    <script src="<?php echo base_url();?>js/areas.js"></script>
<?php } ?>
<?php if($this->uri->segment(1)=='asignaturas') { ?>
    <script src="<?php echo base_url();?>js/asignaturas.js"></script>
<?php } ?>
<?php if($this->uri->segment(1)=='periodos_evaluacion') { ?>
    <script src="<?php echo base_url();?>js/periodos_evaluacion.js"></script>
<?php } ?>
<?php if($this->uri->segment(1)=='aportes_evaluacion') { ?>
    <script src="<?php echo base_url();?>js/aportes_evaluacion.js"></script>
<?php } ?>
<?php if($this->uri->segment(1)=='rubricas_evaluacion') { ?>
    <script src="<?php echo base_url();?>js/rubricas_evaluacion.js"></script>
<?php } ?>
<script>
$(document).ready(function () {
    $('.sidebar-menu').tree();
    <?php if($this->uri->segment(1)=='periodos_lectivos') { ?>
        showAllPeriodosLectivos();
    <?php } ?>
    <?php if($this->uri->segment(1)=='perfiles') { ?>
        showAllPerfiles();
    <?php } ?>
    <?php if($this->uri->segment(1)=='menus') { ?>
        $("#tbody_menus").html("<tr><td colspan='4' align='center'>Debe seleccionar un perfil...</td></tr>");
    <?php } ?>
    <?php if($this->uri->segment(1)=='usuarios') { ?>
        $("#tbody_usuarios").html("<tr><td colspan='6' align='center'>Debe seleccionar un perfil...</td></tr>");
    <?php } ?>
    <?php if($this->uri->segment(1)=='niveles_educacion') { ?>
        showAllNiveles();
    <?php } ?>
    <?php if($this->uri->segment(1)=='especialidades') { ?>
        showEspecialidadesPeriodoLectivo();
    <?php } ?>
    <?php if($this->uri->segment(1)=='cursos') { ?>
        showCursosPeriodoLectivo();
    <?php } ?>
    <?php if($this->uri->segment(1)=='paralelos') { ?>
        showParalelosPeriodoLectivo();
    <?php } ?>
    <?php if($this->uri->segment(1)=='areas') { ?>
        showAllAreas();
    <?php } ?>
    <?php if($this->uri->segment(1)=='asignaturas') { ?>
        showAllAsignaturas();
    <?php } ?>
    <?php if($this->uri->segment(1)=='periodos_evaluacion') { ?>
        showPeriodosEvaluacion();
    <?php } ?>
    <?php if($this->uri->segment(1)=='aportes_evaluacion') { ?>
        $("#tbody_aportes_evaluacion").html("<tr><td colspan='4' align='center'>Debe seleccionar un periodo de evaluación...</td></tr>");
    <?php } ?>
    <?php if($this->uri->segment(1)=='rubricas_evaluacion') { ?>
        $("#tbody_rubricas_evaluacion").html("<tr><td colspan='4' align='center'>Debe seleccionar un periodo de evaluación...</td></tr>");
        $("#cboAportesEvaluacion").prop("disabled", true);
        $("#cboTiposAsignatura").prop("disabled", true);
        $("#btn-save").prop("disabled",true);
        $("#btn-cancel").prop("disabled",true);
    <?php } ?>

    $("#pe_fecha_inicio").datepicker({
        dateFormat : 'yy-mm-dd',
        firstDay: 1
    });
    $("#ap_fecha_apertura").datepicker({
        dateFormat : 'yy-mm-dd',
        firstDay: 1
    });
    $("#ap_fecha_cierre").datepicker({
        dateFormat : 'yy-mm-dd',
        firstDay: 1
    });
});
</script>
</body>
</html>