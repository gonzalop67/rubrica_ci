        <!-- =============================================== -->

        <!-- Left side column. contains the sidebar -->
        <aside class="main-sidebar">
        <section class="sidebar">
            <!-- Sidebar user panel -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="<?php echo base_url()?>assets/template/dist/img/<?php echo $this->session->userdata("foto") ?>" class="img-circle" alt="User Image">
                </div>
                <div class="pull-left info">
                    <p><?php echo $this->session->userdata("nombre") ?></p>
                    <span style="font-size: 0.9em"><?php echo $this->session->userdata("rol_name") ?></span>
                </div>
            </div>
            <!-- sidebar: style can be found in sidebar.less -->
            <section class="sidebar">
                <!-- sidebar menu: : style can be found in sidebar.less -->
                <ul class="sidebar-menu" data-widget="tree">
                    <li class="header">MENU</li>
                    <li>
                        <a href="<?php echo base_url();?>dashboard">
                            <i class="fa fa-home"></i> <span>Inicio</span>
                        </a>
                    </li>
                    <?php 
                        $menusNivel1 = $this->Menus_model->listarMenusNivel1($this->session->userdata("id_perfil")); 
                        foreach($menusNivel1 as $menu1) {
                    ?>
                        <?php if(count($this->Menus_model->listarMenusHijos($menu1->id_menu)) > 0) { ?>
                            <li class="treeview">                        
                                <a href="#">
                                    <i class="fa fa-laptop"></i> <span><?php echo $menu1->mnu_texto; ?></span>
                                    <span class="pull-right-container">
                                        <i class="fa fa-angle-left pull-right"></i>
                                    </span>
                                </a>
                                <ul class="treeview-menu">
                                    <?php foreach($this->Menus_model->listarMenusHijos($menu1->id_menu) as $menu2) { ?>
                                        <li><a href="<?php echo base_url() . $menu2->mnu_enlace; ?>"><i class="fa fa-circle-o"></i> <?php echo $menu2->mnu_texto; ?></a></li>
                                    <?php } ?>
                                </ul>
                            </li>
                        <?php }else{ ?>
                            <li class="treeview">                        
                                <a href="<?php echo $menu1->mnu_enlace; ?>">
                                    <i class="fa fa-laptop"></i> <span><?php echo $menu1->mnu_texto; ?></span>
                                </a>
                            </li>
                        <?php } ?>
                    <?php } ?>
                </ul>
            </section>
            <!-- /.sidebar -->
        </aside>

        <!-- =============================================== -->
