
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1 id="titulo_principal">
        Niveles de educación
        </h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="box box-solid">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-8 table-responsive">
                        <input type="hidden" id="base_url" value="<?php echo base_url(); ?>">
                        <hr>
                        <table id="example1" class="table table-hover">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Nombre</th>
                                    <th>¿Es Bachillerato?</th>
                                    <th colspan="4">Acciones</th>
                                </tr>
                            </thead>
                            <tbody id="tbody_niveles_educacion">
                                <!-- En este lugar se van a poblar los niveles de educación mediante AJAX -->
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-4">
                        <div class="panel panel-success">
                            <div id="titulo" class="panel-heading">Nuevo Nivel de Educación</div>
                        </div>
                        <div class="panel-body">
                            <form id="frm-nivel-educacion" action="" method="post">
                                <input type="hidden" name="id_tipo_educacion" id="id_tipo_educacion" value="0">
                                <div class="form-group">
                                    <label for="te_nombre">Nombre:</label>
                                    <input type="text" name="te_nombre" id="te_nombre" class="form-control" autofocus>
                                </div>
                                <div class="form-group">
                                    <label for="te_bachillerato">¿Es Bachillerato?</label>
                                    <select name="te_bachillerato" id="te_bachillerato" class="form-control">
                                        <option value="1">Sí</option>
                                        <option value="0">No</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <button id="btn-save" type="submit" class="btn btn-success">Guardar</button>
                                    <button id="btn-cancel" type="button" class="btn btn-info">Cancelar</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div id="btn-regresar" class="col-md-12">
                        
                    </div>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
