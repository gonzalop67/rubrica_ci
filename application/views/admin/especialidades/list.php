
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1 id="titulo_principal">
        Especialidades
        </h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="box box-solid">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-8 table-responsive">
                        <input type="hidden" id="base_url" value="<?php echo base_url(); ?>">
                        <hr>
                        <table id="example1" class="table table-hover">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Nombre</th>
                                    <th>Figura</th>
                                    <th><!-- Acciones --></th>
                                </tr>
                            </thead>
                            <tbody id="tbody_especialidades">
                                <!-- En este lugar se van a poblar las especialidades mediante AJAX -->
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-4">
                        <div class="panel panel-success">
                            <div id="titulo" class="panel-heading">Nueva Especialidad</div>
                        </div>
                        <div class="panel-body">
                            <form id="frm-especialidad" action="" method="post">
                                <input type="hidden" name="id_menu" id="id_especialidad" value="0">
                                <div class="form-group">
                                    <label for="es_nombre">Nombre:</label>
                                    <input type="text" name="es_nombre" id="es_nombre" class="form-control" autofocus>
                                </div>
                                <div class="form-group">
                                    <label for="es_figura">Figura Profesional:</label>
                                    <input type="text" name="es_figura" id="es_figura" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="es_abreviatura">Abreviatura:</label>
                                    <input type="text" name="es_abreviatura" id="es_abreviatura" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="cboNivelesEducacion">Nivel de Educación:</label>
                                    <select name="cboNivelesEducacion" id="cboNivelesEducacion" class="form-control">
                                        <option value="0">Seleccione...</option>
                                        <?php foreach ($niveles_educacion as $nivel_educacion): ?>
                                            <option value="<?php echo $nivel_educacion->id_tipo_educacion ?>">
                                                <?php echo $nivel_educacion->te_nombre; ?>
                                            </option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <button id="btn-save" type="submit" class="btn btn-success">Guardar</button>
                                    <button id="btn-cancel" type="button" class="btn btn-info">Cancelar</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <hr>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

