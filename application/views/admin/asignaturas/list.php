
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1 id="titulo_principal">
        Asignaturas
        </h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="box box-solid">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-9 table-responsive">
                        <input type="hidden" id="base_url" value="<?php echo base_url(); ?>">
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon" style="background-color: #ddd;">Buscar</span>
                                <input type="text" name="search_text" id="search_text" placeholder="Buscar por Nombre de Asignatura" class="form-control text-uppercase" />
                            </div>
                        </div>
                        <hr>
                        <table id="example1" class="table table-hover">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Area</th>
                                    <th>Nombre</th>
                                    <th>Abreviatura</th>
                                    <th><!-- Acciones --></th>
                                </tr>
                            </thead>
                            <tbody id="tbody_asignaturas">
                                <!-- En este lugar se van a poblar las asignaturas mediante AJAX -->
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-3">
                        <div class="panel panel-success">
                            <div id="titulo" class="panel-heading">Nueva Asignatura</div>
                        </div>
                        <div class="panel-body">
                            <form id="frm-asignatura" action="" method="post">
                                <input type="hidden" name="id_menu" id="id_asignatura" value="0">
                                <div class="form-group">
                                    <label for="as_nombre">Nombre:</label>
                                    <input type="text" name="as_nombre" id="as_nombre" class="form-control" autofocus>
                                </div>
                                <div class="form-group">
                                    <label for="as_abreviatura">Abreviatura:</label>
                                    <input type="text" name="as_abreviatura" id="as_abreviatura" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="cboAreas">Area:</label>
                                    <select name="cboAreas" id="cboAreas" class="form-control">
                                        <option value="0">Seleccione...</option>
                                        <?php foreach ($areas as $area): ?>
                                            <option value="<?php echo $area->id_area ?>">
                                                <?php echo $area->ar_nombre; ?>
                                            </option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="cboTipos">Tipo:</label>
                                    <select name="cboTipos" id="cboTipos" class="form-control">
                                        <option value="0">Seleccione...</option>
                                        <?php foreach ($tipos as $tipo): ?>
                                            <option value="<?php echo $tipo->id_tipo_asignatura ?>">
                                                <?php echo $tipo->ta_descripcion; ?>
                                            </option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <button id="btn-save" type="submit" class="btn btn-success">Guardar</button>
                                    <button id="btn-cancel" type="button" class="btn btn-info">Cancelar</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <hr>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

