
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1 id="titulo_principal">
        Usuarios
        </h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="box box-solid">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-7 table-responsive">
                        <!-- form start -->
                        <form id="frm-asociar" class="form-horizontal">
                            <div class="box-body">
                                <input type="hidden" id="base_url" value="<?php echo base_url(); ?>">
                                <div class="form-group">
                                    <label for="cboPerfiles" class="col-sm-2 control-label">Perfil</label>
                                    <div class="col-sm-10">
                                        <select name="cboPerfiles" id="cboPerfiles" class="form-control">
                                            <option value="0">Seleccione un perfil...</option>
                                            <?php foreach ($perfiles as $perfil): ?>
                                                <option value="<?php echo $perfil->id_perfil ?>">
                                                    <?php echo $perfil->pe_nombre; ?>
                                                </option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="search_user" class="col-sm-2 control-label">Buscar</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="search_user" placeholder="Escriba el nombre del usuario a ser asociado...">
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer">
                                <button type="submit" class="btn btn-success pull-right">Asociar</button>
                            </div>
                            <!-- /.box-footer -->
                        </form>
                        <hr>
                        <table id="example1" class="table table-hover">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Id</th>
                                    <th>Usuario</th>
                                    <th>Activo</th>
                                    <th>Avatar</th>
                                    <th><!-- Opciones --></th>
                                </tr>
                            </thead>
                            <tbody id="tbody_usuarios">
                                <!-- En este lugar se van a poblar los usuarios mediante AJAX -->
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-5">
                        <div class="panel-body">
                            <!-- Horizontal Form -->
                            <div class="box box-info">
                                <div class="box-header with-border">
                                <h3 id="titulo" class="box-title">Nuevo Usuario</h3>
                                </div>
                                <!-- /.box-header -->
                                <input type="hidden" name="id_usuario" id="id_usuario" value="0">
                                <!-- form frm-usuario start -->
                                <form id="frm-usuario" class="form-horizontal">
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="us_titulo" class="col-sm-2 control-label">Título</label>

                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="us_titulo" placeholder="Por ejemplo: Ing.">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="us_apellidos" class="col-sm-2 control-label">Apellidos</label>

                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="us_apellidos">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="us_nombres" class="col-sm-2 control-label">Nombres</label>

                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="us_nombres">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="us_login" class="col-sm-2 control-label">Usuario</label>

                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="us_login">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="us_password" class="col-sm-2 control-label">Password</label>

                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="us_password">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="us_genero" class="col-sm-2 control-label">Género</label>

                                            <div class="col-sm-10">
                                                <select class="form-control" id="us_genero">
                                                    <option value="M">Masculino</option>
                                                    <option value="F">Femenino</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="us_activo" class="col-sm-2 control-label">Activo</label>

                                            <div class="col-sm-10">
                                                <select class="form-control" id="us_activo">
                                                    <option value="1">Sí</option>
                                                    <option value="0">No</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.box-body -->
                                    <div class="box-footer">
                                        <button id="btn-cancel" type="button" class="btn btn-default">Cancelar</button>
                                        <button id="btn-upload-image" type="button" class="btn btn-primary hide">Cambiar Imagen</button>
                                        <button id="btn-save" type="submit" class="btn btn-success pull-right">Guardar</button>
                                    </div>
                                    <!-- /.box-footer -->
                                </form>
                                <!-- form frm-usuario end -->
                                <!-- form frm-upload-image start -->
                                <form id="frm-upload-image" class="form-horizontal hide" enctype="multipart/form-data">
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="" class="col-sm-2 control-label">Avatar</label>

                                            <div class="col-sm-10">
                                                <img id="us_avatar" src="<?php echo base_url(); ?>assets/template/dist/img/default-avatar.png" height="100" width="100">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="us_foto" class="col-sm-2 control-label"></label>

                                            <div class="col-sm-10">
                                                <input type="file" name="us_foto" id="us_foto">
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.box-body -->
                                    <div class="box-footer">
                                        <button id="btn-cancel-upload" type="button" class="btn btn-default">Cancelar</button>
                                        <button id="btn-save-image" type="submit" class="btn btn-success pull-right">Cambiar</button>
                                    </div>
                                    <!-- /.box-footer -->
                                </form>
                                <!-- form frm-upload-image start -->
                            </div>
                            <!-- /.box -->
                        </div>
                    </div>
                </div>
                <hr>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
