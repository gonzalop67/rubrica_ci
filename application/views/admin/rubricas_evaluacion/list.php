
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1 id="titulo_principal">
        Aportes de Evaluación
        </h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="box box-solid">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-8 table-responsive">
                        <div class="form-group">
                            <input type="hidden" id="base_url" value="<?php echo base_url(); ?>">
                            <select name="cboPeriodosEvaluacion" id="cboPeriodosEvaluacion" class="form-control">
                                <option value="0">Seleccione un Periodo de Evaluación...</option>
                                <?php foreach ($periodos_evaluacion as $periodo_evaluacion): ?>
                                    <option value="<?php echo $periodo_evaluacion->id_periodo_evaluacion ?>">
                                        <?php echo $periodo_evaluacion->pe_nombre; ?>
                                    </option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <select name="cboAportesEvaluacion" id="cboAportesEvaluacion" class="form-control">
                                <option value="0">Seleccione un Aporte de Evaluación...</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <select name="cboTiposAsignatura" id="cboTiposAsignatura" class="form-control">
                                <option value="0">Seleccione un Tipo de Asignatura...</option>
                                <?php foreach ($tipos_asignatura as $tipo_asignatura): ?>
                                    <option value="<?php echo $tipo_asignatura->id_tipo_asignatura ?>">
                                        <?php echo $tipo_asignatura->ta_descripcion; ?>
                                    </option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <hr>
                        <table id="example1" class="table table-hover">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Nombre</th>
                                    <th>Abreviatura</th>
                                    <th><!-- Acciones --></th>
                                </tr>
                            </thead>
                            <tbody id="tbody_rubricas_evaluacion">
                                <!-- En este lugar se van a poblar las rubricas de evaluación mediante AJAX -->
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-4">
                        <div class="panel panel-success">
                            <div id="titulo" class="panel-heading">Nueva Rúbrica de Evaluación</div>
                        </div>
                        <div class="panel-body">
                            <form id="frm-rubrica-evaluacion" action="" method="post">
                                <input type="hidden" name="id_rubrica_evaluacion" id="id_rubrica_evaluacion" value="0">
                                <div class="form-group">
                                    <label for="ru_nombre">Nombre:</label>
                                    <input type="text" name="ru_nombre" id="ru_nombre" class="form-control" autofocus>
                                </div>
                                <div class="form-group">
                                    <label for="ru_abreviatura">Abreviatura:</label>
                                    <input type="text" name="ru_abreviatura" id="ru_abreviatura" class="form-control">
                                </div>
                                <div class="form-group">
                                    <button id="btn-save" type="submit" class="btn btn-success">Guardar</button>
                                    <button id="btn-cancel" type="button" class="btn btn-info">Cancelar</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <hr>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

