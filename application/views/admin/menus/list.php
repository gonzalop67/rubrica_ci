
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1 id="titulo_principal">
        Menús
        </h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="box box-solid">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-8 table-responsive">
                        <div class="form-group">
                            <input type="hidden" id="base_url" value="<?php echo base_url(); ?>">
                            <select name="cboPerfiles" id="cboPerfiles" class="form-control">
                                <option value="0">Seleccione un perfil...</option>
                                <?php foreach ($perfiles as $perfil): ?>
                                    <option value="<?php echo $perfil->id_perfil ?>">
                                        <?php echo $perfil->pe_nombre; ?>
                                    </option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <hr>
                        <table id="example1" class="table table-hover">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Texto</th>
                                    <th>Publicado</th>
                                    <th><!-- Acciones --></th>
                                </tr>
                            </thead>
                            <tbody id="tbody_menus">
                                <!-- En este lugar se van a poblar los menús mediante AJAX -->
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-4">
                        <div class="panel panel-success">
                            <div id="titulo" class="panel-heading">Nuevo Menu</div>
                        </div>
                        <div class="panel-body">
                            <form id="frm-menu" action="" method="post">
                                <input type="hidden" name="id_menu" id="id_menu" value="0">
                                <input type="hidden" name="mnu_padre" id="mnu_padre" value="0">
                                <input type="hidden" name="mnu_nivel" id="mnu_nivel" value="1">
                                <div class="form-group">
                                    <label for="mnu_texto">Texto:</label>
                                    <input type="text" name="mnu_texto" id="mnu_texto" class="form-control" autofocus>
                                </div>
                                <div class="form-group">
                                    <label for="mnu_enlace">Enlace:</label>
                                    <input type="text" name="mnu_enlace" id="mnu_enlace" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="mnu_publicado">Publicado:</label>
                                    <select name="mnu_publicado" id="mnu_publicado" class="form-control">
                                        <option value="1">Sí</option>
                                        <option value="0">No</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <button id="btn-save" type="submit" class="btn btn-success">Guardar</button>
                                    <button id="btn-cancel" type="button" class="btn btn-info">Cancelar</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div id="btn-regresar" class="col-md-12">
                        
                    </div>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

