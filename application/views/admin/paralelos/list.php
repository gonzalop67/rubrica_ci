
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1 id="titulo_principal">
        Paralelos
        </h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="box box-solid">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-8 table-responsive">
                        <input type="hidden" id="base_url" value="<?php echo base_url(); ?>">
                        <hr>
                        <table id="example1" class="table table-hover">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Especialidad</th>
                                    <th>Curso</th>
                                    <th>Nombre</th>
                                    <th><!-- Acciones --></th>
                                </tr>
                            </thead>
                            <tbody id="tbody_paralelos">
                                <!-- En este lugar se van a poblar los paralelos mediante AJAX -->
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-4">
                        <div class="panel panel-success">
                            <div id="titulo" class="panel-heading">Nuevo Paralelo</div>
                        </div>
                        <div class="panel-body">
                            <form id="frm-paralelo" action="" method="post">
                                <input type="hidden" name="id_paralelo" id="id_paralelo" value="0">
                                <div class="form-group">
                                    <label for="pa_nombre">Nombre:</label>
                                    <input type="text" name="pa_nombre" id="pa_nombre" class="form-control" autofocus>
                                </div>
                                <div class="form-group">
                                    <label for="cboCursos">Cursos:</label>
                                    <select name="cboCursos" id="cboCursos" class="form-control">
                                        <option value="0">Seleccione...</option>
                                        <?php foreach ($cursos as $curso): ?>
                                            <option value="<?php echo $curso->id_curso ?>">
                                                <?php echo "[".$curso->es_figura."] ".$curso->cu_nombre; ?>
                                            </option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <button id="btn-save" type="submit" class="btn btn-success">Guardar</button>
                                    <button id="btn-cancel" type="button" class="btn btn-info">Cancelar</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <hr>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

