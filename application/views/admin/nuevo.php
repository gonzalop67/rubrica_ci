<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>SIAE Web 2 | Log in</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  
  <!-- base_url() = http://localhost/ci_siae/-->

  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/template/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/template/font-awesome/css/font-awesome.min.css">

  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/template/dist/css/AdminLTE.min.css">

  <!-- Estilos propios de esta pagina -->
  <style type="text/css">
    .error {
      color: #F00;
      display: none;
    }
    .blanco {
      color: #FFF;
    }
  </style>

</head>
<body class="hold-transition login-page" style="background: url('<?php echo base_url();?>assets/images/loginFont.jpg')">
    <div class="login-box blanco">
        <div class="login-logo">
            <h2>S. I. A. E.</h2>
        </div>
        <!-- /.login-logo -->
        <div class="login-box-body">
            <p class="login-box-msg">Registro del Administrador</p>
            <?php if($this->session->flashdata("error")): ?>
              <div class="alert alert-danger">
                <p><?php echo $this->session->flashdata("error") ?></p>
              </div>
            <?php endif; ?>
            <form id="form-new-admin" action="<?php echo base_url(); ?>admin/registrar" method="post">
                <div class="form-group <?php echo !empty(form_error("usertitle"))?'has-error':'' ?>">
                    <label for="usertitle">Título:</label>
                    <input type="text" class="form-control" placeholder="Ingrese el Título del Usuario - ejemplo: Ing." id="usertitle" name="usertitle" value="<?php echo set_value("usertitle") ?>">
                    <?php echo form_error("usertitle","<span class='help-block'>","</span>") ?>
                </div>
                <div class="form-group <?php echo !empty(form_error("userlastname"))?'has-error':'' ?>">
                    <label for="userlastname">Apellidos:</label>
                    <input type="text" class="form-control" placeholder="Ingrese los Apellidos - ejemplo: Pérez Prado" id="userlastname" name="userlastname">
                    <?php echo form_error("userlastname","<span class='help-block'>","</span>") ?>
                </div>
                <div class="form-group <?php echo !empty(form_error("userfirstname"))?'has-error':'' ?>">
                    <label for="userfirstname">Nombres:</label>
                    <input type="text" class="form-control" placeholder="Ingrese los Nombres - ejemplo: Dámazo" id="userfirstname" name="userfirstname">
                    <?php echo form_error("userfirstname","<span class='help-block'>","</span>") ?>
                </div>
                <div class="form-group <?php echo !empty(form_error("password"))?'has-error':'' ?>">
                    <label for="password">Password:</label>
                    <input type="password" class="form-control" placeholder="Password" id="password" name="password">
                    <span class="form-control-feedback">
                      <img src="<?php echo base_url();?>assets/images/if_91_171450.png" height="16px" width="16px">
                    </span>
                    <?php echo form_error("password","<span class='help-block'>","</span>") ?>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <button type="submit" class="btn btn-danger btn-block btn-flat" id="btnEnviar">Registrar</button>
                    </div>
                </div>
            </form>
        </div>
        <!-- /.login-box-body -->
    </div>
    
	<footer style="text-align: center; color: white;">
		.: &copy; <?php echo date("  Y"); ?> - Unidad Educativa PCEI Fiscal Salamanca :.
	</footer>

	<!-- jQuery 3 -->
	<script src="<?php echo base_url();?>assets/template/jquery/jquery.min.js"></script>
	<!-- Bootstrap 3.3.7 -->
	<script src="<?php echo base_url();?>assets/template/bootstrap/js/bootstrap.min.js"></script>

  <script>
    $(document).ready(function(){
      document.getElementById("usertitle").value = "";
      document.getElementById("userlastname").value = "";
      document.getElementById("userfirstname").value = "";
      document.getElementById("password").value = "";
    });
  </script>
</body>
</html>