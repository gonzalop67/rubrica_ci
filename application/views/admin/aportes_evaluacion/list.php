
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1 id="titulo_principal">
        Aportes de Evaluación
        </h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="box box-solid">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-8 table-responsive">
                        <div class="form-group">
                            <input type="hidden" id="base_url" value="<?php echo base_url(); ?>">
                            <select name="cboPeriodosEvaluacion" id="cboPeriodosEvaluacion" class="form-control">
                                <option value="0">Seleccione un periodo de evaluación...</option>
                                <?php foreach ($periodos_evaluacion as $periodo_evaluacion): ?>
                                    <option value="<?php echo $periodo_evaluacion->id_periodo_evaluacion ?>">
                                        <?php echo $periodo_evaluacion->pe_nombre; ?>
                                    </option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <hr>
                        <table id="example1" class="table table-hover">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Nombre</th>
                                    <th>Abreviatura</th>
                                    <th><!-- Acciones --></th>
                                </tr>
                            </thead>
                            <tbody id="tbody_aportes_evaluacion">
                                <!-- En este lugar se van a poblar los aportes de evaluación mediante AJAX -->
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-4">
                        <div class="panel panel-success">
                            <div id="titulo" class="panel-heading">Nuevo Aporte de Evaluación</div>
                        </div>
                        <div class="panel-body">
                            <form id="frm-aporte-evaluacion" action="" method="post">
                                <input type="hidden" name="id_aporte_evaluacion" id="id_aporte_evaluacion" value="0">
                                <div class="form-group">
                                    <label for="ap_nombre">Nombre:</label>
                                    <input type="text" name="ap_nombre" id="ap_nombre" class="form-control" autofocus>
                                </div>
                                <div class="form-group">
                                    <label for="ap_abreviatura">Abreviatura:</label>
                                    <input type="text" name="ap_abreviatura" id="ap_abreviatura" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="ap_fecha_apertura">Fecha Inicial:</label>
                                    <div class="controls">
                                        <div class="input-group date">
                                            <input type="text" name="ap_fecha_apertura" id="ap_fecha_apertura" class="form-control">
                                            <label class="input-group-addon generic-btn" style="cursor: pointer;" onclick="$('#ap_fecha_apertura').focus();"><i class="fa fa-calendar" aria-hidden="true"></i></label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="ap_fecha_cierre">Fecha Final:</label>
                                    <div class="controls">
                                        <div class="input-group date">
                                            <input type="text" name="ap_fecha_cierre" id="ap_fecha_cierre" class="form-control">
                                            <label class="input-group-addon generic-btn" style="cursor: pointer;" onclick="$('#ap_fecha_cierre').focus();"><i class="fa fa-calendar" aria-hidden="true"></i></label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="mnu_publicado">Tipo de Aporte de Evaluación:</label>
                                    <select name="cboTiposAporteEvaluacion" id="cboTiposAporteEvaluacion" class="form-control">
                                        <option value="0">Seleccione un tipo de aporte...</option>
                                        <?php foreach ($tipos_aporte as $tipo_aporte): ?>
                                            <option value="<?php echo $tipo_aporte->id_tipo_aporte ?>">
                                                <?php echo $tipo_aporte->ta_descripcion; ?>
                                            </option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <button id="btn-save" type="submit" class="btn btn-success">Guardar</button>
                                    <button id="btn-cancel" type="button" class="btn btn-info">Cancelar</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <hr>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

