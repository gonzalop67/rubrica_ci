
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1 id="titulo_principal">
        Periodos de Evaluación
        </h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="box box-solid">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-8 table-responsive">
                        <input type="hidden" id="base_url" value="<?php echo base_url(); ?>">
                        <hr>
                        <table id="example1" class="table table-hover">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Nombre</th>
                                    <th>Abreviatura</th>
                                    <th><!-- Acciones --></th>
                                </tr>
                            </thead>
                            <tbody id="tbody_periodos_evaluacion">
                                <!-- En este lugar se van a poblar los periodos de evaluacion mediante AJAX -->
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-4">
                        <div class="panel panel-success">
                            <div id="titulo" class="panel-heading">Nuevo Periodo de Evaluación</div>
                        </div>
                        <div class="panel-body">
                            <form id="frm-periodo-evaluacion" action="" method="post">
                                <input type="hidden" name="id_periodo_evaluacion" id="id_periodo_evaluacion" value="0">
                                <div class="form-group">
                                    <label for="pe_nombre">Nombre:</label>
                                    <input type="text" name="pe_nombre" id="pe_nombre" class="form-control" autofocus>
                                </div>
                                <div class="form-group">
                                    <label for="pe_abreviatura">Abreviatura:</label>
                                    <input type="text" name="pe_abreviatura" id="pe_abreviatura" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="cboTiposPeriodoEvaluacion">Tipo de Periodo de Evaluación:</label>
                                    <select name="cboTiposPeriodoEvaluacion" id="cboTiposPeriodoEvaluacion" class="form-control">
                                        <option value="0">Seleccione...</option>
                                        <?php foreach ($tipos_periodo as $tipo_periodo): ?>
                                            <option value="<?php echo $tipo_periodo->id_tipo_periodo ?>">
                                                <?php echo $tipo_periodo->tp_descripcion; ?>
                                            </option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <button id="btn-save" type="submit" class="btn btn-success">Guardar</button>
                                    <button id="btn-cancel" type="button" class="btn btn-info">Cancelar</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <hr>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

