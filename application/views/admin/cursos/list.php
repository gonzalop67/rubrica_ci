
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1 id="titulo_principal">
        Cursos
        </h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="box box-solid">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-8 table-responsive">
                        <input type="hidden" id="base_url" value="<?php echo base_url(); ?>">
                        <hr>
                        <table id="example1" class="table table-hover">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Nombre</th>
                                    <th>Especialidad</th>
                                    <th><!-- Acciones --></th>
                                </tr>
                            </thead>
                            <tbody id="tbody_cursos">
                                <!-- En este lugar se van a poblar los cursos mediante AJAX -->
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-4">
                        <div class="panel panel-success">
                            <div id="titulo" class="panel-heading">Nuevo Curso</div>
                        </div>
                        <div class="panel-body">
                            <form id="frm-curso" action="" method="post">
                                <input type="hidden" name="id_curso" id="id_curso" value="0">
                                <div class="form-group">
                                    <label for="cu_nombre">Nombre:</label>
                                    <input type="text" name="cu_nombre" id="cu_nombre" class="form-control" autofocus>
                                </div>
                                <div class="form-group">
                                    <label for="cu_shortname">Nombre Corto:</label>
                                    <input type="text" name="cu_shortname" id="cu_shortname" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="cu_abreviatura">Abreviatura:</label>
                                    <input type="text" name="cu_abreviatura" id="cu_abreviatura" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="cboEspecialidades">Especialidad:</label>
                                    <select name="cboEspecialidades" id="cboEspecialidades" class="form-control">
                                        <option value="0">Seleccione...</option>
                                        <?php foreach ($especialidades as $especialidad): ?>
                                            <option value="<?php echo $especialidad->id_especialidad ?>">
                                                <?php echo $especialidad->es_figura; ?>
                                            </option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <button id="btn-save" type="submit" class="btn btn-success">Guardar</button>
                                    <button id="btn-cancel" type="button" class="btn btn-info">Cancelar</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <hr>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

