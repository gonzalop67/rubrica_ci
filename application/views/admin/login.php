<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>SIAE Web 2 | Log in</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  
  <!-- base_url() = http://localhost/ci_siae/-->

  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/template/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/template/font-awesome/css/font-awesome.min.css">

  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/template/dist/css/AdminLTE.min.css">

  <!-- Estilos propios de esta pagina -->
  <style type="text/css">
    .error {
      color: #F00;
      display: none;
    }
    .blanco {
      color: #FFF;
    }
  </style>

</head>
<body class="hold-transition login-page" style="background: url('<?php echo base_url();?>assets/images/loginFont.jpg')">
    <div class="login-box blanco">
        <div class="login-logo">
            <h2>S. I. A. E.</h2>
        </div>
        <!-- /.login-logo -->
        <div class="login-box-body">
            <p class="login-box-msg">Introduzca sus datos de ingreso</p>
            <?php if($this->session->flashdata("error")): ?>
              <div class="alert alert-danger">
                <p><?php echo $this->session->flashdata("error") ?></p>
              </div>
            <?php endif; ?>
            <form id="form-login" action="<?php echo base_url(); ?>auth/login" method="post">
                <div class="form-group has-feedback">
                    <input type="text" class="form-control" placeholder="Usuario" id="username" name="username">
                    <span class="form-control-feedback">
                      <img src="<?php echo base_url();?>assets/images/if_user_male_172625.png" height="16px" width="16px">
                    </span>
                    <span class="help-desk error" id="mensaje1">Debe ingresar su nombre de Usuario</span>
                </div>
                <div class="form-group has-feedback">
                    <input type="password" class="form-control" placeholder="Password" id="password" name="password">
                    <span class="form-control-feedback">
                      <img src="<?php echo base_url();?>assets/images/if_91_171450.png" height="16px" width="16px">
                    </span>
                    <span class="help-desk error" id="mensaje2">Debe ingresar su Password</span>
                </div>
                <div class="form-group has-feedback">
                    <select class="form-control" id="periodo" name="periodo">
                    	<option value="">Seleccione el periodo lectivo...</option>
                      <?php foreach ($periodos_lectivos as $periodo_lectivo): ?>
                        <option value="<?php echo $periodo_lectivo->id_periodo_lectivo; ?>"><?php echo $periodo_lectivo->pe_anio_inicio . " - " . $periodo_lectivo->pe_anio_fin . " [" . $periodo_lectivo->pe_descripcion . "]"; ?></option>
                      <?php endforeach ?>
                	</select>
                  <span class="help-desk error" id="mensaje3">Debe seleccionar el periodo lectivo</span>
                </div>
                <div class="form-group has-feedback">
                    <select class="form-control" id="perfil" name="perfil">
                    	<option value="">Seleccione su perfil...</option>
                      <?php foreach ($perfiles as $perfil): ?>
                        <option value="<?php echo $perfil->id_perfil; ?>"><?php echo $perfil->pe_nombre; ?></option>
                      <?php endforeach ?>                    
                    </select>
                    <span class="help-desk error" id="mensaje4">Debe seleccionar su perfil</span>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <button type="submit" class="btn btn-danger btn-block btn-flat" id="btnEnviar">Ingresar</button>
                    </div>
                </div>
                <?php if($numAdministradores == 0): ?>
                  <hr>
                  <div class="row">
                      <div class="col-xs-12">
                          <a class="btn btn-info btn-block btn-flat" id="btnRegAdmin" href="<?php echo base_url(); ?>admin">Registrar Administrador</a>
                      </div>
                  </div>
                <?php endif; ?>
            </form>
        </div>
        <!-- /.login-box-body -->
    </div>
    <!-- /.login-box -->

	<footer style="text-align: center; color: white; margin-top: -30px">
		.: &copy; <?php echo date("  Y"); ?> - <?php echo $nombreInstitucion; ?> :.
	</footer>

	<!-- jQuery 3 -->
	<script src="<?php echo base_url();?>assets/template/jquery/jquery.min.js"></script>
	<!-- Bootstrap 3.3.7 -->
	<script src="<?php echo base_url();?>assets/template/bootstrap/js/bootstrap.min.js"></script>

  <script>
    $(document).ready(function(){
      $("#btnEnviar").click(function(){
          nombre = $("#username").val();
          password = $("#password").val();
          periodo = $("#periodo").val();
          perfil = $("#perfil").val();
          errors = 0;

          if (nombre == "") {
              $("#mensaje1").fadeIn("slow");
              errors++;
          } else {
              $("#mensaje1").fadeOut();
          }
          if (password == "") {
              $("#mensaje2").fadeIn("slow");
              errors++;
          } else {
              $("#mensaje2").fadeOut();
          }
          if (periodo == "") {
              $("#mensaje3").fadeIn("slow");
              errors++;
          } else {
              $("#mensaje3").fadeOut();
          }
          if (perfil == "") {
              $("#mensaje4").fadeIn("slow");
              errors++;
          } else {
              $("#mensaje4").fadeOut();
          }

          if(errors > 0) {
              return false;
          }
      });
    });
  </script>

</body>
</html>
