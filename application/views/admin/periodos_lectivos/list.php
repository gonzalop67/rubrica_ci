
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
        Periodos Lectivos
        <small>Listado</small>
        </h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="box box-solid">
            <div class="box-body">
                <input type="hidden" name="base_url" id="base_url" value="<?php echo base_url(); ?>">
                <hr>
                <div class="row">
                    <div class="col-md-9 table-responsive">
                        <table id="example1" class="table table-hover">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Año Inicial</th>
                                    <th>Año Final</th>
                                    <th>Estado</th>
                                    <th>Opciones</th>
                                </tr>
                            </thead>
                            <tbody id="tbody_periodos_lectivos">
                                <!-- Aqui vamos a poblar los periodos lectivos ingresados en la BDD mediante AJAX  -->
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-3">
                        <div class="panel panel-success">
                            <div id="titulo" class="panel-heading">Nuevo Periodo Lectivo</div>
                        </div>
                        <div class="panel-body">
                            <form id="frm-periodo-lectivo" action="" method="post">
                                <input type="hidden" name="id_periodo_lectivo" id="id_periodo_lectivo" value="0">
                                <div class="form-group">
                                    <label for="pe_anio_inicio">Año Inicial:</label>
                                    <input type="text" name="pe_anio_inicio" id="pe_anio_inicio" class="form-control" autofocus>
                                </div>
                                <div class="form-group">
                                    <label for="pe_anio_final">Año Final:</label>
                                    <input type="text" name="pe_anio_fin" id="pe_anio_fin" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="pe_fecha_inicio">Fecha de inicio:</label>
                                    <div class="controls">
                                        <div class="input-group date">
                                            <input type="text" name="pe_fecha_inicio" id="pe_fecha_inicio" class="form-control">
                                            <label class="input-group-addon generic-btn" style="cursor: pointer;" onclick="$('#pe_fecha_inicio').focus();"><i class="fa fa-calendar" aria-hidden="true"></i></label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <button id="btn-save" type="submit" class="btn btn-success">Guardar</button>
                                    <button id="btn-cancel" type="button" class="btn btn-info">Cancelar</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

