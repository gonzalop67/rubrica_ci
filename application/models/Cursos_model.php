<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cursos_model extends CI_Model {

    public function getCursosPeriodoLectivo($id_periodo_lectivo) {
        $this->db->select('c.*,es_figura');
        $this->db->from('sw_curso c');
        $this->db->join('sw_especialidad e', 'e.id_especialidad = c.id_especialidad');
        $this->db->join('sw_tipo_educacion t', 't.id_tipo_educacion = e.id_tipo_educacion');
        $this->db->where('id_periodo_lectivo', $id_periodo_lectivo);
        $this->db->order_by('cu_orden','ASC');
        return $this->db->get()->result();
    }

    public function obtenerSecuencial($id_periodo_lectivo) {
        $query = $this->db->query("SELECT MAX(cu_orden) AS orden 
                                     FROM sw_curso c,
                                          sw_especialidad e,
                                          sw_tipo_educacion t
                                    WHERE e.id_especialidad = c.id_especialidad
                                      AND t.id_tipo_educacion = e.id_tipo_educacion
                                      AND t.id_periodo_lectivo = $id_periodo_lectivo");
        $row = $query->row();
        if(isset($row)) {
            return $row->orden + 1;
        } else {
            return 1;
        }
    }

    public function existeNombreCurso($cu_nombre, $id_curso) {
        $this->db->where('cu_nombre', $cu_nombre);
        $this->db->where('id_curso', $id_curso);
        $resultado = $this->db->get('sw_curso');
        return $resultado->num_rows() > 0;
    }

    public function getCurso($id_curso) {
        $this->db->where('id_curso', $id_curso);
        $resultado = $this->db->get('sw_curso');
        return $resultado->row();
    }

    public function tieneParalelos($id_curso) {
        $this->db->where('id_curso', $id_curso);
        $resultado = $this->db->get('sw_paralelo');
        return $resultado->num_rows() > 0;
    }

    public function getOrden($id_curso, $id_periodo_lectivo) {
        $resultado = $this->db->query("SELECT cu_orden 
                                         FROM sw_curso c, 
                                              sw_especialidad e, 
                                              sw_tipo_educacion t 
                                        WHERE e.id_especialidad = c.id_especialidad 
                                          AND t.id_tipo_educacion = e.id_tipo_educacion 
                                          AND id_curso = $id_curso 
                                          AND id_periodo_lectivo = $id_periodo_lectivo");
        return $resultado->row()->cu_orden;
    }

    // Devuelve el id_curso del curso con el orden anterior/posterior
    public function getIdCursoAnterior($cu_orden, $id_periodo_lectivo, $offset){
        $resultado = $this->db->query("SELECT id_curso 
                                         FROM sw_curso c, 
                                              sw_especialidad e, 
                                              sw_tipo_educacion t
                                        WHERE e.id_especialidad = c.id_especialidad 
                                          AND t.id_tipo_educacion = e.id_tipo_educacion
                                          AND cu_orden = $cu_orden + $offset 
                                          AND t.id_periodo_lectivo = $id_periodo_lectivo");
        return $resultado->row()->id_curso;
    }

    // Decrementa en 1 el cu_orden del curso
    public function decrementarOrdenCurso($id_curso){
        $this->db->query("UPDATE sw_curso 
                             SET cu_orden = cu_orden - 1 
                           WHERE id_curso = $id_curso");
    }

    // Incrementa en 1 el cu_orden del curso
    public function incrementarOrdenCurso($id_curso){
        $this->db->query("UPDATE sw_curso 
                             SET cu_orden = cu_orden + 1 
                           WHERE id_curso = $id_curso");
    }

    public function save($data) {
        return $this->db->insert('sw_curso', $data);
    }

    public function update($id_curso, $data) {
        $this->db->where('id_curso', $id_curso);
        return $this->db->update('sw_curso', $data);
    }

    public function delete($id_curso) {
        $this->db->where('id_curso', $id_curso);
        return $this->db->delete('sw_curso');
    }
}