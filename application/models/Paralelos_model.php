<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Paralelos_model extends CI_Model {

    public function getParalelosPeriodoLectivo($id_periodo_lectivo) {
        $this->db->select('p.*,cu_nombre,es_figura');
        $this->db->from('sw_paralelo p');
        $this->db->join('sw_curso c', 'c.id_curso = p.id_curso');
        $this->db->join('sw_especialidad e', 'e.id_especialidad = c.id_especialidad');
        $this->db->join('sw_tipo_educacion t', 't.id_tipo_educacion = e.id_tipo_educacion');
        $this->db->where('id_periodo_lectivo', $id_periodo_lectivo);
        $this->db->order_by('pa_orden','ASC');
        return $this->db->get()->result();
    }

    public function obtenerSecuencial($id_periodo_lectivo){
        $query = $this->db->query("SELECT MAX(pa_orden) AS orden 
                                     FROM sw_paralelo p,
                                          sw_curso c,
                                          sw_especialidad e,
                                          sw_tipo_educacion t
                                    WHERE c.id_curso = p.id_curso
                                      AND e.id_especialidad = c.id_especialidad
                                      AND t.id_tipo_educacion = e.id_tipo_educacion
                                      AND t.id_periodo_lectivo = $id_periodo_lectivo");
        $row = $query->row();
        if(isset($row)) {
            return $row->orden + 1;
        } else {
            return 1;
        }
    }

    public function existeParaleloCurso($pa_nombre, $id_curso) {
        $this->db->where('pa_nombre', $pa_nombre);
        $this->db->where('id_curso', $id_curso);
        $resultado = $this->db->get('sw_paralelo');
        return $resultado->num_rows() > 0;
    }

    public function getParalelo($id_paralelo) {
        $this->db->where('id_paralelo', $id_paralelo);
        $resultado = $this->db->get('sw_paralelo');
        return $resultado->row();
    }

    public function getOrden($id_paralelo, $id_periodo_lectivo) {
        $resultado = $this->db->query("SELECT pa_orden 
                                         FROM sw_paralelo p, 
                                              sw_curso c, 
                                              sw_especialidad e, 
                                              sw_tipo_educacion t 
                                        WHERE c.id_curso = p.id_curso 
                                          AND e.id_especialidad = c.id_especialidad 
                                          AND t.id_tipo_educacion = e.id_tipo_educacion 
                                          AND id_paralelo = $id_paralelo 
                                          AND id_periodo_lectivo = $id_periodo_lectivo");
        return $resultado->row()->pa_orden;
    }

    // Devuelve el id_paralelo del paralelo con el orden anterior/posterior
    public function getIdParaleloAnterior($pa_orden, $id_periodo_lectivo, $offset){
        $resultado = $this->db->query("SELECT id_paralelo 
                                         FROM sw_paralelo p,
                                              sw_curso c, 
                                              sw_especialidad e, 
                                              sw_tipo_educacion t
                                        WHERE c.id_curso = p.id_curso 
                                          AND e.id_especialidad = c.id_especialidad 
                                          AND t.id_tipo_educacion = e.id_tipo_educacion
                                          AND pa_orden = $pa_orden + $offset 
                                          AND t.id_periodo_lectivo = $id_periodo_lectivo");
        return $resultado->row()->id_paralelo;
    }

    // Decrementa en 1 el pa_orden del paralelo
    public function decrementarOrdenParalelo($id_paralelo){
        $this->db->query("UPDATE sw_paralelo 
                             SET pa_orden = pa_orden - 1 
                           WHERE id_paralelo = $id_paralelo");
    }

    // Incrementa en 1 el pa_orden del paralelo
    public function incrementarOrdenParalelo($id_paralelo){
        $this->db->query("UPDATE sw_paralelo 
                             SET pa_orden = pa_orden + 1 
                           WHERE id_paralelo = $id_paralelo");
    }

    public function save($data) {
        return $this->db->insert('sw_paralelo', $data);
    }

    public function update($id_paralelo, $data) {
        $this->db->where('id_paralelo', $id_paralelo);
        return $this->db->update('sw_paralelo', $data);
    }
}