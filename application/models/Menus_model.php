<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menus_model extends CI_Model {

    public function listarMenusNivel1($id_perfil) {
        $this->db->where('id_perfil', $id_perfil);
        $this->db->where('mnu_padre', '0');
        $this->db->order_by('mnu_orden','ASC');
        return $this->db->get('sw_menu')->result();
    }

    public function listarMenusHijos($mnu_padre) {
        $this->db->where('mnu_padre', $mnu_padre);
        $this->db->order_by('mnu_orden', 'ASC');
        return $this->db->get('sw_menu')->result();
    }

    public function getSubmenus($id_menu) {
        $this->db->where('mnu_padre', $id_menu);
        $this->db->order_by('mnu_orden','ASC');
        return $this->db->get('sw_menu')->result();
    }

    public function tieneSubmenus($id_menu) {
        $this->db->where('mnu_padre', $id_menu);
        $result = $this->db->get('sw_menu');
        return $result->num_rows() > 0;
    }

    public function obtenerSecuencial($id_perfil, $nivel, $mnu_padre){
        $resultado = $this->db->query("SELECT secuencial_menu_nivel_perfil_padre($nivel, $id_perfil, $mnu_padre) AS secuencial");
        return $resultado->row()->secuencial;
    }

    public function getOrden($id_menu){
        $resultado = $this->db->query("SELECT mnu_orden FROM sw_menu WHERE id_menu = $id_menu");
        return $resultado->row()->mnu_orden;
    }

    // Devuelve el id_menu del menu con el orden anterior/posterior de nivel 1
    public function getIdMenuPerfil($mnu_orden, $id_perfil, $offset){
        $resultado = $this->db->query("SELECT id_menu FROM sw_menu WHERE mnu_orden = $mnu_orden + $offset AND id_perfil = $id_perfil");
        return $resultado->row()->id_menu;
    }

    // Devuelve el id_menu del menu con el orden anterior de nivel 2
    public function getIdMenuPadre($mnu_orden, $mnu_padre, $offset){
        $resultado = $this->db->query("SELECT id_menu FROM sw_menu WHERE mnu_orden = $mnu_orden + $offset AND mnu_padre = $mnu_padre");
        return $resultado->row()->id_menu;
    }

    // Decrementa en 1 el mnu_orden del menu
    public function decrementarOrdenMenu($id_menu){
        $this->db->query("UPDATE sw_menu SET mnu_orden = mnu_orden - 1 WHERE id_menu = $id_menu");
    }

    // Incrementa en 1 el mnu_orden del menu
    public function incrementarOrdenMenu($id_menu){
        $this->db->query("UPDATE sw_menu SET mnu_orden = mnu_orden + 1 WHERE id_menu = $id_menu");
    }

    public function crearMenu($data){
        return $this->db->insert('sw_menu',
                    array('id_perfil'=>$data['id_perfil'],
                            'mnu_texto'=>$data['mnu_texto'],
                            'mnu_enlace'=>$data['mnu_enlace'],
                            'mnu_nivel'=>$data['mnu_nivel'],
                            'mnu_orden'=>$data['mnu_orden'],
                            'mnu_padre'=>$data['mnu_padre'],
                            'mnu_publicado'=>$data["mnu_publicado"]
                    ));
    }

    public function actualizarMenu($id_menu, $data){
        $this->db->where('id_menu', $id_menu);
        return $this->db->update('sw_menu',
                      array('mnu_texto'=>$data['mnu_texto'],
                            'mnu_enlace'=>$data['mnu_enlace'],
                            'mnu_publicado'=>$data["mnu_publicado"]
                      ));
    }

    public function delete($id_menu){
        $this->db->where('id_menu', $id_menu);
        return $this->db->delete('sw_menu');
    }

    public function getMenu($id_menu){
        $this->db->where('id_menu', $id_menu);
        $result = $this->db->get('sw_menu');
        return $result->row();
    }

}