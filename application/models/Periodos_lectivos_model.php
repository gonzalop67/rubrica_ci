<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Periodos_lectivos_model extends CI_Model {

	public function getPeriodosLectivos() {
		$this->db->select("p.*, pe_descripcion");
		$this->db->from("sw_periodo_lectivo p");
		$this->db->join("sw_periodo_estado pe", "pe.id_periodo_estado = p.id_periodo_estado");
		$this->db->order_by("pe_anio_inicio","DESC");
		$resultado = $this->db->get();
		return $resultado->result();
	}

	public function getPeriodoLectivo($id) {
		$this->db->from("sw_periodo_lectivo");
		$this->db->where("id_periodo_lectivo",$id);
		$resultado = $this->db->get();
		return $resultado->row();
	}

	public function getPeriodName($id_periodo) {
		$this->db->select("pe_anio_inicio, pe_anio_fin");
		$this->db->where("id_periodo_lectivo",$id_periodo);
		$resultado = $this->db->get("sw_periodo_lectivo");
		return $resultado->row();
	}

	public function getLastId() {
		$this->db->select("id_periodo_lectivo");
		$this->db->order_by("id_periodo_lectivo","ASC");
		$resultado = $this->db->get("sw_periodo_lectivo");
		return $resultado->row();
	}

	public function getPeriodosEstado() {
		$resultado = $this->db->get("sw_periodo_estado");
		return $resultado->result();
	}

	public function existeAnioInicial($pe_anio_inicio) {
		$this->db->where("pe_anio_inicio", $pe_anio_inicio);
		$resultado = $this->db->get("sw_periodo_lectivo");
		return $resultado->num_rows() > 0;
	}

	public function existeAnioFinal($pe_anio_fin) {
		$this->db->where("pe_anio_fin", $pe_anio_fin);
		$resultado = $this->db->get("sw_periodo_lectivo");
		return $resultado->num_rows() > 0;
	}

	public function getPeriodoAnterior($pe_anio_inicio) {
		$this->db->select("id_periodo_lectivo");
		$this->db->where("pe_anio_inicio", $pe_anio_inicio - 1);
		$resultado = $this->db->get("sw_periodo_lectivo");
		return $resultado->row()->id_periodo_lectivo;
	}

	public function save($data) {
		return $this->db->insert("sw_periodo_lectivo", $data);
	}

	public function update($id, $data) {
		$this->db->where("id_periodo_lectivo", $id);
		return $this->db->update("sw_periodo_lectivo", $data);
	}

}

?>