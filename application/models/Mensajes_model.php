<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Mensajes_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function getMensajes()
    {
        $this->db->select('m.*, us_titulo, us_apellidos, us_nombres, us_foto, pe_nombre');
        $this->db->from('sw_mensaje m');
        $this->db->join('sw_usuario u', 'u.id_usuario = m.id_usuario');
        $this->db->join('sw_perfil p', 'p.id_perfil = m.id_perfil');
        $this->db->order_by("me_fecha", "DESC");
        return $this->db->get()->result();
    }

    public function getMensaje($id)
    {
        $this->db->select('m.*, us_titulo, us_apellidos, us_nombres, us_foto, pe_nombre');
        $this->db->from('sw_mensaje m');
        $this->db->join('sw_usuario u', 'u.id_usuario = m.id_usuario');
        $this->db->join('sw_usuario_perfil up', 'u.id_usuario = up.id_usuario');
        $this->db->join('sw_perfil p', 'p.id_perfil = up.id_perfil');
        $this->db->where('id_mensaje', $id);
        return $this->db->get()->row();
    }

    public function save($data)
    {
        return $this->db->insert("sw_mensaje", $data);
    }

    public function eliminarMensaje($id)
    {
        $query = "DELETE FROM sw_mensaje WHERE id_mensaje = $id";
        $this->db->query($query);
    }
}
