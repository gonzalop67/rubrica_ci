<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Institucion_model extends CI_Model {

	public function getInstitutionName() {
        $this->db->select("in_nombre");
        $this->db->from("sw_institucion");
        $this->db->where("id_institucion = 1");
		$resultado = $this->db->get();
		return $resultado->row()->in_nombre;
	}

}

?>