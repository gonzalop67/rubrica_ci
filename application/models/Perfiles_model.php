<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Perfiles_model extends CI_Model {

	public function getPerfiles() {
		$this->db->order_by("pe_nombre");
		$resultado = $this->db->get("sw_perfil");
		return $resultado->result();
	}

	public function getPerfil($id) {
		$this->db->where("id_perfil",$id);
		$resultado = $this->db->get("sw_perfil");
		return $resultado->row();
	}

	public function save($data) {
		return $this->db->insert("sw_perfil",$data);
	}

	public function update($id, $data) {
		$this->db->where("id_perfil", $id);
		return $this->db->update("sw_perfil", $data);
	}

	public function existeNombrePerfil($pe_nombre) {
		$this->db->where('pe_nombre', $pe_nombre);
		$resultado = $this->db->get('sw_perfil');
		return $resultado->num_rows() > 0;
	}

	function existeUsuariosPerfil($id){
        $this->db->where("id_perfil", $id);
		$query = $this->db->get("sw_usuario_perfil");
        return $query->num_rows();
    }

	function existeMenusPerfil($id){
        $this->db->where("id_perfil", $id);
		$query = $this->db->get("sw_menu");
        return $query->num_rows();
	}
	
	function eliminarPerfil($id){
		$this->db->where('id_perfil', $id);
		$this->db->delete('sw_perfil');
    }

    public function getPerfilesNoAsociados($id_usuario) {
		$query = "SELECT * FROM sw_perfil WHERE id_perfil NOT IN (SELECT id_perfil FROM sw_usuario_perfil WHERE id_usuario = $id_usuario)";
        $resultado = $this->db->query($query);
		return $resultado->result();
	}

	public function getPerfilesAsociados($id_usuario) {
		$this->db->select("u.id_usuario, us_apellidos, us_nombres, p.id_perfil, pe_nombre");
		$this->db->from("sw_usuario_perfil up");
		$this->db->join("sw_usuario u","u.id_usuario = up.id_usuario");
		$this->db->join("sw_perfil p","p.id_perfil = up.id_perfil");
		$this->db->where("u.id_usuario",$id_usuario);
		$this->db->order_by("pe_nombre");
		$resultado = $this->db->get();
		return $resultado->result();
	}

}

?>