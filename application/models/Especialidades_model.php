<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Especialidades_model extends CI_Model {

    public function getEspecialidadesPeriodoLectivo($id_periodo_lectivo) {
        $this->db->select('e.*');
        $this->db->from('sw_especialidad e');
        $this->db->join('sw_tipo_educacion t', 't.id_tipo_educacion = e.id_tipo_educacion');
        $this->db->where('id_periodo_lectivo', $id_periodo_lectivo);
        $this->db->order_by('es_orden','ASC');
        return $this->db->get()->result();
    }

    public function obtenerSecuencial($id_periodo_lectivo){
        $resultado = $this->db->query("SELECT secuencial_especialidad($id_periodo_lectivo) AS secuencial");
        return $resultado->row()->secuencial;
    }

    public function getOrden($id_especialidad, $id_periodo_lectivo){
        $resultado = $this->db->query("SELECT es_orden 
                                         FROM sw_tipo_educacion t,
                                              sw_especialidad e
                                        WHERE t.id_tipo_educacion = e.id_tipo_educacion
                                          AND t.id_periodo_lectivo = $id_periodo_lectivo
                                          AND id_especialidad = $id_especialidad");
        return $resultado->row()->es_orden;
    }

    // Devuelve el id_especialidad de la especialidad con el orden anterior/posterior
    public function getIdEspecialidadPeriodoLectivo($es_orden, $id_periodo_lectivo, $offset){
        $resultado = $this->db->query("SELECT id_especialidad 
                                         FROM sw_especialidad e,
                                              sw_tipo_educacion t
                                        WHERE t.id_tipo_educacion = e.id_tipo_educacion
                                          AND es_orden = $es_orden + $offset 
                                          AND t.id_periodo_lectivo = $id_periodo_lectivo");
        return $resultado->row()->id_especialidad;
    }

    // Decrementa en 1 el es_orden de la especialidad
    public function decrementarOrdenEspecialidad($id_especialidad){
        $this->db->query("UPDATE sw_especialidad 
                             SET es_orden = es_orden - 1 
                           WHERE id_especialidad = $id_especialidad");
    }

    // Incrementa en 1 el es_orden de la especialidad
    public function incrementarOrdenEspecialidad($id_especialidad){
        $this->db->query("UPDATE sw_especialidad 
                             SET es_orden = es_orden + 1 
                           WHERE id_especialidad = $id_especialidad");
    }

    public function getEspecialidad($id_especialidad) {
        $this->db->where('id_especialidad', $id_especialidad);
        $resultado = $this->db->get('sw_especialidad');
        return $resultado->row();
    }

    public function tieneCursos($id_especialidad) {
        $this->db->where('id_especialidad', $id_especialidad);
        $resultado = $this->db->get('sw_curso');
        return $resultado->num_rows() > 0;
    }

    public function save($data) {
        return $this->db->insert('sw_especialidad', $data);
    }

    public function update($id_especialidad, $data) {
        $this->db->where('id_especialidad', $id_especialidad);
        return $this->db->update('sw_especialidad', $data);
    }

    public function delete($id_especialidad) {
        $this->db->where('id_especialidad', $id_especialidad);
        return $this->db->delete('sw_especialidad');
    }

}