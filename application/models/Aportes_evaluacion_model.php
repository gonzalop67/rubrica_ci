<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Aportes_evaluacion_model extends CI_Model {

    public function getTiposAporteEvaluacion() {
        $this->db->order_by("id_tipo_aporte");
		$resultado = $this->db->get("sw_tipo_aporte");
		return $resultado->result();
    }

    public function getAportesPeriodoEvaluacion($id_periodo_evaluacion) {
        $this->db->where("id_periodo_evaluacion", $id_periodo_evaluacion);
        $resultado = $this->db->get("sw_aporte_evaluacion");
		return $resultado->result();
    }

    public function getPeriodosEvaluacion($id_periodo_lectivo) {
        $this->db->select('*');
        $this->db->from('sw_periodo_evaluacion');
        $this->db->where('id_periodo_lectivo', $id_periodo_lectivo);
        $this->db->order_by('id_periodo_evaluacion','ASC');
        return $this->db->get()->result();
    }

    public function getAporteEvaluacion($id_aporte_evaluacion) {
        $this->db->where('id_aporte_evaluacion', $id_aporte_evaluacion);
        $resultado = $this->db->get('sw_aporte_evaluacion');
        return $resultado->row();
    }

    public function tieneRubricasEvaluacion($id_aporte_evaluacion) {
        $this->db->where('id_aporte_evaluacion', $id_aporte_evaluacion);
        $resultado = $this->db->get('sw_rubrica_evaluacion');
        return $resultado->num_rows() > 0;
    }

    public function save($data) {
        return $this->db->insert('sw_aporte_evaluacion', $data);
    }

    public function update($id_aporte_evaluacion, $data) {
        $this->db->where('id_aporte_evaluacion', $id_aporte_evaluacion);
        return $this->db->update('sw_aporte_evaluacion', $data);
    }

    public function delete($id_aporte_evaluacion) {
        $this->db->where('id_aporte_evaluacion', $id_aporte_evaluacion);
        return $this->db->delete('sw_aporte_evaluacion');
    }

}