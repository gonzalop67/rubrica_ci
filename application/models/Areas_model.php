<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Areas_model extends CI_Model {

	public function getAreas() {
		$this->db->order_by("ar_nombre");
		$resultado = $this->db->get("sw_area");
		return $resultado->result();
	}

	public function getArea($id) {
		$this->db->where("id_area",$id);
		$resultado = $this->db->get("sw_area");
		return $resultado->row();
	}

	public function save($data) {
		return $this->db->insert("sw_area",$data);
	}

	public function update($id, $data) {
		$this->db->where("id_area", $id);
		return $this->db->update("sw_area", $data);
	}

	public function existeNombreArea($ar_nombre) {
		$this->db->where('ar_nombre', $ar_nombre);
		$resultado = $this->db->get('sw_area');
		return $resultado->num_rows() > 0;
	}

	function existeAsignaturasArea($id){
        $this->db->where("id_area", $id);
		$query = $this->db->get("sw_asignatura");
        return $query->num_rows();
    }

	function eliminarArea($id){
		$this->db->where('id_area', $id);
		$this->db->delete('sw_area');
    }

}

?>