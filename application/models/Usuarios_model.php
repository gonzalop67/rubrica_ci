<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Usuarios_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function login($login, $clave, $id_perfil)
    {
        $this->db->select('up.id_usuario, us_nombres, us_foto, us_genero, us_password, p.id_perfil, pe_nombre');
        $this->db->from('sw_usuario_perfil up');
        $this->db->join('sw_usuario u', 'u.id_usuario = up.id_usuario');
        $this->db->join('sw_perfil p', 'p.id_perfil = up.id_perfil');
        $this->db->where('us_login', $login);
        $this->db->where('p.id_perfil', $id_perfil);
        $resultados = $this->db->get();

        // Compruebo si coinciden las claves...
        $res = $resultados->row();
        $decrypt_key = $this->encryption->decrypt($res->us_password);

        //if ($resultados->num_rows() > 0) {
        if ($clave == $decrypt_key) {
            return $resultados->row();
        } else {
            return false;
        }
    }

    public function getUsuariosPerfil($id_perfil)
    {
        $this->db->select("u.*, pe_nombre");
        $this->db->from("sw_usuario u");
        $this->db->join("sw_usuario_perfil up","u.id_usuario = up.id_usuario");
        $this->db->join("sw_perfil p","p.id_perfil = up.id_perfil");
        $this->db->where("up.id_perfil", $id_perfil);
        $this->db->order_by("us_apellidos");
        $resultado = $this->db->get();
        return $resultado->result();
    }

    public function getImage($id_usuario)
    {
        $this->db->select("us_foto");
        $this->db->from("sw_usuario");
        $this->db->where("id_usuario", $id_usuario);
        $resultado = $this->db->get();
        return $resultado->row()->us_foto;
    }

    public function getUsuario($id_usuario)
    {
        $this->db->select("u.*, pe_nombre");
        $this->db->from("sw_usuario u");
        $this->db->join("sw_usuario_perfil up","u.id_usuario = up.id_usuario");
        $this->db->join("sw_perfil p","p.id_perfil = up.id_perfil");
        $this->db->where("u.id_usuario", $id_usuario);
        $resultado = $this->db->get();
        return $resultado->row();
    }

    public function getUsers($valor)
    {
        $this->db->select("id_usuario AS id, CONCAT(us_apellidos,' ',us_nombres,' (',us_login,')') AS value");
        $this->db->from("sw_usuario");
        $this->db->like('us_fullname', $valor);
        $resultado = $this->db->get();
        return $resultado->result();
    }

    public function existeLogin($us_login)
    {
        $this->db->where("us_login", $us_login);
        $resultado = $this->db->get("sw_usuario");
        return $resultado->num_rows() > 0;
    }

    public function existeUsuarioPerfil($id_perfil, $id_usuario){
        $this->db->where("id_perfil", $id_perfil);
        $this->db->where("id_usuario", $id_usuario);
        $resultado = $this->db->get("sw_usuario_perfil");
        return $resultado->num_rows() > 0;
    }

    public function save($data)
    {
        $this->db->insert("sw_usuario", $data);
    }

    public function lastID()
    {
        return $this->db->insert_id();
    }

    public function save_usuario_perfil($data)
    {
        return $this->db->insert("sw_usuario_perfil", $data);
    }

    public function update($id_usuario, $data)
    {
        $this->db->where("id_usuario", $id_usuario);
        return $this->db->update("sw_usuario", $data);
    }

    public function cambiarClave($id, $clave)
    {
        $sql = "UPDATE sw_usuario SET us_clave = sha1($id) WHERE id_usuario = $id";
        return $this->db->query($sql);
    }

    public function eliminarUsuario($id)
    {
        $this->db->where("id_usuario", $id);
        $resultado = $this->db->get("sw_usuario_perfil");
        if ($resultado->num_rows() > 0) {
            return false;
        } else {
            $query = "DELETE FROM sw_usuario WHERE id_usuario = $id";
            $this->db->query($query);
            return true;
        }
    }

    public function eliminarUsuarioPerfil($id_usuario, $id_perfil)
    {
        $this->db->where("id_usuario", $id_usuario);
        $this->db->where("id_perfil", $id_perfil);

        return $this->db->delete('sw_usuario_perfil');
    }

    public function numAdministradores()
    {
        $this->db->select("u.*");
        $this->db->from("sw_usuario u");
        $this->db->join("sw_usuario_perfil up","u.id_usuario = up.id_usuario");
        $this->db->where("id_perfil", "1");
        $resultado = $this->db->get();
        return $resultado->num_rows();
    }

}
