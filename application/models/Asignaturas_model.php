<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Asignaturas_model extends CI_Model {

    public function getAsignaturas() {
        $this->db->select('id_asignatura, ar_nombre, as_nombre, as_abreviatura');
        $this->db->from('sw_asignatura a');
        $this->db->join('sw_area ar', 'ar.id_area = a.id_area');
        $this->db->order_by('ar_nombre','ASC');
        $this->db->order_by('as_nombre','ASC');
        return $this->db->get()->result();
    }

    public function searchAsignaturas($query) {
        $this->db->select('id_asignatura, ar_nombre, as_nombre, as_abreviatura');
        $this->db->from('sw_asignatura a');
        $this->db->join('sw_area ar', 'ar.id_area = a.id_area');
        $this->db->like('as_nombre', $query);
        $this->db->order_by('ar_nombre','ASC');
        $this->db->order_by('as_nombre','ASC');
        return $this->db->get()->result();
    }

    public function getTiposAsignatura() {
        $this->db->order_by("id_tipo_asignatura");
		$resultado = $this->db->get("sw_tipo_asignatura");
		return $resultado->result();
    }

    public function getAsignatura($id_asignatura) {
        $this->db->where('id_asignatura', $id_asignatura);
        $resultado = $this->db->get('sw_asignatura');
        return $resultado->row();
    }

    public function tieneCalificaciones($id_asignatura) {
        $this->db->where('id_asignatura', $id_asignatura);
        $resultado = $this->db->get('sw_rubrica_estudiante');
        return $resultado->num_rows() > 0;
    }

    public function save($data) {
        return $this->db->insert('sw_asignatura', $data);
    }

    public function update($id_asignatura, $data) {
        $this->db->where('id_asignatura', $id_asignatura);
        return $this->db->update('sw_asignatura', $data);
    }

    public function delete($id_asignatura) {
        $this->db->where('id_asignatura', $id_asignatura);
        return $this->db->delete('sw_asignatura');
    }

}