<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Periodos_evaluacion_model extends CI_Model {

    public function getTiposPeriodoEvaluacion() {
        $this->db->order_by("id_tipo_periodo");
		$resultado = $this->db->get("sw_tipo_periodo");
		return $resultado->result();
    }

    public function getPeriodosEvaluacion($id_periodo_lectivo) {
        $this->db->select('*');
        $this->db->from('sw_periodo_evaluacion');
        $this->db->where('id_periodo_lectivo', $id_periodo_lectivo);
        $this->db->order_by('id_periodo_evaluacion','ASC');
        return $this->db->get()->result();
    }

    public function getPeriodoEvaluacion($id_periodo_evaluacion) {
        $this->db->where('id_periodo_evaluacion', $id_periodo_evaluacion);
        $resultado = $this->db->get('sw_periodo_evaluacion');
        return $resultado->row();
    }

    public function tieneAportesEvaluacion($id_periodo_evaluacion) {
        $this->db->where('id_periodo_evaluacion', $id_periodo_evaluacion);
        $resultado = $this->db->get('sw_periodo_evaluacion');
        return $resultado->num_rows() > 0;
    }

    public function save($data) {
        return $this->db->insert('sw_periodo_evaluacion', $data);
    }

    public function update($id_periodo_evaluacion, $data) {
        $this->db->where('id_periodo_evaluacion', $id_periodo_evaluacion);
        return $this->db->update('sw_periodo_evaluacion', $data);
    }

    public function delete($id_periodo_evaluacion) {
        $this->db->where('id_periodo_evaluacion', $id_periodo_evaluacion);
        return $this->db->delete('sw_periodo_evaluacion');
    }

}