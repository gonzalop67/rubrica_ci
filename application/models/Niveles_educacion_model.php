<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Niveles_educacion_model extends CI_Model {

    public function getNivelesEducacion($id_periodo_lectivo) {
        $this->db->where('id_periodo_lectivo', $id_periodo_lectivo);
        $this->db->order_by('te_orden','ASC');
        return $this->db->get('sw_tipo_educacion')->result();
    }

    public function getNivelEducacion($id_tipo_educacion) {
        $this->db->where('id_tipo_educacion', $id_tipo_educacion);
        $resultado = $this->db->get('sw_tipo_educacion');
        return $resultado->row();
    }

    public function obtenerSecuencial($id_periodo_lectivo){
        $resultado = $this->db->query("SELECT secuencial_tipo_educacion($id_periodo_lectivo) AS secuencial");
        return $resultado->row()->secuencial;
    }

    public function existeNombreNivel($id_periodo_lectivo, $te_nombre) {
        $this->db->where('id_periodo_lectivo', $id_periodo_lectivo);
        $this->db->where('te_nombre', $te_nombre);
        $resultado = $this->db->get('sw_tipo_educacion');
        return $resultado->num_rows() > 0;
    }

    public function tieneEspecialidades($id_tipo_educacion) {
        $this->db->where('id_tipo_educacion', $id_tipo_educacion);
        $resultado = $this->db->get('sw_especialidad');
        return $resultado->num_rows() > 0;
    }

    public function getOrden($id_tipo_educacion){
        $resultado = $this->db->query("SELECT te_orden FROM sw_tipo_educacion WHERE id_tipo_educacion = $id_tipo_educacion");
        return $resultado->row()->te_orden;
    }

    // Devuelve el id_tipo_educacion del tipo de educación con el orden anterior/posterior de nivel 1
    public function getIdNivelPeriodoLectivo($te_orden, $id_periodo_lectivo, $offset){
        $resultado = $this->db->query("SELECT id_tipo_educacion FROM sw_tipo_educacion WHERE te_orden = $te_orden + $offset AND id_periodo_lectivo = $id_periodo_lectivo");
        return $resultado->row()->id_tipo_educacion;
    }

    // Decrementa en 1 el te_orden del tipo de educación
    public function decrementarOrdenNivelEducacion($id_tipo_educacion){
        $this->db->query("UPDATE sw_tipo_educacion SET te_orden = te_orden - 1 WHERE id_tipo_educacion = $id_tipo_educacion");
    }

    // Incrementa en 1 el te_orden del tipo de educación
    public function incrementarOrdenNivelEducacion($id_tipo_educacion){
        $this->db->query("UPDATE sw_tipo_educacion SET te_orden = te_orden + 1 WHERE id_tipo_educacion = $id_tipo_educacion");
    }

    public function save($data) {
        return $this->db->insert('sw_tipo_educacion', $data);
    }

    public function update($id_tipo_educacion, $data) {
        $this->db->where('id_tipo_educacion', $id_tipo_educacion);
        return $this->db->update('sw_tipo_educacion', $data);
    }

    public function delete($id_tipo_educacion) {
        $this->db->where('id_tipo_educacion', $id_tipo_educacion);
        return $this->db->delete('sw_tipo_educacion');
    }
}