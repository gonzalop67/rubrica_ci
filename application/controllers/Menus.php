<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Menus extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model("Perfiles_model");
    }

    public function index()
    {
        $data = array(
            'nombreInstitucion' => $this->Institucion_model->getInstitutionName(),
            'perfiles' => $this->Perfiles_model->getPerfiles(),
        );
        $this->load->view('layouts/header');
        $this->load->view('layouts/aside');
        $this->load->view('admin/menus/list', $data);
        $this->load->view('layouts/footer');
    }

    public function getMenusPerfil()
    {
        $id_perfil = $this->input->post("id_perfil");
        echo json_encode($this->Menus_model->listarMenusNivel1($id_perfil));
    }

    public function getSubmenus()
    {
        $id_menu = $this->input->post("id_menu");
        echo json_encode($this->Menus_model->getSubmenus($id_menu));
    }

    public function getMenu()
    {
        $id_menu = $this->uri->segment(3);
        $data = $this->Menus_model->getMenu($id_menu);
        echo json_encode($data);
    }

    public function store(){
        $id_perfil = $this->input->post("id_perfil");
        $mnu_texto = $this->input->post("mnu_texto");
        $mnu_enlace = $this->input->post("mnu_enlace");
        $mnu_publicado = $this->input->post("mnu_publicado");
        $mnu_padre = $this->input->post("mnu_padre");
        $mnu_nivel = $this->input->post("mnu_nivel");
        $data = array(
            'id_perfil' => $id_perfil,
            'mnu_texto' => $mnu_texto,
            'mnu_enlace' => $mnu_enlace,
            'mnu_publicado' => $mnu_publicado,
            'mnu_nivel' => $mnu_nivel,
            'mnu_orden' => $this->Menus_model->obtenerSecuencial($id_perfil, $mnu_nivel, $mnu_padre),
            'mnu_padre' => $mnu_padre
        );
        if($this->Menus_model->crearMenu($data)){
            $data = array(
				"titulo"       => "Operación exitosa.",
				"mensaje"      => "El menú fue insertado exitosamente.",
				"tipo_mensaje" => "success"
			);
			echo json_encode($data);
        }else{
            $data = array(
				"titulo"       => "Ocurrió un error inesperado.",
				"mensaje"      => "El menú no se pudo insertar...",
				"tipo_mensaje" => "error"
			);
			echo json_encode($data);
        }
    }

    public function update(){
        $id_menu = $this->uri->segment(3);
        $id_perfil = $this->input->post("id_perfil");
        $mnu_texto = $this->input->post("mnu_texto");
        $mnu_enlace = $this->input->post("mnu_enlace");
        $mnu_publicado = $this->input->post("mnu_publicado");
        $data = array(
            'id_perfil' => $id_perfil,
            'mnu_texto' => $mnu_texto,
            'mnu_enlace' => $mnu_enlace,
            'mnu_publicado' => $mnu_publicado
        );
        if($this->Menus_model->actualizarMenu($id_menu, $data)){
            $data = array(
				"titulo"       => "Operación exitosa.",
				"mensaje"      => "El menú fue actualizado exitosamente.",
				"tipo_mensaje" => "success"
			);
			echo json_encode($data);
        }else{
            $data = array(
				"titulo"       => "Ocurrió un error inesperado.",
				"mensaje"      => "El menú no se pudo actualizar...",
				"tipo_mensaje" => "error"
			);
			echo json_encode($data);
        }
    }

    public function delete(){
        $id_menu = $this->uri->segment(3);
        if($this->Menus_model->tieneSubmenus($id_menu)){
            $data = array(
                'titulo' => 'Ocurrió un error inesperado.',
                'mensaje' => 'No se puede eliminar el menú porque tiene submenús asociados.',
                'tipo_mensaje' => 'error'
            );
            echo json_encode($data);
        }else if($this->Menus_model->delete($id_menu)){
            $data = array(
                'titulo' => 'Operación exitosa.',
                'mensaje' => 'El menú fue eliminado exitosamente.',
                'tipo_mensaje' => 'success'
            );
            echo json_encode($data);
        }else{
            $data = array(
				"titulo"       => "Ocurrió un error inesperado.",
				"mensaje"      => "El menú no se pudo eliminar...",
				"tipo_mensaje" => "error"
			);
			echo json_encode($data);
        }
    }

    public function subirMenu()
    {
        $id_menu = $this->input->post("id_menu");
        $id_perfil = $this->input->post("id_perfil");
        $mnu_nivel = $this->input->post("mnu_nivel");
        $mnu_padre = $this->input->post("mnu_padre");
        $offset = -1;
        try {
            // Primero obtengo el "orden" del menu actual
            $orden = $this->Menus_model->getOrden($id_menu);
            // Ahora obtengo el id del registro que tiene el orden anterior
            if($mnu_padre==0)
                $id = $this->Menus_model->getIdMenuPerfil($orden, $id_perfil, $offset);
            else
                $id = $this->Menus_model->getIdMenuPadre($orden, $mnu_padre, $offset);
            // Se actualiza el orden (decrementar en uno) del registro actual...
            $this->Menus_model->decrementarOrdenMenu($id_menu);
            // Luego se actualiza el orden (incrementar en uno) del registro anterior al actual...
            $this->Menus_model->incrementarOrdenMenu($id);
            $data = array(
                'titulo' => 'Operación exitosa.',
                'mensaje' => 'El menú fue subido exitosamente.',
                'tipo_mensaje' => 'success'
            );
			echo json_encode($data);
        } catch (Exception $e) {
            $data = array(
				"titulo"       => "Ocurrió un error inesperado.",
				"mensaje"      => "No se pudo subir el menu...Error: " . $this->db->_error_message(),
				"tipo_mensaje" => "error"
			);
			echo json_encode($data);
        }
    }

    public function bajarMenu()
    {
        $id_menu = $this->input->post("id_menu");
        $id_perfil = $this->input->post("id_perfil");
        $mnu_nivel = $this->input->post("mnu_nivel");
        $mnu_padre = $this->input->post("mnu_padre");
        $offset = 1;
        try {
            // Primero obtengo el "orden" del menu actual
            $orden = $this->Menus_model->getOrden($id_menu);
            // Ahora obtengo el id del registro que tiene el orden posterior
            if($mnu_padre==0)
                $id = $this->Menus_model->getIdMenuPerfil($orden, $id_perfil, $offset);
            else
                $id = $this->Menus_model->getIdMenuPadre($orden, $mnu_padre, $offset);
            // Se actualiza el orden (incrementar en uno) del registro actual...
            $this->Menus_model->incrementarOrdenMenu($id_menu);
            // Luego se actualiza el orden (decrementar en uno) del registro anterior al actual...
            $this->Menus_model->decrementarOrdenMenu($id);
            $data = array(
                'titulo' => 'Operación exitosa.',
                'mensaje' => 'El menú fue bajado exitosamente.',
                'tipo_mensaje' => 'success'
            );
			echo json_encode($data);
        } catch (Exception $e) {
            $data = array(
				"titulo"       => "Ocurrió un error inesperado.",
				"mensaje"      => "No se pudo bajar el menu...Error: " . $this->db->_error_message(),
				"tipo_mensaje" => "error"
			);
			echo json_encode($data);
        }
    }

}