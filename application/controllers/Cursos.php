<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Cursos extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model("Especialidades_model");
        $this->load->model("Cursos_model");
    }

    public function index()
    {
        $id_periodo_lectivo = $this->session->userdata("id_periodo_lectivo");
        $data = array(
            'nombreInstitucion' => $this->Institucion_model->getInstitutionName(),
            'especialidades' => $this->Especialidades_model
                                     ->getEspecialidadesPeriodoLectivo($id_periodo_lectivo),
        );
        $this->load->view('layouts/header');
        $this->load->view('layouts/aside');
        $this->load->view('admin/cursos/list', $data);
        $this->load->view('layouts/footer');
    }

    public function getCursosPeriodoLectivo()
    {
        $id_periodo_lectivo = $this->session->userdata("id_periodo_lectivo");
        echo json_encode($this->Cursos_model->getCursosPeriodoLectivo($id_periodo_lectivo));
    }

    public function getCurso()
    {
        $id_curso = $this->uri->segment(3);
        echo json_encode($this->Cursos_model->getCurso($id_curso));
    }


    public function subirCurso()
    {
        $id_curso = $this->uri->segment(3);
        $id_periodo_lectivo = $this->session->userdata("id_periodo_lectivo");
        $offset = -1;
        // Primero obtengo el "orden" del curso actual
        $orden = $this->Cursos_model->getOrden($id_curso, $id_periodo_lectivo);
        // Ahora obtengo el id del registro que tiene el orden anterior
        $id = $this->Cursos_model->getIdCursoAnterior($orden, $id_periodo_lectivo, $offset);
        // Se actualiza el orden (decrementar en uno) del registro actual...
        $this->Cursos_model->decrementarOrdenCurso($id_curso);
        // Luego se actualiza el orden (incrementar en uno) del registro anterior al actual...
        $this->Cursos_model->incrementarOrdenCurso($id);
    }

    public function bajarCurso()
    {
        $id_curso = $this->uri->segment(3);
        $id_periodo_lectivo = $this->session->userdata("id_periodo_lectivo");
        $offset = 1;
        // Primero obtengo el "orden" del curso actual
        $orden = $this->Cursos_model->getOrden($id_curso, $id_periodo_lectivo);
        // Ahora obtengo el id del registro que tiene el orden posterior
        $id = $this->Curso_model->getIdCursoAnterior($orden, $id_periodo_lectivo, $offset);
        // Se actualiza el orden (incrementar en uno) del registro actual...
        $this->Cursos_model->incrementarOrdenCurso($id_curso);
        // Luego se actualiza el orden (decrementar en uno) del registro anterior al actual...
        $this->Cursos_model->decrementarOrdenCurso($id);
    }

    public function store()
    {
        $id_periodo_lectivo = $this->session->userdata("id_periodo_lectivo");
        $id_especialidad = $this->input->post("id_especialidad");
        $cu_nombre = $this->input->post("cu_nombre");
        $cu_shortname = $this->input->post("cu_shortname");
        $cu_abreviatura = $this->input->post("cu_abreviatura");

        $data = array(
            'cu_nombre'         => $cu_nombre,
            'cu_shortname'      => $cu_shortname,
            'cu_abreviatura'    => $cu_abreviatura,
            'cu_orden'          => $this->Cursos_model->obtenerSecuencial($id_periodo_lectivo),
            'id_especialidad'   => $id_especialidad
        );
        if($this->Cursos_model->save($data)){
            $data = array(
                "titulo"       => "Operación exitosa.",
                "mensaje"      => "El Curso fue insertado exitosamente.",
                "tipo_mensaje" => "success"
            );
            echo json_encode($data);
        }else{
            $data = array(
                "titulo"       => "Ocurrió un error inesperado.",
                "mensaje"      => "El Curso no se pudo insertar...",
                "tipo_mensaje" => "error"
            );
            echo json_encode($data);
        }

    }

    public function update()
    {
        $id_curso = $this->uri->segment(3);
        $cursoActual = $this->Cursos_model->getCurso($id_curso);
        
        $cu_nombre = $this->input->post("cu_nombre");
        $cu_shortname = $this->input->post("cu_shortname");
        $cu_abreviatura = $this->input->post("cu_abreviatura");
        $id_especialidad = $this->input->post("id_especialidad");

        if($cursoActual->cu_nombre != $cu_nombre){
            if($this->Cursos_model->existeNombreCurso($cu_nombre, $id_curso))
            {
                $data = array(
                    "titulo"       => "Ocurrió un error inesperado.",
                    "mensaje"      => "El Curso ingresado ya existe en la base de datos...",
                    "tipo_mensaje" => "error"
                );
                echo json_encode($data);
            }else{
                $data = array(
                    'cu_nombre'      => $cu_nombre,
                    'cu_shortname'   => $cu_shortname,
                    'cu_abreviatura' => $cu_abreviatura
                );
                if($this->Cursos_model->update($id_curso, $data)){
                    $data = array(
                        "titulo"       => "Operación exitosa.",
                        "mensaje"      => "El Curso fue actualizado exitosamente.",
                        "tipo_mensaje" => "success"
                    );
                    echo json_encode($data);
                }else{
                    $data = array(
                        "titulo"       => "Ocurrió un error inesperado.",
                        "mensaje"      => "El Curso no pudo ser actualizado...",
                        "tipo_mensaje" => "error"
                    );
                    echo json_encode($data);
                }
            }
        }else{
            $data = array(
                'cu_nombre'      => $cu_nombre,
                'cu_shortname'   => $cu_shortname,
                'cu_abreviatura' => $cu_abreviatura
            );
            if($this->Cursos_model->update($id_curso, $data)){
                $data = array(
                    "titulo"       => "Operación exitosa.",
                    "mensaje"      => "El Curso fue actualizado exitosamente.",
                    "tipo_mensaje" => "success"
                );
                echo json_encode($data);
            }else{
                $data = array(
                    "titulo"       => "Ocurrió un error inesperado.",
                    "mensaje"      => "El Curso no pudo ser actualizado...",
                    "tipo_mensaje" => "error"
                );
                echo json_encode($data);
            }
        }
    }

    public function delete()
    {
        $id_curso = $this->uri->segment(3);
        if($this->Cursos_model->tieneParalelos($id_curso)){
            $data = array(
                "titulo"       => "No se puede eliminar el Curso.",
                "mensaje"      => "Este Curso tiene asociados Paralelos...",
                "tipo_mensaje" => "error"
            );
            echo json_encode($data);
        }else{
            if($this->Curso_model->delete($id_curso)){
                $data = array(
                    "titulo"       => "Operación exitosa.",
                    "mensaje"      => "El Curso fue eliminado exitosamente.",
                    "tipo_mensaje" => "success"
                );
                echo json_encode($data);
            }else{
                $data = array(
                    "titulo"       => "Ocurrió un error inesperado.",
                    "mensaje"      => "El Curso no se pudo eliminar...",
                    "tipo_mensaje" => "error"
                );
                echo json_encode($data);
            }
        }
    }

}