<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Niveles_educacion extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model("Niveles_educacion_model");
    }

    public function index()
    {
        $data = array(
            'nombreInstitucion' => $this->Institucion_model->getInstitutionName()
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('layouts/aside');
        $this->load->view('admin/niveles_educacion/list');
        $this->load->view('layouts/footer');
    }

    public function getNivelEducacion()
    {
        $id_nivel_educacion = $this->uri->segment(3);
        echo json_encode($this->Niveles_educacion_model->getNivelEducacion($id_nivel_educacion));
    }

    public function getNivelesEducacion()
    {
        $id_periodo_lectivo = $this->session->userdata("id_periodo_lectivo");
        echo json_encode($this->Niveles_educacion_model->getNivelesEducacion($id_periodo_lectivo));
    }

    public function store()
    {
        $te_nombre = $this->input->post("te_nombre");
        $te_bachillerato = $this->input->post("te_bachillerato");
        $id_periodo_lectivo = $this->session->userdata("id_periodo_lectivo");

        if($this->Niveles_educacion_model->existeNombreNivel($id_periodo_lectivo, $te_nombre))
        {
            $data = array(
				"titulo"       => "Ocurrió un error inesperado.",
				"mensaje"      => "El Nivel de Educación ingresado ya existe en la base de datos...",
				"tipo_mensaje" => "error"
			);
			echo json_encode($data);
        }else{
            $data = array(
                'te_nombre'          => $te_nombre,
                'te_bachillerato'    => $te_bachillerato,
                'id_periodo_lectivo' => $id_periodo_lectivo,
                'te_orden' => $this->Niveles_educacion_model->obtenerSecuencial($id_periodo_lectivo)
            );
            if($this->Niveles_educacion_model->save($data)){
                $data = array(
                    "titulo"       => "Operación exitosa.",
                    "mensaje"      => "El Nivel de Educación fue insertado exitosamente.",
                    "tipo_mensaje" => "success"
                );
                echo json_encode($data);
            }else{
                $data = array(
                    "titulo"       => "Ocurrió un error inesperado.",
                    "mensaje"      => "El Nivel de Educación no se pudo insertar...",
                    "tipo_mensaje" => "error"
                );
                echo json_encode($data);
            }
        }
    }

    public function update()
    {
        $id_tipo_educacion = $this->input->post("id_tipo_educacion");
        $nivelEducacionActual = $this->Niveles_educacion_model->getNivelEducacion($id_tipo_educacion);
        
        $te_nombre = $this->input->post("te_nombre");
        $te_bachillerato = $this->input->post("te_bachillerato");
        $id_periodo_lectivo = $this->session->userdata("id_periodo_lectivo");

        if($nivelEducacionActual->te_nombre != $te_nombre){
            if($this->Niveles_educacion_model->existeNombreNivel($id_periodo_lectivo, $te_nombre))
            {
                $data = array(
                    "titulo"       => "Ocurrió un error inesperado.",
                    "mensaje"      => "El Nivel de Educación ingresado ya existe en la base de datos...",
                    "tipo_mensaje" => "error"
                );
                echo json_encode($data);
            }else{
                $data = array(
                    'te_nombre'          => $te_nombre,
                    'te_bachillerato'    => $te_bachillerato
                );
                if($this->Niveles_educacion_model->update($id_tipo_educacion, $data)){
                    $data = array(
                        "titulo"       => "Operación exitosa.",
                        "mensaje"      => "El Nivel de Educación fue actualizado exitosamente.",
                        "tipo_mensaje" => "success"
                    );
                    echo json_encode($data);
                }else{
                    $data = array(
                        "titulo"       => "Ocurrió un error inesperado.",
                        "mensaje"      => "El Nivel de Educación no se pudo actualizar...",
                        "tipo_mensaje" => "error"
                    );
                    echo json_encode($data);
                }
            }
        }else{
            $data = array(
                'te_nombre'          => $te_nombre,
                'te_bachillerato'    => $te_bachillerato
            );
            if($this->Niveles_educacion_model->update($id_tipo_educacion, $data)){
                $data = array(
                    "titulo"       => "Operación exitosa.",
                    "mensaje"      => "El Nivel de Educación fue actualizado exitosamente.",
                    "tipo_mensaje" => "success"
                );
                echo json_encode($data);
            }else{
                $data = array(
                    "titulo"       => "Ocurrió un error inesperado.",
                    "mensaje"      => "El Nivel de Educación no se pudo actualizar...",
                    "tipo_mensaje" => "error"
                );
                echo json_encode($data);
            }
        }
    }

    public function delete()
    {
        $id_tipo_educacion = $this->uri->segment(3);
        if($this->Niveles_educacion_model->tieneEspecialidades($id_tipo_educacion)){
            $data = array(
                "titulo"       => "No se puede eliminar el Nivel de Educación.",
                "mensaje"      => "Este Nivel de Educación tiene asociadas Especialidades...",
                "tipo_mensaje" => "error"
            );
            echo json_encode($data);
        }else{
            if($this->Niveles_educacion_model->delete($id_tipo_educacion)){
                $data = array(
                    "titulo"       => "Operación exitosa.",
                    "mensaje"      => "El Nivel de Educación fue eliminado exitosamente.",
                    "tipo_mensaje" => "success"
                );
                echo json_encode($data);
            }else{
                $data = array(
                    "titulo"       => "Ocurrió un error inesperado.",
                    "mensaje"      => "El Nivel de Educación no se pudo eliminar...",
                    "tipo_mensaje" => "error"
                );
                echo json_encode($data);
            }
        }
    }

    public function subirNivelEducacion()
    {
        $id_tipo_educacion = $this->input->post("id_tipo_educacion");
        $id_periodo_lectivo = $this->session->userdata("id_periodo_lectivo");
        $offset = -1;
        try {
            // Primero obtengo el "orden" del nivel de educación actual
            $orden = $this->Niveles_educacion_model->getOrden($id_tipo_educacion);
            // Ahora obtengo el id del registro que tiene el orden anterior
            $id = $this->Niveles_educacion_model->getIdNivelPeriodoLectivo($orden, $id_periodo_lectivo, $offset);
            // Se actualiza el orden (decrementar en uno) del registro actual...
            $this->Niveles_educacion_model->decrementarOrdenNivelEducacion($id_tipo_educacion);
            // Luego se actualiza el orden (incrementar en uno) del registro anterior al actual...
            $this->Niveles_educacion_model->incrementarOrdenNivelEducacion($id);
            $data = array(
                'titulo' => 'Operación exitosa.',
                'mensaje' => 'El nivel de educación fue subido exitosamente.',
                'tipo_mensaje' => 'success'
            );
			echo json_encode($data);
        } catch (Exception $e) {
            $data = array(
				"titulo"       => "Ocurrió un error inesperado.",
				"mensaje"      => "No se pudo subir el nivel de educación...Error: " . $this->db->_error_message(),
				"tipo_mensaje" => "error"
			);
			echo json_encode($data);
        }
    }

    public function bajarNivelEducacion()
    {
        $id_tipo_educacion = $this->input->post("id_tipo_educacion");
        $id_periodo_lectivo = $this->session->userdata("id_periodo_lectivo");
        $offset = 1;
        try {
            // Primero obtengo el "orden" del tipo de educación actual
            $orden = $this->Niveles_educacion_model->getOrden($id_tipo_educacion);
            // Ahora obtengo el id del registro que tiene el orden posterior
            $id = $this->Niveles_educacion_model->getIdNivelPeriodoLectivo($orden, $id_periodo_lectivo, $offset);
            // Se actualiza el orden (incrementar en uno) del registro actual...
            $this->Niveles_educacion_model->incrementarOrdenNivelEducacion($id_tipo_educacion);
            // Luego se actualiza el orden (decrementar en uno) del registro anterior al actual...
            $this->Niveles_educacion_model->decrementarOrdenNivelEducacion($id);
            $data = array(
                'titulo' => 'Operación exitosa.',
                'mensaje' => 'El menú fue bajado exitosamente.',
                'tipo_mensaje' => 'success'
            );
			echo json_encode($data);
        } catch (Exception $e) {
            $data = array(
				"titulo"       => "Ocurrió un error inesperado.",
				"mensaje"      => "No se pudo bajar el menu...Error: " . $this->db->_error_message(),
				"tipo_mensaje" => "error"
			);
			echo json_encode($data);
        }
    }
}