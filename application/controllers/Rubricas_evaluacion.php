<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Rubricas_evaluacion extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model("Periodos_evaluacion_model");
        $this->load->model("Aportes_evaluacion_model");
        $this->load->model("Rubricas_evaluacion_model");
    }

    public function index()
    {
        $id_periodo_lectivo = $this->session->userdata("id_periodo_lectivo");
        $data = array(
            'nombreInstitucion' => $this->Institucion_model->getInstitutionName(),
            'periodos_evaluacion' => $this->Periodos_evaluacion_model->getPeriodosEvaluacion($id_periodo_lectivo),
            'tipos_asignatura' => $this->Rubricas_evaluacion_model->getTiposAsignatura()
        );
        $this->load->view('layouts/header');
        $this->load->view('layouts/aside');
        $this->load->view('admin/rubricas_evaluacion/list', $data);
        $this->load->view('layouts/footer');
    }

    public function getRubricaEvaluacion()
    {
        $id_rubrica_evaluacion = $this->uri->segment(3);
        $data = $this->Rubricas_evaluacion_model->getRubricaEvaluacion($id_rubrica_evaluacion);
        echo json_encode($data);
    }

    public function getRubricasEvaluacion()
    {
        $id_aporte_evaluacion = $this->input->post("id_aporte_evaluacion");
        $id_tipo_asignatura = $this->input->post("id_tipo_asignatura");
        echo json_encode($this->Rubricas_evaluacion_model->getRubricasEvaluacion($id_aporte_evaluacion, $id_tipo_asignatura));
    }

    public function store(){
        $id_periodo_evaluacion = $this->input->post("id_periodo_evaluacion");
        $ap_nombre = $this->input->post("ap_nombre");
        $ap_abreviatura = $this->input->post("ap_abreviatura");
        $ap_fecha_apertura = $this->input->post("ap_fecha_apertura");
        $ap_fecha_cierre = $this->input->post("ap_fecha_cierre");
        $id_tipo_aporte = $this->input->post("id_tipo_aporte");
        $data = array(
            'id_periodo_evaluacion' => $id_periodo_evaluacion,
            'ap_nombre' => $ap_nombre,
            'ap_abreviatura' => $ap_abreviatura,
            'ap_fecha_apertura' => $ap_fecha_apertura,
            'ap_fecha_cierre' => $ap_fecha_cierre,
            'id_tipo_aporte' => $id_tipo_aporte
        );
        if($this->Aportes_evaluacion_model->save($data)){
            $data = array(
				"titulo"       => "Operación exitosa.",
				"mensaje"      => "El Aporte de Evaluación fue insertado exitosamente.",
				"tipo_mensaje" => "success"
			);
			echo json_encode($data);
        }else{
            $data = array(
				"titulo"       => "Ocurrió un error inesperado.",
				"mensaje"      => "El Aporte de Evaluación no se pudo insertar...",
				"tipo_mensaje" => "error"
			);
			echo json_encode($data);
        }
    }

    public function update(){
        $id_aporte_evaluacion = $this->uri->segment(3);
        $id_periodo_evaluacion = $this->input->post("id_periodo_evaluacion");
        $ap_nombre = $this->input->post("ap_nombre");
        $ap_abreviatura = $this->input->post("ap_abreviatura");
        $ap_fecha_apertura = $this->input->post("ap_fecha_apertura");
        $ap_fecha_cierre = $this->input->post("ap_fecha_cierre");
        $id_tipo_aporte = $this->input->post("id_tipo_aporte");
        $data = array(
            'id_periodo_evaluacion' => $id_periodo_evaluacion,
            'ap_nombre' => $ap_nombre,
            'ap_abreviatura' => $ap_abreviatura,
            'ap_fecha_apertura' => $ap_fecha_apertura,
            'ap_fecha_cierre' => $ap_fecha_cierre,
            'id_tipo_aporte' => $id_tipo_aporte
        );
        if($this->Aportes_evaluacion_model->update($id_aporte_evaluacion, $data)){
            $data = array(
				"titulo"       => "Operación exitosa.",
				"mensaje"      => "El Aporte de Evaluación fue actualizado exitosamente.",
				"tipo_mensaje" => "success"
			);
			echo json_encode($data);
        }else{
            $data = array(
				"titulo"       => "Ocurrió un error inesperado.",
				"mensaje"      => "El Aporte de Evaluación no se pudo actualizar...",
				"tipo_mensaje" => "error"
			);
			echo json_encode($data);
        }
    }

    public function delete(){
        $id_aporte_evaluacion = $this->uri->segment(3);
        if($this->Aportes_evaluacion_model->tieneRubricasEvaluacion($id_aporte_evaluacion)){
            $data = array(
                'titulo' => 'Ocurrió un error inesperado.',
                'mensaje' => 'No se puede eliminar el Aporte de Evaluación porque tiene Rúbricas de Evaluación asociadas.',
                'tipo_mensaje' => 'error'
            );
            echo json_encode($data);
        }else if($this->Aportes_evaluacion_model->delete($id_aporte_evaluacion)){
            $data = array(
                'titulo' => 'Operación exitosa.',
                'mensaje' => 'El Aporte de Evaluación fue eliminado exitosamente.',
                'tipo_mensaje' => 'success'
            );
            echo json_encode($data);
        }else{
            $data = array(
				"titulo"       => "Ocurrió un error inesperado.",
				"mensaje"      => "El Aporte de Evaluación no se pudo eliminar...",
				"tipo_mensaje" => "error"
			);
			echo json_encode($data);
        }
    }

}