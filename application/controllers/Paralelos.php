<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Paralelos extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model("Cursos_model");
        $this->load->model("Paralelos_model");
    }

    public function index()
    {
        $id_periodo_lectivo = $this->session->userdata("id_periodo_lectivo");
        $data = array(
            'nombreInstitucion' => $this->Institucion_model->getInstitutionName(),
            'cursos' => $this->Cursos_model
                             ->getCursosPeriodoLectivo($id_periodo_lectivo),
        );
        $this->load->view('layouts/header');
        $this->load->view('layouts/aside');
        $this->load->view('admin/paralelos/list', $data);
        $this->load->view('layouts/footer');
    }

    public function getParalelosPeriodoLectivo()
    {
        $id_periodo_lectivo = $this->session->userdata("id_periodo_lectivo");
        echo json_encode($this->Paralelos_model->getParalelosPeriodoLectivo($id_periodo_lectivo));
    }

    public function getParalelo()
    {
        $id_paralelo = $this->uri->segment(3);
        echo json_encode($this->Paralelos_model->getParalelo($id_paralelo));
    }

    public function subirParalelo()
    {
        $id_paralelo = $this->uri->segment(3);
        $id_periodo_lectivo = $this->session->userdata("id_periodo_lectivo");
        $offset = -1;
        // Primero obtengo el "orden" del paralelo actual
        $orden = $this->Paralelos_model->getOrden($id_paralelo, $id_periodo_lectivo);
        // Ahora obtengo el id del registro que tiene el orden anterior
        $id = $this->Paralelos_model->getIdParaleloAnterior($orden, $id_periodo_lectivo, $offset);
        // Se actualiza el orden (decrementar en uno) del registro actual...
        $this->Paralelos_model->decrementarOrdenParalelo($id_paralelo);
        // Luego se actualiza el orden (incrementar en uno) del registro anterior al actual...
        $this->Paralelos_model->incrementarOrdenParalelo($id);
    }

    public function bajarParalelo()
    {
        $id_paralelo = $this->uri->segment(3);
        $id_periodo_lectivo = $this->session->userdata("id_periodo_lectivo");
        $offset = 1;
        // Primero obtengo el "orden" del paralelo actual
        $orden = $this->Paralelos_model->getOrden($id_paralelo, $id_periodo_lectivo);
        // Ahora obtengo el id del registro que tiene el orden posterior
        $id = $this->Paralelos_model->getIdParaleloAnterior($orden, $id_periodo_lectivo, $offset);
        // Se actualiza el orden (incrementar en uno) del registro actual...
        $this->Paralelos_model->incrementarOrdenParalelo($id_paralelo);
        // Luego se actualiza el orden (decrementar en uno) del registro posterior al actual...
        $this->Paralelos_model->decrementarOrdenParalelo($id);
    }

    public function store()
    {
        $id_curso = $this->input->post("id_curso");
        $pa_nombre = $this->input->post("pa_nombre");
        $id_periodo_lectivo = $this->session->userdata("id_periodo_lectivo");

        $data = array(
            'pa_nombre' => $pa_nombre,
            'id_curso'  => $id_curso,
            'pa_orden'  => $this->Paralelos_model->obtenerSecuencial($id_periodo_lectivo)
        );
        if($this->Paralelos_model->save($data)){
            $data = array(
                "titulo"       => "Operación exitosa.",
                "mensaje"      => "El Paralelo fue insertado exitosamente.",
                "tipo_mensaje" => "success"
            );
            echo json_encode($data);
        }else{
            $data = array(
                "titulo"       => "Ocurrió un error inesperado.",
                "mensaje"      => "El Paralelo no se pudo insertar...",
                "tipo_mensaje" => "error"
            );
            echo json_encode($data);
        }

    }

    public function update()
    {
        $id_paralelo = $this->uri->segment(3);
        
        $pa_nombre = $this->input->post("pa_nombre");
        $id_curso = $this->input->post("id_curso");

        $data = array(
            'pa_nombre'  => $pa_nombre,
            'id_curso'   => $id_curso
        );

        if($this->Paralelos_model->existeParaleloCurso($pa_nombre, $id_curso)){
            $data = array(
                "titulo"       => "Ocurrió un error inesperado.",
                "mensaje"      => "Ya existe el nombre del Paralelo asociado al Curso seleccionado...",
                "tipo_mensaje" => "error"
            );
            echo json_encode($data);
        }else{
            if($this->Paralelos_model->update($id_paralelo, $data)){
                $data = array(
                    "titulo"       => "Operación exitosa.",
                    "mensaje"      => "El Paralelo fue actualizado exitosamente.",
                    "tipo_mensaje" => "success"
                );
                echo json_encode($data);
            }else{
                $data = array(
                    "titulo"       => "Ocurrió un error inesperado.",
                    "mensaje"      => "El Paralelo no pudo ser actualizado...",
                    "tipo_mensaje" => "error"
                );
                echo json_encode($data);
            }
        }
        
    }

}