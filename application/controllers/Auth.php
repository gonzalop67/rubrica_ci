<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Institucion_model');
        $this->load->model('Perfiles_model');
        $this->load->model('Periodos_lectivos_model');
        $this->load->model('Usuarios_model');
    }

    public function index()
    {
        /* $password = $this->encryption->encrypt("Gp67M24e");
        echo $password."<br>";
        echo $this->encryption->decrypt($password); */

        $data = array(
            'nombreInstitucion'  => $this->Institucion_model->getInstitutionName(),
            'periodos_lectivos'  => $this->Periodos_lectivos_model->getPeriodosLectivos(),
            'perfiles'           => $this->Perfiles_model->getPerfiles(),
            'numAdministradores' => $this->Usuarios_model->numAdministradores()
        );
        if ($this->session->userdata("login")) {
            redirect(base_url() . "dashboard");
        } else {
            $this->load->view('admin/login', $data);
        }
    }

    public function login()
    {
        $username           = $this->input->post("username");
        $password           = $this->input->post("password");
        $id_periodo_lectivo = $this->input->post("periodo");
        $perfil             = $this->input->post("perfil");

        $res = $this->Usuarios_model->login($username, $password, $perfil);

        if (!$res) {
            $this->session->set_flashdata("error", "El Usuario, Contraseña y/o Perfil son incorrectos.");
            redirect(base_url());
        } else {
            $periodo     = $this->Periodos_lectivos_model->getPeriodName($id_periodo_lectivo);
            $nom_periodo = $periodo->pe_anio_inicio . " - " . $periodo->pe_anio_fin;
            $data        = array(
                'id_usuario'         => $res->id_usuario,
                'nombre'             => $res->us_alias == ''? $res->us_nombres: $res->us_alias,
                'foto'               => $res->us_foto,
                'id_perfil'          => $res->id_perfil,
                'rol_name'           => $res->pe_nombre,
                'id_periodo_lectivo' => $id_periodo_lectivo,
                'periodo'            => $nom_periodo,
                'login'              => true,
            );
            $this->session->set_userdata($data);
            redirect(base_url() . "dashboard");
        }
    }

    public function logout()
    {
        $this->session->sess_destroy();
        redirect(base_url());
    }
}
