<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Perfiles extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model("Perfiles_model");
    }

    public function index()
    {
		$data = array(
            'nombreInstitucion' => $this->Institucion_model->getInstitutionName()
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('layouts/aside');
        $this->load->view('admin/perfiles/list');
        $this->load->view('layouts/footer');
    }

    public function getPerfil()
    {
        $id_perfil = $this->uri->segment(3);
        echo json_encode($this->Perfiles_model->getPerfil($id_perfil));
    }

    public function getPerfiles()
    {
        echo json_encode($this->Perfiles_model->getPerfiles());
    }

    public function store()
	{
		$pe_nombre = $this->input->post("pe_nombre");

		if ($this->Perfiles_model->existeNombrePerfil($pe_nombre)) {
			$data = array(
				"titulo"       => "Ocurrió un error inesperado.",
				"mensaje"      => "El perfil ingresado ya se encuentra en la base de datos...",
				"tipo_mensaje" => "error"
			);
			echo json_encode($data);
		} else {
			$data = array(
				'pe_nombre' => $pe_nombre
			);

			if ($this->Perfiles_model->save($data)) {
				$data = array(
					"titulo"       => "Operación exitosa.",
					"mensaje"      => "El perfil se ha insertado exitosamente...",
					"tipo_mensaje" => "success"
				);
				echo json_encode($data);
			} else {
				$data = array(
					"titulo"       => "Ocurrió un error inesperado.",
					"mensaje"      => "El perfil no se pudo insertar...",
					"tipo_mensaje" => "error"
				);
				echo json_encode($data);
			}
		}

	}

	public function update()
	{
		$id_perfil = $this->uri->segment(3);
		$pe_nombre = $this->input->post("pe_nombre");

		$perfilActual = $this->Perfiles_model->getPerfil($id_perfil);

		if ($pe_nombre != $perfilActual->pe_nombre) {
			if ($this->Perfiles_model->existeNombrePerfil($pe_nombre)) {
				$data = array(
					"titulo"       => "Ocurrió un error inesperado.",
					"mensaje"      => "El perfil ingresado ya se encuentra en la base de datos...",
					"tipo_mensaje" => "error"
				);
				echo json_encode($data);
			} else {
				$data = array(
					'pe_nombre' => $pe_nombre
				);
				if ($this->Perfiles_model->update($id_perfil, $data)) {
					$data = array(
						"titulo"       => "Operación exitosa.",
						"mensaje"      => "El perfil se ha actualizado exitosamente...",
						"tipo_mensaje" => "success"
					);
					echo json_encode($data);
				} else {
					$data = array(
						"titulo"       => "Ocurrió un error inesperado.",
						"mensaje"      => "El perfil no se pudo actualizar...",
						"tipo_mensaje" => "error"
					);
					echo json_encode($data);
				}
			}
		}
		else {
			$data = array(
				'pe_nombre' => $pe_nombre
			);
			if ($this->Perfiles_model->update($id_perfil, $data)) {
				$data = array(
					"titulo"       => "Operación exitosa.",
					"mensaje"      => "El perfil se ha actualizado exitosamente...",
					"tipo_mensaje" => "success"
				);
				echo json_encode($data);
			} else {
				$data = array(
					"titulo"       => "Ocurrió un error inesperado.",
					"mensaje"      => "El perfil no se pudo actualizar...",
					"tipo_mensaje" => "error"
				);
				echo json_encode($data);
			}
		}

	}

	public function delete()
	{
		$id_perfil = $this->uri->segment(3);
		if ($this->Perfiles_model->existeUsuariosPerfil($id_perfil) > 0){
			$data = array(
				"titulo"       => "Ocurrió un error inesperado.",
				"mensaje"      => "El perfil no se puede eliminar porque tiene usuarios relacionados...",
				"tipo_mensaje" => "error"
			);
			echo json_encode($data);
        } else if ($this->Perfiles_model->existeMenusPerfil($id_perfil) > 0){
			$data = array(
				"titulo"       => "Ocurrió un error inesperado.",
				"mensaje"      => "El perfil no se puede eliminar porque tiene menúes relacionados...",
				"tipo_mensaje" => "error"
			);
			echo json_encode($data);
		} else  {
			$this->Perfiles_model->eliminarPerfil($id_perfil);
			$data = array(
				"titulo"       => "Operación exitosa.",
				"mensaje"      => "El perfil fue eliminado exitosamente...",
				"tipo_mensaje" => "success"
			);
			echo json_encode($data);
		}
		
	}
}