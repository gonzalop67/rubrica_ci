<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Usuarios extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model("Perfiles_model");
        $this->load->model("Usuarios_model");
    }

    public function index()
    {
        $data = array(
            'nombreInstitucion' => $this->Institucion_model->getInstitutionName(),
            'perfiles' => $this->Perfiles_model->getPerfiles(),
        );
        $this->load->view('layouts/header');
        $this->load->view('layouts/aside');
        $this->load->view('admin/usuarios/list', $data);
        $this->load->view('layouts/footer');
    }

    public function getUsuariosPerfil()
    {
        $id_perfil = $this->input->post("id_perfil");
        echo json_encode($this->Usuarios_model->getUsuariosPerfil($id_perfil));
    }

    public function existeLogin(){
        $us_login = $this->uri->segment(3);
        if($this->Usuarios_model->existeLogin($us_login)){
            echo json_encode(array("existe" => true));
        }else{
            echo json_encode(array("existe" => false));
        }
    }

    public function getUsuario()
    {
        $id_usuario = $this->uri->segment(3);
        $usuario = $this->Usuarios_model->getUsuario($id_usuario);
        $data = array(
            'id_usuario' => $usuario->id_usuario,
            'us_titulo' => $usuario->us_titulo,
            'us_apellidos' => $usuario->us_apellidos,
            'us_nombres' => $usuario->us_nombres,
            'us_login' => $usuario->us_login,
            'us_password' => $this->encryption->decrypt($usuario->us_password),
            'us_genero' => $usuario->us_genero,
            'us_activo' => $usuario->us_activo,
            'us_foto' => $usuario->us_foto
        );
        echo json_encode($data);
    }

    public function getImage(){
        $id_usuario = $this->input->post("id_usuario");
        $data = array(
            'imagen' => $this->Usuarios_model->getImage($id_usuario)
        );
        echo json_encode($data);
    }

    public function searchUsers(){
        echo json_encode($this->Usuarios_model->getUsers($this->input->post("valor")));
    }

    public function desasociarUsuarioPerfil(){
        $id_perfil = $this->input->post("id_perfil");
        $id_usuario = $this->input->post("id_usuario");
        if($this->Usuarios_model->eliminarUsuarioPerfil($id_usuario, $id_perfil)){
            $data = array(
                "titulo"       => "Operación exitosa.",
                "mensaje"      => "Usuario des-asociado exitosamente...",
                "tipo_mensaje" => "success"
            );
            echo json_encode($data);
        }else{
            $data = array(
                "titulo"       => "Ocurrió un error inesperado.",
                "mensaje"      => "No se pudo des-asociar el Usuario...",
                "tipo_mensaje" => "error"
            );
            echo json_encode($data);
        }
    }

    public function asociarUsuarioPerfil(){
        $id_perfil = $this->input->post("id_perfil");
        $id_usuario = $this->input->post("id_usuario");
        //Primero verificar si ya se realizo la asociacion
        if($this->Usuarios_model->existeUsuarioPerfil($id_perfil, $id_usuario)){
            $data = array(
				"titulo"       => "Ocurrió un error inesperado.",
				"mensaje"      => "Este usuario ya fue asociado anteriormente...",
				"tipo_mensaje" => "error"
			);
			echo json_encode($data);
        }else{
            $data = array(
                'id_perfil' => $id_perfil,
                'id_usuario' => $id_usuario
            );
            if($this->Usuarios_model->save_usuario_perfil($data)){
                $data = array(
                    "titulo"       => "Operación exitosa.",
                    "mensaje"      => "Usuario asociado exitosamente...",
                    "tipo_mensaje" => "success"
                );
                echo json_encode($data);
            }else{
                $data = array(
                    "titulo"       => "Ocurrió un error inesperado.",
                    "mensaje"      => "No se pudo asociar el Usuario...",
                    "tipo_mensaje" => "error"
                );
                echo json_encode($data);
            }
        }
    }

    public function upload_image(){
        $id_usuario = $this->uri->segment(3);
        if($_FILES["us_foto"]["name"]==""){
            $data = array(
                "titulo"       => "Ocurrió un error inesperado.",
                "mensaje"      => "Debe seleccionar el archivo de la imagen...",
                "tipo_mensaje" => "error"
            );
            echo json_encode($data);
        }else{
            $config = array(
                'upload_path' => './assets/template/dist/img/',
                'allowed_types' => 'gif|jpg|jpeg|png',
                'overwrite' => TRUE,
                'max_size' => '2097152', // Can be set to particular file size , here it is 2 MB(2048 Kb)
                'max_height' => '768',
                'max_width' => '1024'
            );
            $this->load->library('upload', $config);
            if(!$this->upload->do_upload('us_foto'))
			{
                //echo $this->upload->display_errors();
                $data = array(
                    "titulo"       => "Ocurrió un error inesperado.",
                    "mensaje"      => "No se pudo subir la imagen...Error: ".$this->upload->display_errors(),
                    "tipo_mensaje" => "error"
                );
                echo json_encode($data);
            }
            else
            {
                $data = $this->upload->data();
				//echo '<img src="'.base_url().'upload/'.$data["file_name"].'" width="300" height="225" class="img-thumbnail" />';
                $us_foto = $data["file_name"];
                $data = array(
                    'us_foto' => $us_foto
                );
                if($this->Usuarios_model->update($id_usuario, $data)){
                    $data = array(
                        "titulo"       => "Operación exitosa.",
                        "mensaje"      => "El usuario fue actualizado exitosamente.",
                        "tipo_mensaje" => "success"
                    );
                    echo json_encode($data);
                }else{
                    $data = array(
                        "titulo"       => "Ocurrió un error inesperado.",
                        "mensaje"      => "El usuario no se pudo actualizar...",
                        "tipo_mensaje" => "error"
                    );
                    echo json_encode($data);
                }
            }
        }
    }

    public function store(){
        $id_periodo_lectivo = $this->session->userdata("id_periodo_lectivo");
        $id_perfil = $this->input->post("id_perfil");
        $us_titulo = $this->input->post("us_titulo");
        $us_apellidos = $this->input->post("us_apellidos");
        $us_nombres = $this->input->post("us_nombres");
        $nombres = explode(" ", $us_nombres);
        $apellidos = explode(" ", $us_apellidos);
        $us_shortname = $us_titulo . " " . $nombres[0] . " " . $apellidos[0];
        $us_fullname = $us_apellidos . " " . $us_nombres;
        $us_login = $this->input->post("us_login");
        $us_password = $this->encryption->encrypt($this->input->post("us_password"));
        $us_genero = $this->input->post("us_genero");
        $us_activo = $this->input->post("us_activo");
        $us_foto = $us_genero=="M" ? "teacher-male-avatar.png" : "teacher-female-avatar.png";
        //Primero verificar si existe el us_login en la base de datos
        if($this->Usuarios_model->existeLogin($us_login)){
            $data = array(
				"titulo"       => "Ocurrió un error inesperado.",
				"mensaje"      => "El nombre de usuario ya existe en la base de datos...",
				"tipo_mensaje" => "error"
			);
			echo json_encode($data);
        }else{
            $data = array(
                'id_periodo_lectivo' => $id_periodo_lectivo,
                'id_perfil' => $id_perfil,
                'us_titulo' => $us_titulo,
                'us_apellidos' => $us_apellidos,
                'us_nombres' => $us_nombres,
                'us_shortname' => $us_shortname,
                'us_fullname' => $us_fullname,
                'us_login' => $us_login,
                'us_password' => $us_password,
                'us_foto' => $us_foto,
                'us_genero' => $us_genero,
                'us_activo' => $us_activo
            );
            try {
                $this->Usuarios_model->save($data);
                $last_ID = $this->Usuarios_model->lastID();
                $data = array(
                    'id_usuario' => $last_ID,
                    'id_perfil' => $id_perfil
                );
                $this->Usuarios_model->save_usuario_perfil($data);
                $data = array(
                    "titulo"       => "Operación exitosa.",
                    "mensaje"      => "El usuario fue insertado exitosamente.",
                    "tipo_mensaje" => "success"
                );
                echo json_encode($data);
            } catch (Exception $e) {
                $data = array(
                    "titulo"       => "Ocurrió un error inesperado.",
                    "mensaje"      => "El usuario no se pudo insertar...Error: ".$e->getMessage(),
                    "tipo_mensaje" => "error"
                );
                echo json_encode($data);
            }
        }
    }

    public function update()
    {

        $id_usuario = $this->uri->segment(3);
        $us_titulo = $this->input->post("us_titulo");
        $us_apellidos = $this->input->post("us_apellidos");
        $us_nombres = $this->input->post("us_nombres");
        $nombres = explode(" ", $us_nombres);
        $apellidos = explode(" ", $us_apellidos);
        $us_shortname = $us_titulo . " " . $nombres[0] . " " . $apellidos[0];
        $us_fullname = $us_apellidos . " " . $us_nombres;
        $us_login = $this->input->post("us_login");
        $us_password = $this->input->post("us_password");
        $us_genero = $this->input->post("us_genero");
        $us_activo = $this->input->post("us_activo");

        $data = array(
            'us_titulo' => $us_titulo,
            'us_apellidos' => $us_apellidos,
            'us_nombres' => $us_nombres,
            'us_shortname' => $us_shortname,
            'us_fullname' => $us_fullname,
            'us_login' => $us_login,
            'us_password' => $this->encryption->encrypt($us_password),
            'us_genero' => $us_genero,
            'us_activo' => $us_activo
        );
        
        if($this->Usuarios_model->update($id_usuario, $data)){
            $data = array(
                "titulo"       => "Operación exitosa.",
                "mensaje"      => "El usuario fue actualizado exitosamente.",
                "tipo_mensaje" => "success"
            );
            echo json_encode($data);
        }else{
            $data = array(
                "titulo"       => "Ocurrió un error inesperado.",
                "mensaje"      => "El usuario no se pudo actualizar...",
                "tipo_mensaje" => "error"
            );
            echo json_encode($data);
        }
    }

/*    }else{
            
            
            
            
        }

    }
}*/
}