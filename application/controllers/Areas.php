<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Areas extends CI_Controller
{

    public function __construct()
    {
		parent::__construct();
        $this->load->model("Areas_model");
    }

    public function index()
    {
		$data = array(
			'nombreInstitucion' => $this->Institucion_model->getInstitutionName()
		);
        $this->load->view('layouts/header', $data);
        $this->load->view('layouts/aside');
        $this->load->view('admin/areas/list');
        $this->load->view('layouts/footer');
    }

    public function getArea()
    {
        $id_area = $this->uri->segment(3);
        echo json_encode($this->Areas_model->getArea($id_area));
    }

    public function getAreas()
    {
        echo json_encode($this->Areas_model->getAreas());
    }

    public function store()
	{
		$ar_nombre = $this->input->post("ar_nombre");

		if ($this->Areas_model->existeNombreArea($ar_nombre)) {
			$data = array(
				"titulo"       => "Ocurrió un error inesperado.",
				"mensaje"      => "El área ingresada ya se encuentra en la base de datos...",
				"tipo_mensaje" => "error"
			);
			echo json_encode($data);
		} else {
			$data = array(
				'ar_nombre' => $ar_nombre
			);

			if ($this->Areas_model->save($data)) {
				$data = array(
					"titulo"       => "Operación exitosa.",
					"mensaje"      => "El área se ha insertado exitosamente...",
					"tipo_mensaje" => "success"
				);
				echo json_encode($data);
			} else {
				$data = array(
					"titulo"       => "Ocurrió un error inesperado.",
					"mensaje"      => "El área no se pudo insertar...",
					"tipo_mensaje" => "error"
				);
				echo json_encode($data);
			}
		}

	}

	public function update()
	{
		$id_area = $this->uri->segment(3);
		$ar_nombre = $this->input->post("ar_nombre");

		$areaActual = $this->Areas_model->getArea($id_area);

		if ($ar_nombre != $areaActual->ar_nombre) {
			if ($this->Areas_model->existeNombreArea($ar_nombre)) {
				$data = array(
					"titulo"       => "Ocurrió un error inesperado.",
					"mensaje"      => "El área ingresada ya se encuentra en la base de datos...",
					"tipo_mensaje" => "error"
				);
				echo json_encode($data);
			} else {
				$data = array(
					'ar_nombre' => $ar_nombre
				);
				if ($this->Areas_model->update($id_area, $data)) {
					$data = array(
						"titulo"       => "Operación exitosa.",
						"mensaje"      => "El área se ha actualizado exitosamente...",
						"tipo_mensaje" => "success"
					);
					echo json_encode($data);
				} else {
					$data = array(
						"titulo"       => "Ocurrió un error inesperado.",
						"mensaje"      => "El área no se pudo actualizar...",
						"tipo_mensaje" => "error"
					);
					echo json_encode($data);
				}
			}
		}
		else {
			$data = array(
				'ar_nombre' => $ar_nombre
			);
			if ($this->Areas_model->update($id_area, $data)) {
				$data = array(
					"titulo"       => "Operación exitosa.",
					"mensaje"      => "El área se ha actualizado exitosamente...",
					"tipo_mensaje" => "success"
				);
				echo json_encode($data);
			} else {
				$data = array(
					"titulo"       => "Ocurrió un error inesperado.",
					"mensaje"      => "El área no se pudo actualizar...",
					"tipo_mensaje" => "error"
				);
				echo json_encode($data);
			}
		}

	}

	public function delete()
	{
		$id_area = $this->uri->segment(3);
		if ($this->Areas_model->existeAsignaturasArea($id_area) > 0){
			$data = array(
				"titulo"       => "Ocurrió un error inesperado.",
				"mensaje"      => "El área no se puede eliminar porque tiene asignaturas relacionadas...",
				"tipo_mensaje" => "error"
			);
			echo json_encode($data);
        } else  {
			$this->Areas_model->eliminarArea($id_area);
			$data = array(
				"titulo"       => "Operación exitosa.",
				"mensaje"      => "El área fue eliminada exitosamente...",
				"tipo_mensaje" => "success"
			);
			echo json_encode($data);
		}
		
	}
}