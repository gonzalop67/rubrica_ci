<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Especialidades extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model("Niveles_educacion_model");
        $this->load->model("Especialidades_model");
    }

    public function index()
    {
        $id_periodo_lectivo = $this->session->userdata("id_periodo_lectivo");
        $data = array(
            'nombreInstitucion' => $this->Institucion_model->getInstitutionName(),
            'niveles_educacion' => $this->Niveles_educacion_model->getNivelesEducacion($id_periodo_lectivo),
        );
        $this->load->view('layouts/header');
        $this->load->view('layouts/aside');
        $this->load->view('admin/especialidades/list', $data);
        $this->load->view('layouts/footer');
    }

    public function getEspecialidadesPeriodoLectivo()
    {
        $id_periodo_lectivo = $this->session->userdata("id_periodo_lectivo");
        echo json_encode($this->Especialidades_model->getEspecialidadesPeriodoLectivo($id_periodo_lectivo));
    }

    public function getEspecialidad()
    {
        $id_especialidad = $this->uri->segment(3);
        echo json_encode($this->Especialidades_model->getEspecialidad($id_especialidad));
    }

    public function subirEspecialidad()
    {
        $id_especialidad = $this->uri->segment(3);
        $id_periodo_lectivo = $this->session->userdata("id_periodo_lectivo");
        $offset = -1;
        try {
            // Primero obtengo el "orden" de la especialidad actual
            $orden = $this->Especialidades_model->getOrden($id_especialidad, $id_periodo_lectivo);
            // Ahora obtengo el id del registro que tiene el orden anterior
            $id = $this->Especialidades_model->getIdEspecialidadPeriodoLectivo($orden, $id_periodo_lectivo, $offset);
            // Se actualiza el orden (decrementar en uno) del registro actual...
            $this->Especialidades_model->decrementarOrdenEspecialidad($id_especialidad);
            // Luego se actualiza el orden (incrementar en uno) del registro anterior al actual...
            $this->Especialidades_model->incrementarOrdenEspecialidad($id);
            $data = array(
                'titulo' => 'Operación exitosa.',
                'mensaje' => 'La especialidad fue subida exitosamente.',
                'tipo_mensaje' => 'success'
            );
			echo json_encode($data);
        } catch (Exception $e) {
            $data = array(
				"titulo"       => "Ocurrió un error inesperado.",
				"mensaje"      => "No se pudo subir la especialidad...Error: " . $this->db->_error_message(),
				"tipo_mensaje" => "error"
			);
			echo json_encode($data);
        }
    }

    public function store()
    {
        $id_tipo_educacion = $this->input->post("id_tipo_educacion");
        $es_nombre = $this->input->post("es_nombre");
        $es_figura = $this->input->post("es_figura");
        $es_abreviatura = $this->input->post("es_abreviatura");
        $id_periodo_lectivo = $this->session->userdata("id_periodo_lectivo");

        $data = array(
            'es_nombre'         => $es_nombre,
            'es_figura'         => $es_figura,
            'es_abreviatura'    => $es_abreviatura,
            'id_tipo_educacion' => $id_tipo_educacion,
            'es_orden' => $this->Especialidades_model->obtenerSecuencial($id_periodo_lectivo)
        );
        if($this->Especialidades_model->save($data)){
            $data = array(
                "titulo"       => "Operación exitosa.",
                "mensaje"      => "La Especialidad fue insertada exitosamente.",
                "tipo_mensaje" => "success"
            );
            echo json_encode($data);
        }else{
            $data = array(
                "titulo"       => "Ocurrió un error inesperado.",
                "mensaje"      => "La Especialidad no se pudo insertar...",
                "tipo_mensaje" => "error"
            );
            echo json_encode($data);
        }

    }

    public function update()
    {
        $id_especialidad = $this->uri->segment(3);
        $especialidadActual = $this->Especialidades_model->getEspecialidad($id_especialidad);
        
        $es_nombre = $this->input->post("es_nombre");
        $es_figura = $this->input->post("es_figura");
        $es_abreviatura = $this->input->post("es_abreviatura");
        $id_tipo_educacion = $this->input->post("id_tipo_educacion");

        if($especialidadActual->es_nombre != $es_nombre){
            if($this->Especialidades_model->existeNombreEspecialidad($es_nombre, $id_tipo_educacion))
            {
                $data = array(
                    "titulo"       => "Ocurrió un error inesperado.",
                    "mensaje"      => "La Especialidad ingresada ya existe en la base de datos...",
                    "tipo_mensaje" => "error"
                );
                echo json_encode($data);
            }else{
                $data = array(
                    'es_nombre'      => $es_nombre,
                    'es_figura'      => $es_figura,
                    'es_abreviatura' => $es_abreviatura
                );
                if($this->Especialidades_model->update($id_especialidad, $data)){
                    $data = array(
                        "titulo"       => "Operación exitosa.",
                        "mensaje"      => "La Especialidad fue actualizada exitosamente.",
                        "tipo_mensaje" => "success"
                    );
                    echo json_encode($data);
                }else{
                    $data = array(
                        "titulo"       => "Ocurrió un error inesperado.",
                        "mensaje"      => "La Especialidad no pudo ser actualizada...",
                        "tipo_mensaje" => "error"
                    );
                    echo json_encode($data);
                }
            }
        }else{
            $data = array(
                'es_nombre'      => $es_nombre,
                'es_figura'      => $es_figura,
                'es_abreviatura' => $es_abreviatura
            );
            if($this->Especialidades_model->update($id_especialidad, $data)){
                $data = array(
                    "titulo"       => "Operación exitosa.",
                    "mensaje"      => "La Especialidad fue actualizada exitosamente.",
                    "tipo_mensaje" => "success"
                );
                echo json_encode($data);
            }else{
                $data = array(
                    "titulo"       => "Ocurrió un error inesperado.",
                    "mensaje"      => "La Especialidad no pudo ser actualizada...",
                    "tipo_mensaje" => "error"
                );
                echo json_encode($data);
            }
        }
    }

    public function delete()
    {
        $id_especialidad = $this->uri->segment(3);
        if($this->Especialidades_model->tieneCursos($id_especialidad)){
            $data = array(
                "titulo"       => "No se puede eliminar la Especialidad.",
                "mensaje"      => "Esta Especialidad tiene asociados Cursos...",
                "tipo_mensaje" => "error"
            );
            echo json_encode($data);
        }else{
            if($this->Especialidades_model->delete($id_especialidad)){
                $data = array(
                    "titulo"       => "Operación exitosa.",
                    "mensaje"      => "La Especialidad fue eliminada exitosamente.",
                    "tipo_mensaje" => "success"
                );
                echo json_encode($data);
            }else{
                $data = array(
                    "titulo"       => "Ocurrió un error inesperado.",
                    "mensaje"      => "La Especialidad no se pudo eliminar...",
                    "tipo_mensaje" => "error"
                );
                echo json_encode($data);
            }
        }
    }

}