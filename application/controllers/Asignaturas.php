<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Asignaturas extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model("Asignaturas_model");
        $this->load->model("Areas_model");
    }

    public function index()
    {
        $data = array(
            'nombreInstitucion' => $this->Institucion_model->getInstitutionName(),
            'areas' => $this->Areas_model->getAreas(),
            'tipos' => $this->Asignaturas_model->getTiposAsignatura()
        );
        $this->load->view('layouts/header');
        $this->load->view('layouts/aside');
        $this->load->view('admin/asignaturas/list', $data);
        $this->load->view('layouts/footer');
    }

    public function getAsignaturas()
    {
        echo json_encode($this->Asignaturas_model->getAsignaturas());
    }

    public function searchAsignaturas()
    {
        $query = $this->input->post("query");
        echo json_encode($this->Asignaturas_model->searchAsignaturas($query));
    }

    public function getAsignatura()
    {
        $id_asignatura = $this->uri->segment(3);
        echo json_encode($this->Asignaturas_model->getAsignatura($id_asignatura));
    }

    public function store()
    {
        $as_nombre = $this->input->post("as_nombre");
        $as_abreviatura = $this->input->post("as_abreviatura");
        $id_area = $this->input->post("id_area");
        $id_tipo_asignatura = $this->input->post("id_tipo_asignatura");

        $data = array(
            'as_nombre'          => $as_nombre,
            'as_abreviatura'     => $as_abreviatura,
            'id_area'            => $id_area,
            'id_tipo_asignatura' => $id_tipo_asignatura
        );
        if($this->Asignaturas_model->save($data)){
            $data = array(
                "titulo"       => "Operación exitosa.",
                "mensaje"      => "La Asignatura fue insertada exitosamente.",
                "tipo_mensaje" => "success"
            );
            echo json_encode($data);
        }else{
            $data = array(
                "titulo"       => "Ocurrió un error inesperado.",
                "mensaje"      => "La Asignatura no se pudo insertar...",
                "tipo_mensaje" => "error"
            );
            echo json_encode($data);
        }

    }

    public function update()
    {
        $id_asignatura = $this->uri->segment(3);
        $asignaturaActual = $this->Asignaturas_model->getAsignatura($id_asignatura);
        
        $as_nombre = $this->input->post("as_nombre");
        $as_abreviatura = $this->input->post("as_abreviatura");
        $id_area = $this->input->post("id_area");
        $id_tipo_asignatura = $this->input->post("id_tipo_asignatura");

        if($asignaturaActual->as_nombre != $as_nombre){
            if($this->Asignaturas_model->existeNombreAsignatura($as_nombre))
            {
                $data = array(
                    "titulo"       => "Ocurrió un error inesperado.",
                    "mensaje"      => "La Asignatura digitada ya existe en la base de datos...",
                    "tipo_mensaje" => "error"
                );
                echo json_encode($data);
            }else{
                $data = array(
                    'as_nombre'          => $as_nombre,
                    'as_abreviatura'     => $as_abreviatura,
                    'id_area'            => $id_area,
                    'id_tipo_asignatura' => $id_tipo_asignatura
                );
                if($this->Asignaturas_model->update($id_asignatura, $data)){
                    $data = array(
                        "titulo"       => "Operación exitosa.",
                        "mensaje"      => "La Asignatura fue actualizada exitosamente.",
                        "tipo_mensaje" => "success"
                    );
                    echo json_encode($data);
                }else{
                    $data = array(
                        "titulo"       => "Ocurrió un error inesperado.",
                        "mensaje"      => "La Asignatura no pudo ser actualizada...",
                        "tipo_mensaje" => "error"
                    );
                    echo json_encode($data);
                }
            }
        }else{
            $data = array(
                'as_nombre'          => $as_nombre,
                'as_abreviatura'     => $as_abreviatura,
                'id_area'            => $id_area,
                'id_tipo_asignatura' => $id_tipo_asignatura
            );
            if($this->Asignaturas_model->update($id_asignatura, $data)){
                $data = array(
                    "titulo"       => "Operación exitosa.",
                    "mensaje"      => "La Asignatura fue actualizada exitosamente.",
                    "tipo_mensaje" => "success"
                );
                echo json_encode($data);
            }else{
                $data = array(
                    "titulo"       => "Ocurrió un error inesperado.",
                    "mensaje"      => "La Asignatura no pudo ser actualizada...",
                    "tipo_mensaje" => "error"
                );
                echo json_encode($data);
            }
        }
    }

    public function delete()
    {
        $id_asignatura = $this->uri->segment(3);
        if($this->Asignaturas_model->tieneCalificaciones($id_asignatura)){
            $data = array(
                "titulo"       => "No se puede eliminar la Asignatura.",
                "mensaje"      => "Esta Asignatura tiene asociadas Calificaciones...",
                "tipo_mensaje" => "error"
            );
            echo json_encode($data);
        }else{
            if($this->Asignaturas_model->delete($id_asignatura)){
                $data = array(
                    "titulo"       => "Operación exitosa.",
                    "mensaje"      => "La Asignatura fue eliminada exitosamente.",
                    "tipo_mensaje" => "success"
                );
                echo json_encode($data);
            }else{
                $data = array(
                    "titulo"       => "Ocurrió un error inesperado.",
                    "mensaje"      => "La Asignatura no se pudo eliminar...",
                    "tipo_mensaje" => "error"
                );
                echo json_encode($data);
            }
        }
    }

}