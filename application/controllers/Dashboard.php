<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata("login")) {
            redirect(base_url());
        }
        $this->load->model('Usuarios_model');
    }

    public function index()
    {
        $data = array(
            'nombreInstitucion' => $this->Institucion_model->getInstitutionName(),
            'usuario' => $this->Usuarios_model->getUsuario($this->session->userdata("id_usuario"))
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('layouts/aside');
        $this->load->view('admin/dashboard');
        $this->load->view('layouts/footer');
    }
}
