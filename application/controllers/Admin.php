<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Periodos_lectivos_model');
        $this->load->model('Usuarios_model');
    }

    public function index()
    {
        $this->load->view('admin/nuevo');
    }

    public function registrar()
    {
        $titulo = $this->input->post("usertitle");
        $apellidos = $this->input->post("userlastname");
        $nombres = $this->input->post("userfirstname");
        $clave = $this->input->post("password");

        //Aplico la funcion escape para evitar la inyección sql
        $titulo = $this->db->escape($titulo);
        $apellidos = $this->db->escape($apellidos);
        $nombres = $this->db->escape($nombres);
        $clave = $this->db->escape($clave);

        $this->form_validation->set_rules("usertitle","Título del Usuario","trim|required|min_length[3]|max_length[5]");
        $this->form_validation->set_rules("userlastname","Apellidos del Usuario","trim|required");
        $this->form_validation->set_rules("userfirstname","Nombres del Usuario","trim|required");
        $this->form_validation->set_rules("password","Password","required");

        if ($this->form_validation->run()) {
            // Obtengo el primer apellido
            $array_apellidos = explode(" ", $apellidos);
            $primer_apellido = $array_apellidos[0];

            // Obtengo el primer nombre
            $array_nombres = explode(" ", $nombres);
            $primer_nombre = $array_nombres[0];

            $data = array( 
				'id_perfil'          => 1, 
                'us_titulo'          => $titulo,
                'us_apellidos'       => $apellidos,
                'us_nombres'         => $nombres,
                'us_shortname'       => $titulo . " " . $primer_nombre . " " . $primer_apellido,
                'us_fullname'        => $apellidos . " " . $nombres,
                'us_login'           => 'administrador',
                'us_password'        => $this->encryption->encrypt($clave),
                'us_activo'          => 1
            );
            
            if ($this->Usuarios_model->save($data)) {
				redirect(base_url()."auth");
			}
			else {
				$this->session->set_flashdata("error", "No se pudo guardar al Administrador.");
				redirect(base_url()."admin");
            }
            
        } else {
            $this->load->view('admin/nuevo');
        }
    }
}

