<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Periodos_lectivos extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model("Periodos_lectivos_model");
    }

    public function index()
    {
        $data = array(
            'nombreInstitucion' => $this->Institucion_model->getInstitutionName()
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('layouts/aside');
        $this->load->view('admin/periodos_lectivos/list');
        $this->load->view('layouts/footer');
    }

    public function getPeriodoLectivo()
    {
        $id_periodo_lectivo = $this->uri->segment(3);
        echo json_encode($this->Periodos_lectivos_model->getPeriodoLectivo($id_periodo_lectivo));
    }

    public function getPeriodosLectivos()
    {
        echo json_encode($this->Periodos_lectivos_model->getPeriodosLectivos());
    }
 
    public function store()
    {
        $pe_anio_inicio = $this->input->post("pe_anio_inicio");
        $pe_anio_fin = $this->input->post("pe_anio_fin");
        $pe_fecha_inicio = $this->input->post("pe_fecha_inicio");

        if($this->Periodos_lectivos_model->existeAnioInicial($pe_anio_inicio)) {
            $data = array(
                "titulo"       => "Ocurrió un error inesperado.",
                "mensaje"      => "El año inicial ya se encuentra ingresado en la base de datos.",
                "tipo_mensaje" => "error"
            );
            echo json_encode($data);
        }else if($this->Periodos_lectivos_model->existeAnioFinal($pe_anio_fin)) {
            $data = array(
                "titulo"       => "Ocurrió un error inesperado.",
                "mensaje"      => "El año final ya se encuentra ingresado en la base de datos.",
                "tipo_mensaje" => "error"
            );
            echo json_encode($data);
        }else{
            $id_periodo_lectivo_anterior = $this->Periodos_lectivos_model->getPeriodoAnterior($pe_anio_inicio);
            if(!is_null($id_periodo_lectivo_anterior)){
                $sql = "CALL sp_cerrar_periodo_lectivo($id_periodo_lectivo_anterior)";
                $this->db->query($sql);
            }
            $data = array(
                'pe_anio_inicio' => $pe_anio_inicio,
                'pe_anio_fin' => $pe_anio_fin,
                'pe_fecha_inicio' => $pe_fecha_inicio,
                'id_periodo_estado' => 1,
                'id_institucion' => 1,
                'pe_estado' => 'A'
            );
            if($this->Periodos_lectivos_model->save($data)){
                $data = array(
                    "titulo"       => "Operación exitosa.",
                    "mensaje"      => "El periodo lectivo se ha insertado exitosamente...",
                    "tipo_mensaje" => "success"
                );
                echo json_encode($data);
            }else{
                $data = array(
                    "titulo"       => "Ocurrió un error inesperado.",
                    "mensaje"      => "El periodo lectivo no se pudo insertar exitosamente...",
                    "tipo_mensaje" => "error"
                );
                echo json_encode($data);
            }
        }
    }

    public function cerrar($id)
    {
        try {
            $sql = "CALL sp_cerrar_periodo_lectivo($id)";
            $result = $this->db->query($sql);
            $data = array(
                "titulo"       => "Operación exitosa.",
                "mensaje"      => "El periodo lectivo se ha cerrado exitosamente...",
                "tipo_mensaje" => "success"
            );
            echo json_encode($data);
        } catch (Exception $e) {
            $data = array(
                "titulo"       => "Ocurrió un error inesperado.",
                "mensaje"      => "El periodo lectivo no se pudo cerrar exitosamente...Error: " . $e->getMessage(),
                "tipo_mensaje" => "error"
            );
            echo json_encode($data);
        }
    }

	public function update($id) {
		$pe_anio_inicio = $this->input->post("pe_anio_inicio");
        $pe_anio_fin = $this->input->post("pe_anio_fin");
        $pe_fecha_inicio = $this->input->post("pe_fecha_inicio");

        $periodo_lectivo_actual = $this->Periodos_lectivos_model->getPeriodoLectivo($id);

        if($pe_anio_inicio != $periodo_lectivo_actual->pe_anio_inicio || $pe_anio_fin != $periodo_lectivo_actual->pe_anio_fin) {
            if($this->Periodos_lectivos_model->existeAnioInicial($pe_anio_inicio)) {
                $data = array(
                    "titulo"       => "Ocurrió un error inesperado.",
                    "mensaje"      => "El año inicial ya se encuentra ingresado en la base de datos.",
                    "tipo_mensaje" => "error"
                );
                echo json_encode($data);
            } else {
                if($this->Periodos_lectivos_model->existeAnioFinal($pe_anio_fin)) {
                    $data = array(
                        "titulo"       => "Ocurrió un error inesperado.",
                        "mensaje"      => "El año final ya se encuentra ingresado en la base de datos.",
                        "tipo_mensaje" => "error"
                    );
                    echo json_encode($data);
                } else {
                    $data = array(
                        'pe_anio_inicio' => $pe_anio_inicio,
                        'pe_anio_fin' => $pe_anio_fin,
                        'pe_fecha_inicio' => $pe_fecha_inicio
                    );

                    if($this->Periodos_lectivos_model->update($id, $data)) {
                        $data = array(
                            "titulo"       => "Operación exitosa.",
                            "mensaje"      => "El periodo lectivo se ha insertado exitosamente...",
                            "tipo_mensaje" => "success"
                        );
                        echo json_encode($data);
                    } else {
                        $data = array(
                            "titulo"       => "Ocurrió un error inesperado.",
                            "mensaje"      => "El periodo lectivo no se pudo insertar exitosamente...",
                            "tipo_mensaje" => "error"
                        );
                        echo json_encode($data);
                    } // fin if($this->Periodos_lectivos_model->update($id, $data))
                } // fin if($this->Periodos_lectivos_model->existeAnioFinal($pe_anio_fin))
            } // fin if($this->Periodos_lectivos_model->existeAnioInicial($pe_anio_inicio))
        } else {
            $data = array(
                'pe_anio_inicio' => $pe_anio_inicio,
                'pe_anio_fin' => $pe_anio_fin,
                'pe_fecha_inicio' => $pe_fecha_inicio
            );

            if($this->Periodos_lectivos_model->update($id, $data)) {
                $data = array(
                    "titulo"       => "Operación exitosa.",
                    "mensaje"      => "El periodo lectivo se ha insertado exitosamente...",
                    "tipo_mensaje" => "success"
                );
                echo json_encode($data);
            } else {
                $data = array(
                    "titulo"       => "Ocurrió un error inesperado.",
                    "mensaje"      => "El periodo lectivo no se pudo insertar exitosamente...",
                    "tipo_mensaje" => "error"
                );
                echo json_encode($data);
            } // fin if($this->Periodos_lectivos_model->update($id, $data))
        } // fin if($pe_anio_inicio != $periodo_lectivo_actual->pe_anio_inicio || $pe_anio_fin != $periodo_lectivo_actual->pe_anio_fin)
    } // fin public function update($id)
        
}