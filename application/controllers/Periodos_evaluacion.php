<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Periodos_evaluacion extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model("Periodos_evaluacion_model");
    }

    public function index()
    {
        $data = array(
            'nombreInstitucion' => $this->Institucion_model->getInstitutionName(),
            'tipos_periodo' => $this->Periodos_evaluacion_model->getTiposPeriodoEvaluacion()
        );
        $this->load->view('layouts/header');
        $this->load->view('layouts/aside');
        $this->load->view('admin/periodos_evaluacion/list', $data);
        $this->load->view('layouts/footer');
    }

    public function getPeriodosEvaluacion()
    {
        $id_periodo_lectivo = $this->session->userdata("id_periodo_lectivo");
        echo json_encode($this->Periodos_evaluacion_model->getPeriodosEvaluacion($id_periodo_lectivo));
    }

    public function getPeriodoEvaluacion()
    {
        $id_periodo_evaluacion = $this->uri->segment(3);
        echo json_encode($this->Periodos_evaluacion_model->getPeriodoEvaluacion($id_periodo_evaluacion));
    }

    public function store()
    {
        $id_tipo_periodo = $this->input->post("id_tipo_periodo");
        $pe_nombre = $this->input->post("pe_nombre");
        $pe_abreviatura = $this->input->post("pe_abreviatura");
        $id_periodo_lectivo = $this->session->userdata("id_periodo_lectivo");

        $data = array(
            'pe_nombre'          => $pe_nombre,
            'pe_abreviatura'     => $pe_abreviatura,
            'id_tipo_periodo'    => $id_tipo_periodo,
            'id_periodo_lectivo' => $id_periodo_lectivo
        );
        if($this->Periodos_evaluacion_model->save($data)){
            $data = array(
                "titulo"       => "Operación exitosa.",
                "mensaje"      => "El Periodo de Evaluación fue insertado exitosamente.",
                "tipo_mensaje" => "success"
            );
            echo json_encode($data);
        }else{
            $data = array(
                "titulo"       => "Ocurrió un error inesperado.",
                "mensaje"      => "El Periodo de Evaluación no se pudo insertar...",
                "tipo_mensaje" => "error"
            );
            echo json_encode($data);
        }

    }

    public function update()
    {
        $id_periodo_evaluacion = $this->uri->segment(3);
        $periodoEvaluacionActual = $this->Periodos_evaluacion_model->getPeriodoEvaluacion($id_periodo_evaluacion);
        
        $pe_nombre = $this->input->post("pe_nombre");
        $pe_abreviatura = $this->input->post("pe_abreviatura");
        $id_tipo_periodo = $this->input->post("id_tipo_periodo");

        if($periodoEvaluacionActual->pe_nombre != $pe_nombre){
            if($this->Periodos_evaluacion_model->existeNombrePeriodoEvaluacion($pe_nombre, $id_periodo_evaluacion))
            {
                $data = array(
                    "titulo"       => "Ocurrió un error inesperado.",
                    "mensaje"      => "El Periodo de Evaluación digitado ya existe en la base de datos...",
                    "tipo_mensaje" => "error"
                );
                echo json_encode($data);
            }else{
                $data = array(
                    'pe_nombre'       => $pe_nombre,
                    'pe_abreviatura'  => $pe_abreviatura,
                    'id_tipo_periodo' => $id_tipo_periodo
                );
                if($this->Periodos_evaluacion_model->update($id_periodo_evaluacion, $data)){
                    $data = array(
                        "titulo"       => "Operación exitosa.",
                        "mensaje"      => "El Periodo de Evaluación fue actualizado exitosamente.",
                        "tipo_mensaje" => "success"
                    );
                    echo json_encode($data);
                }else{
                    $data = array(
                        "titulo"       => "Ocurrió un error inesperado.",
                        "mensaje"      => "El Periodo de Evaluación no pudo ser actualizado...",
                        "tipo_mensaje" => "error"
                    );
                    echo json_encode($data);
                }
            }
        }else{
            $data = array(
                'pe_nombre'       => $pe_nombre,
                'pe_abreviatura'  => $pe_abreviatura,
                'id_tipo_periodo' => $id_tipo_periodo
            );
            if($this->Periodos_evaluacion_model->update($id_periodo_evaluacion, $data)){
                $data = array(
                    "titulo"       => "Operación exitosa.",
                    "mensaje"      => "La Especialidad fue actualizada exitosamente.",
                    "tipo_mensaje" => "success"
                );
                echo json_encode($data);
            }else{
                $data = array(
                    "titulo"       => "Ocurrió un error inesperado.",
                    "mensaje"      => "La Especialidad no pudo ser actualizada...",
                    "tipo_mensaje" => "error"
                );
                echo json_encode($data);
            }
        }
    }

    public function delete()
    {
        $id_periodo_evaluacion = $this->uri->segment(3);
        if($this->Periodos_evaluacion_model->tieneAportesEvaluacion($id_periodo_evaluacion)){
            $data = array(
                "titulo"       => "No se puede eliminar el Periodo de Evaluación.",
                "mensaje"      => "Este Periodo de Evaluación tiene asociados Aportes de Evaluación...",
                "tipo_mensaje" => "error"
            );
            echo json_encode($data);
        }else{
            if($this->Periodos_evaluacion_model->delete($id_periodo_evaluacion)){
                $data = array(
                    "titulo"       => "Operación exitosa.",
                    "mensaje"      => "El Periodo de Evaluación fue eliminado exitosamente.",
                    "tipo_mensaje" => "success"
                );
                echo json_encode($data);
            }else{
                $data = array(
                    "titulo"       => "Ocurrió un error inesperado.",
                    "mensaje"      => "El Periodo de Evaluación no se pudo eliminar...",
                    "tipo_mensaje" => "error"
                );
                echo json_encode($data);
            }
        }
    }

}