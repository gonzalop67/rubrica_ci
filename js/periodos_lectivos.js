var base_url = $("#base_url").val();

function showAllPeriodosLectivos(){

    var request = $.ajax({
        url: base_url+"periodos_lectivos/getPeriodosLectivos",
        dataType: "json"
    });

    request.done(function(data){
        var html = '';
        var i;
        if(data.length==0){
            $("#tbody_periodos_lectivos").html("<tr><td colspan='6' align='center'>No se han definido Periodos Lectivos todavía...</td></tr>")
        }else{
            for(i = 0; i < data.length; i++){
                html += '<tr>' +
                            '<td>' + data[i].id_periodo_lectivo + '</td>' +
                            '<td>' + data[i].pe_anio_inicio + '</td>' +
                            '<td>' + data[i].pe_anio_fin + '</td>' +
                            '<td>' + data[i].pe_descripcion + '</td>' +
                            '<td>' + 
                                '<div class="btn-group">' +
                                    '<a href="javascript:;" class="btn btn-warning item-edit" data="' + data[i].id_periodo_lectivo + '" title="Editar"><span class="fa fa-pencil"></span></a>' +
                                    '<a href="javascript:;" class="btn btn-danger item-delete" data="' + data[i].id_periodo_lectivo + '" title="Cerrar"><span class="fa fa-remove"></span></a>' +
                                '</div>' +
                            '</td>' +
                        '</tr>';
            }
        }
        $("#tbody_periodos_lectivos").html(html);
    });

    request.fail(function(jqXHR, textStatus){
        alert("Requerimiento fallido: " + jqXHR.responseText);
    });
}

$("#btn-cancel").click(function(){
    $("#frm-periodo-lectivo")[0].reset(); 
    $("#titulo").html("Nuevo Periodo Lectivo");
    $("#btn-save").html("Guardar");
});

$('#tbody_periodos_lectivos').on('click', '.item-edit', function(){
    var id_periodo_lectivo = $(this).attr('data');
    $("#titulo").html("Editar Periodo Lectivo");
    $("#btn-save").html("Actualizar");
    $.ajax({
        url: base_url+"periodos_lectivos/getPeriodoLectivo/"+id_periodo_lectivo,
        dataType: "json",
        success: function(data){
            $("#id_periodo_lectivo").val(id_periodo_lectivo);
            $("#pe_anio_inicio").val(data.pe_anio_inicio);
            $("#pe_anio_fin").val(data.pe_anio_fin);
            $("#pe_fecha_inicio").val(data.pe_fecha_inicio);
        },
        error: function(jqXHR, textStatus){
            alert(jqXHR.responseText);
        }
    });
});

$('#tbody_periodos_lectivos').on('click', '.item-delete', function(){
    var id_periodo_lectivo = $(this).attr('data');
    swal({
        title: "¿Está seguro que quiere cerrar el periodo lectivo?",
        text: "No podrá reversar el cierre de los periodos de evaluacion!",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Sí, ciérrelo!"
    },
    function(){
        $.ajax({
            url: base_url+"periodos_lectivos/cerrar/"+id_periodo_lectivo,
            dataType: "json",
            success: function(data){
                swal({
                    title: data.titulo,
                    text: data.mensaje,
                    type: data.tipo_mensaje,
                    confirmButtonText: 'Aceptar'
                });

                showAllPeriodosLectivos();
                
                $("#frm-periodo-lectivo")[0].reset();
                
                if($("#btn-save").html()=="Actualizar"){
                    $("#btn-save").html("Guardar");
                    $("#titulo").html("Nuevo Periodo Lectivo");
                }
            },
            error: function(jqXHR, textStatus){
                alert(jqXHR.responseText);
            }
        });
    });
});

$("#frm-periodo-lectivo").submit(function(e){
    e.preventDefault();
    var url;
    var id_periodo_lectivo = $("#id_periodo_lectivo").val();
    var pe_anio_inicio = $.trim($("#pe_anio_inicio").val());
    var pe_anio_fin = $.trim($("#pe_anio_fin").val());
    var pe_fecha_inicio = $.trim($("#pe_fecha_inicio").val());

    if(pe_anio_inicio==""){
        swal("Ocurrió un error inesperado!", "Debe ingresar el año inicial.", "error");
    }else if(pe_anio_fin==""){
        swal("Ocurrió un error inesperado!", "Debe ingresar el año final.", "error");
    }else if(pe_fecha_inicio==""){
        swal("Ocurrió un error inesperado!", "Debe ingresar la fecha de inicio.", "error");
    }else if(pe_anio_inicio.length!=4){
        swal("Ocurrió un error inesperado!", "Debe ingresar los 4 dígitos para el año inicial.", "error");
    }else if(pe_anio_fin.length!=4){
        swal("Ocurrió un error inesperado!", "Debe ingresar los 4 dígitos para el año final.", "error");
    }else{
        
        if($("#btn-save").html()=="Guardar")
            url = base_url+"periodos_lectivos/store";
        else if($("#btn-save").html()=="Actualizar")
            url = base_url+"periodos_lectivos/update/"+id_periodo_lectivo;
            
        $.ajax({
            url: url,
            method: "post",
            data: {
                pe_anio_inicio: pe_anio_inicio,
                pe_anio_fin: pe_anio_fin,
                pe_fecha_inicio: pe_fecha_inicio
            },
            dataType: "json",
            success: function(response){

                swal({
                    title: response.titulo,
                    text: response.mensaje,
                    type: response.tipo_mensaje,
                    confirmButtonText: 'Aceptar'
                });

                showAllPeriodosLectivos();
                
                $("#frm-periodo-lectivo")[0].reset();

                if($("#btn-save").html()=="Actualizar"){
                    $("#btn-save").html("Guardar");
                    $("#titulo").html("Nuevo Periodo Lectivo");
                }

            },
            error: function(jqXHR, textStatus){
                alert(jqXHR.responseText);
            }
        });
    }

});