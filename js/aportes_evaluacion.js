$("#cboPeriodosEvaluacion").change(function(){
    if($(this).val()==0)
        $("#tbody_aportes_evaluacion").html("<tr><td colspan='4' align='center'>Debe seleccionar un periodo de evaluación...</td></tr>");
    else
        showAportesPeriodoEvaluacion($(this).val());
    $("#btn-save").html("Guardar");
    $("#ap_nombre").focus();
});

$("#btn-cancel").click(function(){
    $("#frm-aporte-evaluacion")[0].reset(); 
    $("#titulo").html("Nuevo Aporte de Evaluación");
    $("#btn-save").html("Guardar");
});

function showAportesPeriodoEvaluacion(id_periodo_evaluacion){
    var base_url = $("#base_url").val();

    var request = $.ajax({
        url: base_url+"aportes_evaluacion/getAportesPeriodoEvaluacion",
        method: "post",
        data: {id_periodo_evaluacion: id_periodo_evaluacion},
        dataType: "json"
    });

    request.done(function(data){
        var html = '';
        var i;
        if(data.length == 0) {
            html = "<tr><td colspan='4' align='center'>Aún no se han definido aportes de evaluación pare el periodo seleccionado...</td></tr>";
        } else {
            for(i = 0; i < data.length; i++) {
                html += '<tr>' +
                            '<td>' + data[i].id_aporte_evaluacion + '</td>' +
                            '<td>' + data[i].ap_nombre + '</td>' +
                            '<td>' + data[i].ap_abreviatura + '</td>' +                        
                            '<td>' +
                                '<div class="btn-group">' +
                                    '<a href="javascript:;" class="btn btn-warning item-edit" data="' + data[i].id_aporte_evaluacion + '" title="Editar"><span class="fa fa-pencil"></span></a>' +
                                    '<a href="javascript:;" class="btn btn-danger item-delete" data="' + data[i].id_aporte_evaluacion + '" title="Eliminar"><span class="fa fa-remove"></span></a>' +
                                '</div>' +
                            '</td>' +
                        '</tr>';
            }
        }
        $("#tbody_aportes_evaluacion").html(html);
    });

    request.fail(function(jqXHR, textStatus){
        alert("Requerimiento fallido: " + jqXHR.responseText);
    });
}

function setearIndice(nombreCombo,indice)
{
    for (var i=0;i<document.getElementById(nombreCombo).options.length;i++)
        if (document.getElementById(nombreCombo).options[i].value == indice) {
            document.getElementById(nombreCombo).options[i].selected = indice;
        }
}

$('#tbody_aportes_evaluacion').on('click', '.item-delete', function(){
    var base_url = $("#base_url").val();
    var id_aporte_evaluacion = $(this).attr('data');
    var id_periodo_evaluacion = $("#cboPeriodosEvaluacion").val();
    swal({
        title: "¿Está seguro que quiere eliminar el registro?",
        text: "No podrá recuperar el registro a ser eliminado!",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Sí, elimínelo!"
    },
    function(){
        $.ajax({
            url: base_url+"aportes_evaluacion/delete/"+id_aporte_evaluacion,
            dataType: "json",
            success: function(data){
                swal({
                    title: data.titulo,
                    text: data.mensaje,
                    type: data.tipo_mensaje,
                    confirmButtonText: 'Aceptar'
                });

                showAportesPeriodoEvaluacion(id_periodo_evaluacion);
                
                $("#frm-aporte-evaluacion")[0].reset();
                
                if($("#btn-save").html()=="Actualizar"){
                    $("#btn-save").html("Guardar");
                    $("#titulo").html("Nuevo Aporte de Evaluación");
                }
            },
            error: function(jqXHR, textStatus){
                alert(jqXHR.responseText);
            }
        });
    });
});

$('#tbody_aportes_evaluacion').on('click', '.item-edit', function(){
    var base_url = $("#base_url").val();
    var id_aporte_evaluacion = $(this).attr('data');
    var id_periodo_evaluacion = $("#cboPeriodosEvaluacion").val();
    $("#btn-save").html("Actualizar");
    $.ajax({
        url: base_url+"aportes_evaluacion/getAporteEvaluacion/"+id_aporte_evaluacion,
        dataType: "json",
        success: function(data){
            $("#id_aporte_evaluacion").val(id_aporte_evaluacion);
            $("#ap_nombre").val(data.ap_nombre);
            $("#ap_abreviatura").val(data.ap_abreviatura);
            $("#ap_fecha_apertura").val(data.ap_fecha_apertura);
            $("#ap_fecha_cierre").val(data.ap_fecha_cierre);
            setearIndice('cboTiposAporteEvaluacion', data.id_tipo_aporte);
        },
        error: function(jqXHR, textStatus){
            alert(jqXHR.responseText);
        }
    });
});

$("#frm-aporte-evaluacion").submit(function(e){
    e.preventDefault();
    var url;
    var id_aporte_evaluacion = $("#id_aporte_evaluacion").val();
    var base_url = $("#base_url").val();
    var id_periodo_evaluacion = $("#cboPeriodosEvaluacion").val();
    var ap_nombre = $.trim($("#ap_nombre").val());
    var ap_abreviatura = $.trim($("#ap_abreviatura").val());
    var ap_fecha_apertura = $("#ap_fecha_apertura").val();
    var ap_fecha_cierre = $("#ap_fecha_cierre").val();
    var id_tipo_aporte = $("#cboTiposAporteEvaluacion").val();

    if(id_periodo_evaluacion==0){
        swal("Ocurrió un error inesperado!", "Debe seleccionar un periodo de evaluación.", "error");
    }else if(ap_nombre==""){
        swal("Ocurrió un error inesperado!", "Debe ingresar el nombre del aporte de evaluación.", "error");
    }else if(ap_abreviatura==""){
        swal("Ocurrió un error inesperado!", "Debe ingresar la abreviatura del aporte de evaluación.", "error");
    }else if(ap_fecha_apertura==""){
        swal("Ocurrió un error inesperado!", "Debe ingresar la fecha inicial del aporte de evaluación.", "error");
    }else if(ap_fecha_cierre==""){
        swal("Ocurrió un error inesperado!", "Debe ingresar la fecha final del aporte de evaluación.", "error");
    }else{
        
        if($("#btn-save").html()=="Guardar")
            url = base_url+"aportes_evaluacion/store";
        else if($("#btn-save").html()=="Actualizar")
            url = base_url+"aportes_evaluacion/update/"+id_aporte_evaluacion;
            
        $.ajax({
            url: url,
            method: "post",
            data: {
                id_periodo_evaluacion: id_periodo_evaluacion,
                ap_nombre: ap_nombre,
                ap_abreviatura: ap_abreviatura,
                ap_fecha_apertura: ap_fecha_apertura,
                ap_fecha_cierre: ap_fecha_cierre,
                id_tipo_aporte: id_tipo_aporte
            },
            dataType: "json",
            success: function(response){

                swal({
                    title: response.titulo,
                    text: response.mensaje,
                    type: response.tipo_mensaje,
                    confirmButtonText: 'Aceptar'
                });

                showAportesPeriodoEvaluacion(id_periodo_evaluacion);

                $("#frm-aporte-evaluacion")[0].reset();

                if($("#btn-save").html()=="Actualizar"){
                    $("#btn-save").html("Guardar");
                    $("#titulo").html("Nuevo Aporte de Evaluación");
                }

            },
            error: function(jqXHR, textStatus){
                alert(jqXHR.responseText);
            }
        });
    }
});