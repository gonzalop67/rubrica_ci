var base_url = $("#base_url").val();

function showPeriodosEvaluacion(){

    var request = $.ajax({
        url: base_url+"periodos_evaluacion/getPeriodosEvaluacion",
        dataType: "json"
    });

    request.done(function(data){
        var html = '';
        var i;
        if(data.length==0){
            html = '<tr>' +
                        '<td colspan="4" align="center">Aún no se han definido periodos de evaluación...</td>' +
                    '</tr>';
        }else{
            for(i = 0; i < data.length; i++){
                html += '<tr>' +
                            '<td>' + data[i].id_periodo_evaluacion + '</td>' +
                            '<td>' + data[i].pe_nombre + '</td>' +
                            '<td>' + data[i].pe_abreviatura + '</td>' +
                            '<td>' + 
                                '<div class="btn-group">' +
                                    '<a href="javascript:;" class="btn btn-warning item-edit" data="' + data[i].id_periodo_evaluacion + '" title="Editar"><span class="fa fa-pencil"></span></a>' +
                                    '<a href="javascript:;" class="btn btn-danger item-delete" data="' + data[i].id_periodo_evaluacion + '" title="Eliminar"><span class="fa fa-remove"></span></a>' +
                                '</div>' +
                            '</td>' +
                        '</tr>';
            }
        }
        $("#tbody_periodos_evaluacion").html(html);
    });

    request.fail(function(jqXHR, textStatus){
        alert("Requerimiento fallido: " + jqXHR.responseText);
    });
}

function setearIndice(nombreCombo,indice)
{
    for (var i=0;i<document.getElementById(nombreCombo).options.length;i++)
        if (document.getElementById(nombreCombo).options[i].value == indice) {
            document.getElementById(nombreCombo).options[i].selected = indice;
        }
}

$("#btn-cancel").click(function(){
    $("#frm-periodo-evaluacion")[0].reset();
    $("#titulo").html("Nuevo Periodo de Evaluación");
    $("#btn-save").html("Guardar");
    $("#id_periodo_evaluacion").val(0);
});

$('#tbody_periodos_evaluacion').on('click', '.item-edit', function(){
    var id_periodo_evaluacion = $(this).attr('data');
    $("#titulo").html("Editar Periodo de Evaluacion");
    $("#btn-save").html("Actualizar");
    $.ajax({
        url: base_url+"periodos_evaluacion/getPeriodoEvaluacion/"+id_periodo_evaluacion,
        dataType: "json",
        success: function(data){
            $("#id_periodo_evaluacion").val(id_periodo_evaluacion);
            $("#pe_nombre").val(data.pe_nombre);
            $("#pe_abreviatura").val(data.pe_abreviatura);
            setearIndice("cboTiposPeriodoEvaluacion", data.id_tipo_periodo);
        },
        error: function(jqXHR, textStatus){
            alert(jqXHR.responseText);
        }
    });
});

$('#tbody_periodos_evaluacion').on('click', '.item-delete', function(){
    var id_periodo_evaluacion = $(this).attr('data');
    swal({
        title: "¿Está seguro que quiere eliminar el registro?",
        text: "No podrá recuperar el registro a ser eliminado!",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Sí, elimínelo!"
    },
    function(){
        $.ajax({
            url: base_url+"periodos_evaluacion/delete/"+id_periodo_evaluacion,
            dataType: "json",
            success: function(data){
                swal({
                    title: data.titulo,
                    text: data.mensaje,
                    type: data.tipo_mensaje,
                    confirmButtonText: 'Aceptar'
                });
                
                showPeriodosEvaluacion();
                
                $("#frm-periodo-evaluacion")[0].reset();
                
                if($("#btn-save").html()=="Actualizar"){
                    $("#btn-save").html("Guardar");
                    $("#titulo").html("Nuevo Periodo de Evaluación");
                }
            },
            error: function(jqXHR, textStatus){
                alert(jqXHR.responseText);
            }
        });
    });
});

$("#frm-periodo-evaluacion").submit(function(e){
    e.preventDefault();
    var url;
    var id_periodo_evaluacion = $("#id_periodo_evaluacion").val();
    var id_tipo_periodo = $("#cboTiposPeriodoEvaluacion").val();
    var pe_nombre = $.trim($("#pe_nombre").val());
    var pe_abreviatura = $.trim($("#pe_abreviatura").val());

    if(id_tipo_periodo==0){
        swal("Ocurrió un error inesperado!", "Debe seleccionar un tipo de periodo de evaluación.", "error");
    }else if(pe_nombre==""){
        swal("Ocurrió un error inesperado!", "Debe ingresar el nombre del periodo de evaluación.", "error");
    }else if(pe_abreviatura==""){
        swal("Ocurrió un error inesperado!", "Debe ingresar la abreviatura del periodo de evaluación.", "error");
    }else{
        
        if($("#btn-save").html()=="Guardar")
            url = base_url+"periodos_evaluacion/store";
        else if($("#btn-save").html()=="Actualizar")
            url = base_url+"periodos_evaluacion/update/"+id_periodo_evaluacion;
            
        $.ajax({
            url: url,
            method: "post",
            data: {
                id_tipo_periodo: id_tipo_periodo,
                pe_nombre: pe_nombre,
                pe_abreviatura: pe_abreviatura
            },
            dataType: "json",
            success: function(response){

                swal({
                    title: response.titulo,
                    text: response.mensaje,
                    type: response.tipo_mensaje,
                    confirmButtonText: 'Aceptar'
                });

                showPeriodosEvaluacion();
                
                $("#frm-periodo-evaluacion")[0].reset();

                if($("#btn-save").html()=="Actualizar"){
                    $("#btn-save").html("Guardar");
                    $("#titulo").html("Nuevo Periodo de Evaluación");
                }

                $("#id_periodo_evaluacion").val(0);

            },
            error: function(jqXHR, textStatus){
                alert(jqXHR.responseText);
            }
        });

    }
});
