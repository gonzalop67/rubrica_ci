var base_url = $("#base_url").val();

function showAllPerfiles(){
    // Aqui vamos a consultar todos los perfiles definidos en la base de datos
    var request = $.ajax({
        url: base_url+"perfiles/getPerfiles",
        method: "post",
        dataType: "json"
    });

    request.done(function(data){
        var html = '';
        var i;
        if(data.length==0){
            $("#tbody_perfiles").html("<tr><td colspan='4' align='center'>Aún no se han ingresado perfiles...</td></tr>");
        }else{
            for(i = 0; i < data.length; i++){
                html += '<tr>' +
                            '<td>' + data[i].id_perfil + '</td>' +
                            '<td>' + data[i].pe_nombre + '</td>' +
                            '<td>' + 
                                '<div class="btn-group">' + 
                                    '<a href="javascript:;" class="btn btn-warning item-edit" data="' + data[i].id_perfil + '" title="Editar"><span class="fa fa-pencil"></span></a>' +
                                    '<a href="javascript:;" class="btn btn-danger item-delete" data="' + data[i].id_perfil + '" title="Eliminar"><span class="fa fa-remove"></span></a>' +
                                '</div>' +
                            '</td>' +
                        '</tr>';
            }
            $("#tbody_perfiles").html(html);
        }
    });

    request.fail(function(jqXHR, textStatus){
        alert("Requerimiento fallido: " + jqXHR.responseText);
    });
}

$("#btn-cancel").click(function(){
    $("#frm-perfil")[0].reset(); 
    $("#titulo").html("Nuevo Perfil");
    $("#btn-save").html("Guardar");
});

$('#tbody_perfiles').on('click', '.item-edit', function(){
    var id_perfil = $(this).attr('data');
    $("#titulo").html("Editar Perfil");
    $("#btn-save").html("Actualizar");
    $.ajax({
        url: base_url+"perfiles/getPerfil/"+id_perfil,
        dataType: "json",
        success: function(data){
            $("#id_perfil").val(id_perfil);
            $("#pe_nombre").val(data.pe_nombre);
        },
        error: function(jqXHR, textStatus){
            alert(jqXHR.responseText);
        }
    });
});

$('#tbody_perfiles').on('click', '.item-delete', function(){
    var id_perfil = $(this).attr('data');
    swal({
        title: "¿Está seguro que quiere eliminar el registro?",
        text: "No podrá recuperar el registro que va a ser eliminado!",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Sí, elimínelo!"
    },
    function(){
        $.ajax({
            url: base_url+"perfiles/delete/"+id_perfil,
            dataType: "json",
            success: function(data){
                swal({
                    title: data.titulo,
                    text: data.mensaje,
                    type: data.tipo_mensaje,
                    confirmButtonText: 'Aceptar'
                });

                showAllPerfiles();
                
                $("#frm-perfil")[0].reset();
                
                if($("#btn-save").html()=="Actualizar"){
                    $("#btn-save").html("Guardar");
                    $("#titulo").html("Nuevo Perfil");
                }
            },
            error: function(jqXHR, textStatus){
                alert(jqXHR.responseText);
            }
        });
    });
});

$("#frm-perfil").submit(function(e){
    e.preventDefault();
    var url;
    var id_perfil = $("#id_perfil").val();
    var pe_nombre = $.trim($("#pe_nombre").val());

    if(pe_nombre==""){
        swal("Ocurrió un error inesperado!", "Debe ingresar el nombre del perfil.", "error");
    }else{
        
        if($("#btn-save").html()=="Guardar")
            url = base_url+"perfiles/store";
        else if($("#btn-save").html()=="Actualizar")
            url = base_url+"perfiles/update/"+id_perfil;
            
        $.ajax({
            url: url,
            method: "post",
            data: {
                pe_nombre: pe_nombre
            },
            dataType: "json",
            success: function(response){

                swal({
                    title: response.titulo,
                    text: response.mensaje,
                    type: response.tipo_mensaje,
                    confirmButtonText: 'Aceptar'
                });

                showAllPerfiles();
                
                $("#frm-perfil")[0].reset();

                if($("#btn-save").html()=="Actualizar"){
                    $("#btn-save").html("Guardar");
                    $("#titulo").html("Nuevo Perfil");
                }

            },
            error: function(jqXHR, textStatus){
                alert(jqXHR.responseText);
            }
        });
    }
});