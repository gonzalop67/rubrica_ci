var base_url = $("#base_url").val();

function showAllAreas(){
    // Aqui vamos a consultar todas las areas definidas en la base de datos
    var request = $.ajax({
        url: base_url+"areas/getAreas",
        method: "post",
        dataType: "json"
    });

    request.done(function(data){
        var html = '';
        var i;
        if(data.length==0){
            $("#tbody_areas").html("<tr><td colspan='4' align='center'>Aún no se han ingresado áreas...</td></tr>");
        }else{
            for(i = 0; i < data.length; i++){
                html += '<tr>' +
                            '<td>' + data[i].id_area + '</td>' +
                            '<td>' + data[i].ar_nombre + '</td>' +
                            '<td>' + 
                                '<div class="btn-group">' + 
                                    '<a href="javascript:;" class="btn btn-warning item-edit" data="' + data[i].id_area + '" title="Editar"><span class="fa fa-pencil"></span></a>' +
                                    '<a href="javascript:;" class="btn btn-danger item-delete" data="' + data[i].id_area + '" title="Eliminar"><span class="fa fa-remove"></span></a>' +
                                '</div>' +
                            '</td>' +
                        '</tr>';
            }
            $("#tbody_areas").html(html);
        }
    });

    request.fail(function(jqXHR, textStatus){
        alert("Requerimiento fallido: " + jqXHR.responseText);
    });
}

$("#btn-cancel").click(function(){
    $("#frm-area")[0].reset(); 
    $("#titulo").html("Nueva Area");
    $("#btn-save").html("Guardar");
});

$('#tbody_areas').on('click', '.item-edit', function(){
    var id_area = $(this).attr('data');
    $("#titulo").html("Editar Area");
    $("#btn-save").html("Actualizar");
    $.ajax({
        url: base_url+"areas/getArea/"+id_area,
        dataType: "json",
        success: function(data){
            $("#id_area").val(id_area);
            $("#ar_nombre").val(data.ar_nombre);
        },
        error: function(jqXHR, textStatus){
            alert(jqXHR.responseText);
        }
    });
});

$('#tbody_areas').on('click', '.item-delete', function(){
    var id_area = $(this).attr('data');
    swal({
        title: "¿Está seguro que quiere eliminar el registro?",
        text: "No podrá recuperar el registro que va a ser eliminado!",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Sí, elimínelo!"
    },
    function(){
        $.ajax({
            url: base_url+"areas/delete/"+id_area,
            dataType: "json",
            success: function(data){
                swal({
                    title: data.titulo,
                    text: data.mensaje,
                    type: data.tipo_mensaje,
                    confirmButtonText: 'Aceptar'
                });

                showAllAreas();
                
                $("#frm-area")[0].reset();
                
                if($("#btn-save").html()=="Actualizar"){
                    $("#btn-save").html("Guardar");
                    $("#titulo").html("Nueva Area");
                }
            },
            error: function(jqXHR, textStatus){
                alert(jqXHR.responseText);
            }
        });
    });
});

$("#frm-area").submit(function(e){
    e.preventDefault();
    var url;
    var id_area = $("#id_area").val();
    var ar_nombre = $.trim($("#ar_nombre").val());

    if(ar_nombre==""){
        swal("Ocurrió un error inesperado!", "Debe ingresar el nombre del área.", "error");
    }else{
        
        if($("#btn-save").html()=="Guardar")
            url = base_url+"areas/store";
        else if($("#btn-save").html()=="Actualizar")
            url = base_url+"areas/update/"+id_area;
            
        $.ajax({
            url: url,
            method: "post",
            data: {
                ar_nombre: ar_nombre
            },
            dataType: "json",
            success: function(response){

                swal({
                    title: response.titulo,
                    text: response.mensaje,
                    type: response.tipo_mensaje,
                    confirmButtonText: 'Aceptar'
                });

                showAllAreas();
                
                $("#frm-area")[0].reset();

                if($("#btn-save").html()=="Actualizar"){
                    $("#btn-save").html("Guardar");
                    $("#titulo").html("Nueva Area");
                }

            },
            error: function(jqXHR, textStatus){
                alert(jqXHR.responseText);
            }
        });
    }
});