var base_url = $("#base_url").val();

function showEspecialidadesPeriodoLectivo(){

    var request = $.ajax({
        url: base_url+"especialidades/getEspecialidadesPeriodoLectivo",
        dataType: "json"
    });

    request.done(function(data){
        var html = '';
        var i, subir_disabled, bajar_disabled;
        for(i = 0; i < data.length; i++){
            subir_disabled = i==0 ? ' disabled' : '';
            bajar_disabled = i==data.length - 1 ? ' disabled' : '';
            html += '<tr>' +
                        '<td>' + data[i].id_especialidad + '</td>' +
                        '<td>' + data[i].es_nombre + '</td>' +
                        '<td>' + data[i].es_figura + '</td>' +
                        '<td>' + 
                            '<div class="btn-group">' +
                                '<a href="javascript:;" class="btn btn-warning item-edit" data="' + data[i].id_especialidad + '" title="Editar"><span class="fa fa-pencil"></span></a>' +
                                '<a href="javascript:;" class="btn btn-danger item-delete" data="' + data[i].id_especialidad + '" title="Eliminar"><span class="fa fa-remove"></span></a>' +
                                '<a href="javascript:;" class="btn btn-info item-subir"' + subir_disabled + ' data="' + data[i].id_especialidad +'" title="Subir"><span class="fa fa-arrow-up"></span></a>' +
                                '<a href="javascript:;" class="btn btn-primary item-bajar"' + bajar_disabled + ' data="' + data[i].id_especialidad +'" title="Bajar"><span class="fa fa-arrow-down"></span></a>' +
                            '</div>' +
                        '</td>' +
                    '</tr>';
        }
        $("#tbody_especialidades").html(html);
    });

    request.fail(function(jqXHR, textStatus){
        alert("Requerimiento fallido: " + jqXHR.responseText);
    });
}

function setearIndice(nombreCombo,indice)
{
    for (var i=0;i<document.getElementById(nombreCombo).options.length;i++)
        if (document.getElementById(nombreCombo).options[i].value == indice) {
            document.getElementById(nombreCombo).options[i].selected = indice;
        }
}

$("#btn-cancel").click(function(){
    $("#frm-especialidad")[0].reset();
    $("#titulo").html("Nueva Especialidad");
    $("#btn-save").html("Guardar");
    $("#id_especialidad").val(0);
});

$('#tbody_especialidades').on('click', '.item-subir', function(){
    var id_especialidad = $(this).attr('data');
    $.ajax({
        url: base_url+"especialidades/subirEspecialidad/"+id_especialidad,
        success: function(response){

            console.log(response);

            swal({
                title: response.titulo,
                text: response.mensaje,
                type: response.tipo_mensaje,
                confirmButtonText: 'Aceptar'
            });

            showEspecialidadesPeriodoLectivo();
        
        },
        error: function(jqXHR, textStatus){
            alert(jqXHR.responseText);
            console.log(jqXHR.responseText);
        }
    });
});

$('#tbody_especialidades').on('click', '.item-bajar', function(){
    var id_especialidad = $(this).attr('data');
    $.ajax({
        url: base_url+"especialidades/bajarEspecialidad/"+id_especialidad,
        success: function(response){

            swal({
                title: response.titulo,
                text: response.mensaje,
                type: response.tipo_mensaje,
                confirmButtonText: 'Aceptar'
            });

            showEspecialidadesPeriodoLectivo();
        
        },
        error: function(jqXHR, textStatus){
            alert(jqXHR.responseText);
        }
    });
});

$('#tbody_especialidades').on('click', '.item-edit', function(){
    var id_especialidad = $(this).attr('data');
    $("#titulo").html("Editar Especialidad");
    $("#btn-save").html("Actualizar");
    $.ajax({
        url: base_url+"especialidades/getEspecialidad/"+id_especialidad,
        dataType: "json",
        success: function(data){
            $("#id_especialidad").val(id_especialidad);
            $("#es_nombre").val(data.es_nombre);
            $("#es_figura").val(data.es_figura);
            $("#es_abreviatura").val(data.es_abreviatura);
            setearIndice("cboNivelesEducacion", data.id_tipo_educacion);
        },
        error: function(jqXHR, textStatus){
            alert(jqXHR.responseText);
        }
    });
});

$('#tbody_especialidades').on('click', '.item-delete', function(){
    var id_especialidad = $(this).attr('data');
    swal({
        title: "¿Está seguro que quiere eliminar el registro?",
        text: "No podrá recuperar el registro a ser eliminado!",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Sí, elimínelo!"
    },
    function(){
        $.ajax({
            url: base_url+"especialidades/delete/"+id_especialidad,
            dataType: "json",
            success: function(data){
                swal({
                    title: data.titulo,
                    text: data.mensaje,
                    type: data.tipo_mensaje,
                    confirmButtonText: 'Aceptar'
                });
                
                showEspecialidadesPeriodoLectivo();
                
                $("#frm-menu")[0].reset();
                
                if($("#btn-save").html()=="Actualizar"){
                    $("#btn-save").html("Guardar");
                    $("#titulo").html("Nueva Especialidad");
                }
            },
            error: function(jqXHR, textStatus){
                alert(jqXHR.responseText);
            }
        });
    });
});

$("#frm-especialidad").submit(function(e){
    e.preventDefault();
    var url;
    var id_especialidad = $("#id_especialidad").val();
    var id_tipo_educacion = $("#cboNivelesEducacion").val();
    var es_nombre = $.trim($("#es_nombre").val());
    var es_figura = $.trim($("#es_figura").val());
    var es_abreviatura = $.trim($("#es_abreviatura").val());

    if(id_tipo_educacion==0){
        swal("Ocurrió un error inesperado!", "Debe seleccionar un nivel de educación.", "error");
    }else if(es_nombre==""){
        swal("Ocurrió un error inesperado!", "Debe ingresar el nombre de la especialidad.", "error");
    }else if(es_figura==""){
        swal("Ocurrió un error inesperado!", "Debe ingresar la figura profesional de la especialidad.", "error");
    }else if(es_abreviatura==""){
        swal("Ocurrió un error inesperado!", "Debe ingresar la abreviatura de la especialidad.", "error");
    }else{
        
        if($("#btn-save").html()=="Guardar")
            url = base_url+"especialidades/store";
        else if($("#btn-save").html()=="Actualizar")
            url = base_url+"especialidades/update/"+id_especialidad;
            
        $.ajax({
            url: url,
            method: "post",
            data: {
                id_tipo_educacion: id_tipo_educacion,
                es_nombre: es_nombre,
                es_figura: es_figura,
                es_abreviatura: es_abreviatura
            },
            dataType: "json",
            success: function(response){

                swal({
                    title: response.titulo,
                    text: response.mensaje,
                    type: response.tipo_mensaje,
                    confirmButtonText: 'Aceptar'
                });

                showEspecialidadesPeriodoLectivo();
                
                $("#frm-especialidad")[0].reset();

                if($("#btn-save").html()=="Actualizar"){
                    $("#btn-save").html("Guardar");
                    $("#titulo").html("Nueva Especialidad");
                }

                $("#id_especialidad").val(0);

            },
            error: function(jqXHR, textStatus){
                alert(jqXHR.responseText);
            }
        });

    }
});
