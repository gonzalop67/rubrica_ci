var base_url = $("#base_url").val();

function showAllNiveles()
{
    // Aqui vamos a consultar todos los niveles de educación definidos en la base de datos
    var request = $.ajax({
        url: base_url+"niveles_educacion/getNivelesEducacion",
        method: "post",
        dataType: "json"
    });

    request.done(function(data){
        var html = '';
        var i, es_bachillerato, subir_disabled, bajar_disabled;
        if(data.length==0){
            $("#tbody_niveles_educacion").html("<tr><td colspan='5' align='center'>No se han definido niveles de educación...</td></tr>");
        }else{
            for(i = 0; i < data.length; i++){
                es_bachillerato = data[i].te_bachillerato == 1 ? 'Sí' : 'No';
                subir_disabled = i==0 ? ' disabled' : '';
                bajar_disabled = i==data.length - 1 ? ' disabled' : '';
                html += '<tr>' +
                            '<td>' + data[i].id_tipo_educacion + '</td>' +
                            '<td>' + data[i].te_nombre + '</td>' +
                            '<td>' + es_bachillerato + '</td>' +
                            '<td><a href="javascript:;" class="btn btn-warning item-edit" data="' + data[i].id_tipo_educacion + '">Editar</a></td>' +
                            '<td><a href="javascript:;" class="btn btn-danger item-delete" data="' + data[i].id_tipo_educacion + '">Eliminar</a></td>' +
                            '<td><a href="javascript:;" class="btn btn-info item-subir"' + subir_disabled + ' data="' + data[i].id_tipo_educacion +'" title="Subir">Subir</span></a></td>' +
                            '<td><a href="javascript:;" class="btn btn-primary item-bajar"' + bajar_disabled + ' data="' + data[i].id_tipo_educacion +'" title="Bajar">Bajar</span></a></td>' +
                        '</tr>';
            }
            $("#tbody_niveles_educacion").html(html);
        }
    });

    request.fail(function(jqXHR, textStatus){
        alert("Requerimiento fallido: " + jqXHR.responseText);
    });
    
}

function setearIndice(nombreCombo,indice)
{
    for (var i=0;i<document.getElementById(nombreCombo).options.length;i++)
        if (document.getElementById(nombreCombo).options[i].value == indice) {
            document.getElementById(nombreCombo).options[i].selected = indice;
        }
}

$("#btn-cancel").click(function(){
    $("#frm-nivel-educacion")[0].reset();
    $("#titulo").html("Nuevo Nivel de Educación");
    $("#btn-save").html("Guardar");
    $("#id_tipo_educacion").val(0);
});

$('#tbody_niveles_educacion').on('click', '.item-edit', function(){
    var id_tipo_educacion = $(this).attr('data');
    $("#titulo").html("Editar Nivel de Educación");
    $("#btn-save").html("Actualizar");
    $.ajax({
        url: base_url+"niveles_educacion/getNivelEducacion/"+id_tipo_educacion,
        dataType: "json",
        success: function(data){
            $("#id_tipo_educacion").val(id_tipo_educacion);
            $("#te_nombre").val(data.te_nombre);
            setearIndice('te_bachillerato', data.te_bachillerato);
        },
        error: function(jqXHR, textStatus){
            alert(jqXHR.responseText);
        }
    });
});

$('#tbody_niveles_educacion').on('click', '.item-delete', function(){
    var id_tipo_educacion = $(this).attr('data');
    swal({
        title: "¿Está seguro que quiere eliminar el registro?",
        text: "No podrá recuperar el registro a ser eliminado!",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Sí, elimínelo!"
    },
    function(){
        $.ajax({
            url: base_url+"niveles_educacion/delete/"+id_tipo_educacion,
            dataType: "json",
            success: function(data){
                swal({
                    title: data.titulo,
                    text: data.mensaje,
                    type: data.tipo_mensaje,
                    confirmButtonText: 'Aceptar'
                });
                
                showAllNiveles()
                
                $("#frm-menu")[0].reset();
                
                if($("#btn-save").html()=="Actualizar"){
                    $("#btn-save").html("Guardar");
                    $("#titulo").html("Nuevo Menu");
                }
            },
            error: function(jqXHR, textStatus){
                alert(jqXHR.responseText);
            }
        });
    });
});

$('#tbody_niveles_educacion').on('click', '.item-subir', function(){
    var id_tipo_educacion = $(this).attr("data");
    $.ajax({
        url: base_url+"niveles_educacion/subirNivelEducacion",
        method: "post",
        data: {
            id_tipo_educacion: id_tipo_educacion
        },
        dataType: "json",
        success: function(data){
            swal({
                title: data.titulo,
                text: data.mensaje,
                type: data.tipo_mensaje,
                confirmButtonText: 'Aceptar'
            });
            showAllNiveles();
        },
        error: function(jqXHR, textStatus){
            alert(jqXHR.responseText);
        }
    });
});

$('#tbody_niveles_educacion').on('click', '.item-bajar', function(){
    var id_tipo_educacion = $(this).attr("data");
    $.ajax({
        url: base_url+"niveles_educacion/bajarNivelEducacion",
        method: "post",
        data: {
            id_tipo_educacion: id_tipo_educacion
        },
        dataType: "json",
        success: function(data){
            swal({
                title: data.titulo,
                text: data.mensaje,
                type: data.tipo_mensaje,
                confirmButtonText: 'Aceptar'
            });
            showAllNiveles();
        },
        error: function(jqXHR, textStatus){
            alert(jqXHR.responseText);
        }
    });
});

$("#frm-nivel-educacion").submit(function(e){
    e.preventDefault();
    var url;
    var id_tipo_educacion = $("#id_tipo_educacion").val();
    var te_nombre = $("#te_nombre").val();
    var te_bachillerato = $("#te_bachillerato").val();

    if(te_nombre.trim()==""){
        swal("Ocurrió un error inesperado!", "Debe ingresar el nombre del Nivel de Educación.", "error");
    }else{

        if($("#btn-save").html()=="Guardar")
            url = base_url+"niveles_educacion/store";
        else if($("#btn-save").html()=="Actualizar")
            url = base_url+"niveles_educacion/update";

        $.ajax({
            url: url,
            method: "post",
            data: {
                id_tipo_educacion: id_tipo_educacion,
                te_nombre: te_nombre,
                te_bachillerato: te_bachillerato,
            },
            dataType: "json",
            success: function(response){

                swal({
                    title: response.titulo,
                    text: response.mensaje,
                    type: response.tipo_mensaje,
                    confirmButtonText: 'Aceptar'
                });

                showAllNiveles();
                    
                $("#frm-nivel-educacion")[0].reset();

                if($("#btn-save").html()=="Actualizar"){
                    $("#btn-save").html("Guardar");
                    $("#titulo").html("Nuevo Nivel de Educación");
                }

            },
            error: function(jqXHR, textStatus){
                alert(jqXHR.responseText);
            }
        });
    }
});