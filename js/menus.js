$("#cboPerfiles").change(function(){
    var id_perfil = $(this).val();
    if(id_perfil==0)
        $("#tbody_menus").html("<tr><td colspan='8' align='center'>Debe seleccionar un perfil...</td></tr>");
    else
        showMenusPerfil(id_perfil);
    $("#btn-save").html("Guardar");
    $("#mnu_texto").focus();
});

$("#btn-cancel").click(function(){
    var mnu_padre = $("#mnu_padre").val();
    $("#frm-menu")[0].reset();
    if(mnu_padre==0) 
        $("#titulo").html("Nuevo Menu");
    else 
        $("#titulo").html("Nuevo SubMenu");
    $("#btn-save").html("Guardar");
});

function showMenusPerfil(id_perfil){
    var base_url = $("#base_url").val();

    var request = $.ajax({
        url: base_url+"menus/getMenusPerfil",
        method: "post",
        data: {id_perfil: id_perfil},
        dataType: "json"
    });

    request.done(function(data){
        var html = '';
        var i, publicado, subir_disabled, bajar_disabled;
        for(i = 0; i < data.length; i++){
            publicado = data[i].mnu_publicado == 1 ? 'Sí' : 'No';
            subir_disabled = i==0 ? ' disabled' : '';
            bajar_disabled = i==data.length - 1 ? ' disabled' : '';
            html += '<tr>' +
                        '<td>' + data[i].id_menu + '</td>' +
                        '<td>' + data[i].mnu_texto + '</td>' +
                        '<td>' + publicado + '</td>' +                        
                        '<td>' +
                            '<div class="btn-group">' +
                                '<a href="javascript:;" class="btn btn-success item-submenus" data="' + data[i].id_menu + '" data-mnu-texto="' + data[i].mnu_texto + '" title="Submenús"><span class="fa fa-list"></span></a>' +
                                '<a href="javascript:;" class="btn btn-warning item-edit" data="' + data[i].id_menu + '" title="Editar"><span class="fa fa-pencil"></span></a>' +
                                '<a href="javascript:;" class="btn btn-danger item-delete" data="' + data[i].id_menu + '" title="Eliminar"><span class="fa fa-remove"></span></a>' +
                                '<a href="javascript:;" class="btn btn-info item-subirmenu"' + subir_disabled + ' data="' + data[i].id_menu +'" title="Subir"><span class="fa fa-arrow-up"></span></a>' +
                                '<a href="javascript:;" class="btn btn-primary item-bajarmenu"' + bajar_disabled + ' data="' + data[i].id_menu +'" title="Bajar"><span class="fa fa-arrow-down"></span></a>' +
                            '</div>' +
                        '</td>' +
                    '</tr>';
        }
        $("#tbody_menus").html(html);
    });

    request.fail(function(jqXHR, textStatus){
        alert("Requerimiento fallido: " + jqXHR.responseText);
    });
}

$('#tbody_menus').on('click', '.item-subirmenu', function(){
    var base_url = $("#base_url").val();
    var id_menu = $(this).attr("data");
    var id_perfil = $("#cboPerfiles").val();
    var mnu_padre = $("#mnu_padre").val();
    var mnu_nivel = $("#mnu_nivel").val();
    $.ajax({
        url: base_url+"menus/subirMenu",
        method: "post",
        data: {
            id_menu: id_menu,
            id_perfil: id_perfil,
            mnu_nivel: mnu_nivel,
            mnu_padre: mnu_padre
        },
        dataType: "json",
        success: function(data){
            swal({
                title: data.titulo,
                text: data.mensaje,
                type: data.tipo_mensaje,
                confirmButtonText: 'Aceptar'
            });
            if(mnu_padre==0)
                showMenusPerfil(id_perfil);    
            else
                showSubMenus(mnu_padre);
        },
        error: function(jqXHR, textStatus){
            alert(jqXHR.responseText);
        }
    });
});

$('#tbody_menus').on('click', '.item-bajarmenu', function(){
    var base_url = $("#base_url").val();
    var id_menu = $(this).attr("data");
    var id_perfil = $("#cboPerfiles").val();
    var mnu_padre = $("#mnu_padre").val();
    var mnu_nivel = $("#mnu_nivel").val();
    $.ajax({
        url: base_url+"menus/bajarMenu",
        method: "post",
        data: {
            id_menu: id_menu,
            id_perfil: id_perfil,
            mnu_nivel: mnu_nivel,
            mnu_padre: mnu_padre
        },
        dataType: "json",
        success: function(data){
            swal({
                title: data.titulo,
                text: data.mensaje,
                type: data.tipo_mensaje,
                confirmButtonText: 'Aceptar'
            });
            if(mnu_padre==0)
                showMenusPerfil(id_perfil);    
            else
                showSubMenus(mnu_padre);
        },
        error: function(jqXHR, textStatus){
            alert(jqXHR.responseText);
        }
    });
});

function setearIndice(nombreCombo,indice)
{
    for (var i=0;i<document.getElementById(nombreCombo).options.length;i++)
        if (document.getElementById(nombreCombo).options[i].value == indice) {
            document.getElementById(nombreCombo).options[i].selected = indice;
        }
}

function showSubMenus(id_menu){
    var base_url = $("#base_url").val();

    var request = $.ajax({
        url: base_url+"menus/getSubmenus",
        method: "post",
        data: {id_menu: id_menu},
        dataType: "json"
    });

    request.done(function(data){
        var html = '';
        var i, publicado, subir_disabled, bajar_disabled;
        for(i = 0; i < data.length; i++){
            publicado = data[i].mnu_publicado == 1 ? 'Sí' : 'No';
            subir_disabled = i==0 ? ' disabled' : '';
            bajar_disabled = i==data.length - 1 ? ' disabled' : '';
            html += '<tr>' +
                        '<td>' + data[i].id_menu + '</td>' +
                        '<td>' + data[i].mnu_texto + '</td>' +
                        '<td>' + publicado + '</td>' +
                        '<td>' +
                            '<div class="btn-group">' +
                                '<a href="javascript:;" class="btn btn-warning item-edit" data="' + data[i].id_menu + '" title="Editar"><span class="fa fa-pencil"></span></a>' +
                                '<a href="javascript:;" class="btn btn-danger item-delete" data="' + data[i].id_menu + '" title="Eliminar"><span class="fa fa-remove"></span></a>' +
                                '<a href="javascript:;" class="btn btn-info item-subirmenu"' + subir_disabled + ' data="' + data[i].id_menu +'" title="Subir"><span class="fa fa-arrow-up"></span></a>' +
                                '<a href="javascript:;" class="btn btn-primary item-bajarmenu"' + bajar_disabled + ' data="' + data[i].id_menu +'" title="Bajar"><span class="fa fa-arrow-down"></span></a>' +
                            '</div>' +
                        '</td>' +
                    '</tr>';
        }
        $("#tbody_menus").html(html);
    });

    request.fail(function(jqXHR, textStatus){
        alert("Requerimiento fallido: " + jqXHR.responseText);
    });    
}

$('#tbody_menus').on('click', '.item-submenus', function(){
    var id_menu = $(this).attr('data');
    var mnu_texto = $(this).attr('data-mnu-texto');
    $("#id_menu").val(id_menu);
    $("#mnu_padre").val(id_menu);
    $("#mnu_nivel").val("2");
    $("#titulo_principal").html("SubMenus: "+mnu_texto);
    $("#cboPerfiles").prop("disabled", true);
    $("#titulo").html("Nuevo SubMenu");
    $("#btn-save").html("Guardar");
    showSubMenus(id_menu);
    $("#btn-regresar").html('<a id="back_to_menus" onclick="regresar()" href="javascript:;" class="btn btn-primary">Regresar a los menus</a>');
    $("#frm-menu")[0].reset();
});

function regresar(){
    var id_perfil = $("#cboPerfiles").val();
    $("#btn-regresar").html('');
    $("#titulo_principal").html("Menús");
    $("#cboPerfiles").prop("disabled", false);
    $("#titulo").html("Nuevo Menu");
    $("#mnu_padre").val("0");
    $("#mnu_nivel").val("1");
    $("#id_menu").val("0");
    showMenusPerfil(id_perfil);
}

$('#tbody_menus').on('click', '.item-delete', function(){
    var base_url = $("#base_url").val();
    var id_menu = $(this).attr('data');
    var id_perfil = $("#cboPerfiles").val();
    var mnu_padre = $("#mnu_padre").val();
    swal({
        title: "¿Está seguro que quiere eliminar el registro?",
        text: "No podrá recuperar el registro a ser eliminado!",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Sí, elimínelo!"
    },
    function(){
        $.ajax({
            url: base_url+"menus/delete/"+id_menu,
            dataType: "json",
            success: function(data){
                swal({
                    title: data.titulo,
                    text: data.mensaje,
                    type: data.tipo_mensaje,
                    confirmButtonText: 'Aceptar'
                });
                
                if(mnu_padre==0)
                    showMenusPerfil(id_perfil);    
                else
                    showSubMenus(mnu_padre);
                
                $("#frm-menu")[0].reset();
                
                if($("#btn-save").html()=="Actualizar"){
                    $("#btn-save").html("Guardar");
                    $("#titulo").html("Nuevo Menu");
                }
            },
            error: function(jqXHR, textStatus){
                alert(jqXHR.responseText);
            }
        });
    });
});

$('#tbody_menus').on('click', '.item-edit', function(){
    var base_url = $("#base_url").val();
    var id_menu = $(this).attr('data');
    var mnu_padre = $("#mnu_padre").val();
    if(mnu_padre==0)
        $("#titulo").html("Editar Menu");
    else
        $("#titulo").html("Editar SubMenu");
    $("#btn-save").html("Actualizar");
    $.ajax({
        url: base_url+"menus/getMenu/"+id_menu,
        dataType: "json",
        success: function(data){
            $("#id_menu").val(id_menu);
            $("#mnu_texto").val(data.mnu_texto);
            $("#mnu_enlace").val(data.mnu_enlace);
            setearIndice('mnu_publicado', data.mnu_publicado);
        },
        error: function(jqXHR, textStatus){
            alert(jqXHR.responseText);
        }
    });
});

$("#frm-menu").submit(function(e){
    e.preventDefault();
    var url;
    var id_menu = $("#id_menu").val();
    var base_url = $("#base_url").val();
    var id_perfil = $("#cboPerfiles").val();
    var mnu_texto = $.trim($("#mnu_texto").val());
    var mnu_enlace = $.trim($("#mnu_enlace").val());
    var mnu_publicado = $("#mnu_publicado").val();
    var mnu_padre = $("#mnu_padre").val();
    var mnu_nivel = $("#mnu_nivel").val();

    if(id_perfil==0){
        swal("Ocurrió un error inesperado!", "Debe seleccionar un perfil.", "error");
    }else if(mnu_texto==""){
        swal("Ocurrió un error inesperado!", "Debe ingresar el texto del menú.", "error");
    }else if(mnu_enlace==""){
        swal("Ocurrió un error inesperado!", "Debe ingresar el enlace del menú.", "error");
    }else{
        
        if($("#btn-save").html()=="Guardar")
            url = base_url+"menus/store";
        else if($("#btn-save").html()=="Actualizar")
            url = base_url+"menus/update/"+id_menu;
            
        $.ajax({
            url: url,
            method: "post",
            data: {
                id_perfil: id_perfil,
                mnu_texto: mnu_texto,
                mnu_enlace: mnu_enlace,
                mnu_publicado: mnu_publicado,
                mnu_padre: mnu_padre,
                mnu_nivel: mnu_nivel
            },
            dataType: "json",
            success: function(response){

                swal({
                    title: response.titulo,
                    text: response.mensaje,
                    type: response.tipo_mensaje,
                    confirmButtonText: 'Aceptar'
                });

                if(mnu_padre==0){
                    showMenusPerfil(id_perfil);
                }else{
                    showSubMenus(mnu_padre);
                }

                $("#frm-menu")[0].reset();

                if($("#btn-save").html()=="Actualizar"){
                    $("#btn-save").html("Guardar");
                    $("#titulo").html("Nuevo Menu");
                }

            },
            error: function(jqXHR, textStatus){
                alert(jqXHR.responseText);
            }
        });
    }
});