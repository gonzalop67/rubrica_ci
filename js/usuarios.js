var base_url = $("#base_url").val();

$("#cboPerfiles").change(function(){
    var id_perfil = $(this).val();
    if(id_perfil==0)
        $("#tbody_usuarios").html("<tr><td colspan='6' align='center'>Debe seleccionar un perfil...</td></tr>");
    else
        showUsuariosPerfil(id_perfil);
    $("#btn-save").html("Guardar");
    $("#us_titulo").focus();
});

$("#search_user").autocomplete({
    source:function(request,response){
        $.ajax({
            url: base_url + 'usuarios/searchUsers',
            type: 'POST',
            dataType: 'json',
            data: {valor: request.term},
            success: function(data){
                response(data);
            }
        });        
    },
    minLength:2,
    select:function(event,ui){
        $("#id_usuario").val(ui.item.id);
    },
});

function showUsuariosPerfil(id_perfil){
    $.ajax({
        url: base_url+"usuarios/getUsuariosPerfil",
        method: "post",
        data: {
            id_perfil: id_perfil
        },
        dataType: "json",
        success: function(data){
            var html = '';
            var i, activo;
            for(i = 0; i < data.length; i++){
                activo = data[i].us_activo == 1 ? 'Sí' : 'No';
                html += '<tr>' +
                            '<td>' + (i + 1) + '</td>' +
                            '<td>' + data[i].id_usuario + '</td>' +
                            '<td>' + data[i].us_login + '</td>' +
                            '<td>' + activo + '</td>' +
                            '<td><img src="' + base_url + 'assets/template/dist/img/' + data[i].us_foto + '" style="width:50px"></td>' +
                            '<td>' +
                                '<div class="btn-group">' + 
                                    '<a href="javascript:;" class="btn btn-warning item-edit" data="' + data[i].id_usuario + '" title="Editar"><span class="fa fa-pencil"></span></a>' +
                                    '<a href="javascript:;" class="btn btn-danger item-des-asociar" data="' + data[i].id_usuario + '" title="Des-Asociar"><span class="fa fa-remove"></span></a>' + 
                                '</div>' + 
                            '</td>' +
                        '</tr>';
            }
            $("#tbody_usuarios").html(html);
        },
        error: function(jqXHR, textStatus){
            alert(jqXHR.responseText);
        }
    });
}

function setearIndice(nombreCombo,indice)
{
    for (var i=0;i<document.getElementById(nombreCombo).options.length;i++)
        if (document.getElementById(nombreCombo).options[i].value == indice) {
            document.getElementById(nombreCombo).options[i].selected = indice;
        }
}

$("#btn-cancel").click(function(){
    $("#frm-usuario")[0].reset();
    $("#titulo").html("Nuevo Usuario");
    $("#btn-save").html("Guardar");
    $("#cboPerfiles").prop("disabled", false);
    $("#us_avatar").attr("src", base_url+"assets/template/dist/img/default-avatar.png");
    $("#btn-upload-image").addClass("hide");
    $("#id_usuario").val(0);
});

$("#btn-upload-image").click(function(){
    $("#frm-usuario").hide();
    $("#titulo").html("Cambiar Imagen");
    $("#frm-upload-image").removeClass("hide");
});

$("#btn-cancel-upload").click(function(){
    $("#frm-upload-image").addClass("hide");
    $("#frm-usuario").show();
    $("#titulo").html("Editar Usuario");
});

$('#tbody_usuarios').on('click', '.item-des-asociar', function(){
    var id_usuario = $(this).attr('data');
    var id_perfil = $("#cboPerfiles").val();
    swal({
        title: "¿Está seguro que quiere des-asociar el usuario?",
        text: "Tendrá que volver a asociar el usuario...",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Sí, estoy de acuerdo!"
    },
    function(){
        $.ajax({
            url: base_url+"usuarios/desasociarUsuarioPerfil",
            method: "post",
            data: {
                id_usuario: id_usuario,
                id_perfil: id_perfil
            },
            dataType: "json",
            success: function(response){
                swal({
                    title: response.titulo,
                    text: response.mensaje,
                    type: response.tipo_mensaje,
                    confirmButtonText: 'Aceptar'
                });

                showUsuariosPerfil(id_perfil);
            },
            error: function(jqXHR, textStatus){
                alert(jqXHR.responseText);
            }
        });
    });
});

$('#tbody_usuarios').on('click', '.item-edit', function(){
    var id_usuario = $(this).attr('data');
    $("#titulo").html("Editar Usuario");
    $("#btn-save").html("Actualizar");
    $("#cboPerfiles").prop("disabled", true);
    $("#frm-upload-image").addClass("hide");
    $("#frm-usuario").show();
    $("#btn-upload-image").removeClass("hide");
    $.ajax({
        url: base_url+"usuarios/getUsuario/"+id_usuario,
        dataType: "json",
        success: function(data){
            $("#id_usuario").val(id_usuario);
            $("#us_titulo").val(data.us_titulo);
            $("#us_apellidos").val(data.us_apellidos);
            $("#us_nombres").val(data.us_nombres);
            $("#us_login").val(data.us_login);
            $("#us_password").val(data.us_password);
            setearIndice('us_genero', data.us_genero);
            setearIndice('us_activo', data.us_activo);
            $("#us_avatar").attr("src", base_url+"assets/template/dist/img/"+data.us_foto);
        },
        error: function(jqXHR, textStatus){
            alert(jqXHR.responseText);
        }
    });
});

$("#frm-asociar").submit(function(e){
    e.preventDefault();
    var id_usuario = $("#id_usuario").val();
    var id_perfil = $("#cboPerfiles").val();
    if(id_perfil==0){
        swal("Ocurrió un error inesperado!", "Debe seleccionar un perfil.", "error");
    }else if(id_usuario==0){
        swal("Ocurrió un error inesperado!", "Debe \"buscar\" el usuario que va a ser asociado...", "error");
    }else{
        $.ajax({
            url: base_url+"usuarios/asociarUsuarioPerfil",
            method: "post",
            data: {
                id_usuario: id_usuario,
                id_perfil: id_perfil
            },
            dataType: "json",
            success: function(response){

                swal({
                    title: response.titulo,
                    text: response.mensaje,
                    type: response.tipo_mensaje,
                    confirmButtonText: 'Aceptar'
                });

                showUsuariosPerfil(id_perfil);
                
                $("#search_user").val("");

            },
            error: function(jqXHR, textStatus){
                alert(jqXHR.responseText);
            }
        });
    }
});

$("#frm-upload-image").submit(function(e){
    e.preventDefault();
    var id_usuario = $("#id_usuario").val();
    var url = base_url + "usuarios/upload_image/" + id_usuario;
    var id_perfil = $("#cboPerfiles").val();
    var data = new FormData(this);
    $.ajax({
        url: url,
        method: "POST",
        data: data,
        contentType: false,
        cache: false,
        processData: false,
        dataType: "json",
        success: function(response) {
            swal({
                title: response.titulo,
                text: response.mensaje,
                type: response.tipo_mensaje,
                confirmButtonText: 'Aceptar'
            });

            showUsuariosPerfil(id_perfil);

            $("#frm-usuario")[0].reset();
            $("#frm-upload-image").addClass("hide");
            $("#btn-save").html("Guardar");
            $("#btn-upload-image").addClass("hide");
            $("#frm-usuario").show();
            $("#titulo").html("Editar Usuario");

            $("#cboPerfiles").prop("disabled", false);
            $("#id_usuario").val(0);

        },
        error: function(jqXHR, textStatus){
            alert(jqXHR.responseText);
        }
    });
});

$("#frm-usuario").submit(function(e){
    e.preventDefault();
    var url;
    var id_perfil = $("#cboPerfiles").val();
    var id_usuario = $("#id_usuario").val();
    var us_titulo = $.trim($("#us_titulo").val());
    var us_apellidos = $.trim($("#us_apellidos").val());
    var us_nombres = $("#us_nombres").val();
    var us_login = $("#us_login").val();
    var us_password = $("#us_password").val();
    var us_genero = $("#us_genero").val();
    var us_activo = $("#us_activo").val();

    if(id_perfil==0){
        swal("Ocurrió un error inesperado!", "Debe seleccionar un perfil.", "error");
    }else if(us_titulo==""){
        swal("Ocurrió un error inesperado!", "Debe ingresar el título del usuario.", "error");
    }else if(us_apellidos==""){
        swal("Ocurrió un error inesperado!", "Debe ingresar los apellidos del usuario.", "error");
    }else if(us_nombres==""){
        swal("Ocurrió un error inesperado!", "Debe ingresar los nombres del usuario.", "error");
    }else if(us_login==""){
        swal("Ocurrió un error inesperado!", "Debe ingresar el nombre de usuario.", "error");
    }else if(us_password==""){
        swal("Ocurrió un error inesperado!", "Debe ingresar el password del usuario.", "error");
    }else{
        
        if($("#btn-save").html()=="Guardar")
            url = base_url+"usuarios/store";
        else if($("#btn-save").html()=="Actualizar")
            url = base_url+"usuarios/update/"+id_usuario;

        $.ajax({
            url: url,
            method: "POST",
            data: {
                id_perfil: id_perfil,
                id_usuario: id_usuario,
                us_titulo: us_titulo,
                us_apellidos: us_apellidos,
                us_nombres: us_nombres,
                us_login: us_login,
                us_password: us_password,
                us_genero: us_genero,
                us_activo: us_activo
            },
            dataType: "json",
            success: function(response){

                swal({
                    title: response.titulo,
                    text: response.mensaje,
                    type: response.tipo_mensaje,
                    confirmButtonText: 'Aceptar'
                });

                showUsuariosPerfil(id_perfil);
                
                $("#frm-usuario")[0].reset();

                if($("#btn-save").html()=="Actualizar"){
                    $("#btn-save").html("Guardar");
                    $("#titulo").html("Nuevo Usuario");
                    $("#btn-upload-image").addClass("hide");
                }

                // Recupero la imagen desde la base de datos
                //$.ajax({
                //    url: base_url+"usuarios/getImage",
                //    method: "post",
                //    data: {
                //        id_usuario: id_usuario
                //    },
                //    dataType: "json",
                //    success: function(response){
                //        $("#us_avatar").attr("src", response.imagen);
                //    }
                //});

                //$("#us_avatar").attr("src", base_url+"assets/template/dist/img/default-avatar.png");
                $("#cboPerfiles").prop("disabled", false);

                $("#id_usuario").val(0);

            },
            error: function(jqXHR, textStatus){
                alert(jqXHR.error);
            }
        });

    }
});