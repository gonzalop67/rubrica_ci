var base_url = $("#base_url").val();

function showAllAsignaturas(){
    // Aqui vamos a consultar todas las asignaturas definidas en la base de datos
    var request = $.ajax({
        url: base_url+"asignaturas/getAsignaturas",
        method: "post",
        dataType: "json"
    });

    request.done(function(data){
        var html = '';
        var i;
        if(data.length==0){
            $("#tbody_asignaturas").html("<tr><td colspan='5' align='center'>Aún no se han ingresado asignaturas...</td></tr>");
        }else{
            for(i = 0; i < data.length; i++){
                html += '<tr>' +
                            '<td>' + data[i].id_asignatura + '</td>' +
                            '<td>' + data[i].ar_nombre + '</td>' +
                            '<td>' + data[i].as_nombre + '</td>' +
                            '<td>' + data[i].as_abreviatura + '</td>' +
                            '<td>' + 
                                '<div class="btn-group">' + 
                                    '<a href="javascript:;" class="btn btn-warning item-edit" data="' + data[i].id_asignatura + '" title="Editar"><span class="fa fa-pencil"></span></a>' +
                                    '<a href="javascript:;" class="btn btn-danger item-delete" data="' + data[i].id_asignatura + '" title="Eliminar"><span class="fa fa-remove"></span></a>' +
                                '</div>' +
                            '</td>' +
                        '</tr>';
            }
            $("#tbody_asignaturas").html(html);
        }
    });

    request.fail(function(jqXHR, textStatus){
        alert("Requerimiento fallido: " + jqXHR.responseText);
    });
}

function load_data(query){
    // Aqui vamos a consultar las asignaturas que coincidan con el patron de busqueda
    var request = $.ajax({
        url: base_url+"asignaturas/searchAsignaturas",
        data: {
            query: query
        },
        method: "post",
        dataType: "json"
    });

    request.done(function(data){
        var html = '';
        var i;
        if(data.length==0){
            $("#tbody_asignaturas").html("<tr><td colspan='5' align='center'>No se han encontrado Asignaturas coincidentes con la búsqueda...</td></tr>");
        }else{
            for(i = 0; i < data.length; i++){
                html += '<tr>' +
                            '<td>' + data[i].id_asignatura + '</td>' +
                            '<td>' + data[i].ar_nombre + '</td>' +
                            '<td>' + data[i].as_nombre + '</td>' +
                            '<td>' + data[i].as_abreviatura + '</td>' +
                            '<td>' + 
                                '<div class="btn-group">' + 
                                    '<a href="javascript:;" class="btn btn-warning item-edit" data="' + data[i].id_asignatura + '" title="Editar"><span class="fa fa-pencil"></span></a>' +
                                    '<a href="javascript:;" class="btn btn-danger item-delete" data="' + data[i].id_asignatura + '" title="Eliminar"><span class="fa fa-remove"></span></a>' +
                                '</div>' +
                            '</td>' +
                        '</tr>';
            }
            $("#tbody_asignaturas").html(html);
        }
    });

    request.fail(function(jqXHR, textStatus){
        alert("Requerimiento fallido: " + jqXHR.responseText);
    });
}

$('#search_text').keyup(function(){
    var search = $(this).val();
    if(search != '')
    {
        load_data(search);
    }
    else
    {
        showAllAsignaturas();
    }
});

$("#btn-cancel").click(function(){
    $("#frm-asignatura")[0].reset(); 
    $("#titulo").html("Nueva Asignatura");
    $("#btn-save").html("Guardar");
});

function setearIndice(nombreCombo,indice)
{
    for (var i=0;i<document.getElementById(nombreCombo).options.length;i++)
        if (document.getElementById(nombreCombo).options[i].value == indice) {
            document.getElementById(nombreCombo).options[i].selected = indice;
        }
}

$('#tbody_asignaturas').on('click', '.item-edit', function(){
    var id_asignatura = $(this).attr('data');
    $("#titulo").html("Editar Asignatura");
    $("#btn-save").html("Actualizar");
    $.ajax({
        url: base_url+"asignaturas/getAsignatura/"+id_asignatura,
        dataType: "json",
        success: function(data){
            $("#id_asignatura").val(id_asignatura);
            $("#as_nombre").val(data.as_nombre);
            $("#as_abreviatura").val(data.as_abreviatura);
            setearIndice("cboAreas", data.id_area);
            setearIndice("cboTipos", data.id_tipo_asignatura);
        },
        error: function(jqXHR, textStatus){
            alert(jqXHR.responseText);
        }
    });
});

$('#tbody_asignaturas').on('click', '.item-delete', function(){
    var id_asignatura = $(this).attr('data');
    swal({
        title: "¿Está seguro que quiere eliminar el registro?",
        text: "No podrá recuperar el registro que va a ser eliminado!",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Sí, elimínelo!"
    },
    function(){
        $.ajax({
            url: base_url+"asignaturas/delete/"+id_asignatura,
            dataType: "json",
            success: function(data){
                swal({
                    title: data.titulo,
                    text: data.mensaje,
                    type: data.tipo_mensaje,
                    confirmButtonText: 'Aceptar'
                });

                showAllAsignaturas();
                
                $("#frm-asignatura")[0].reset();
                
                if($("#btn-save").html()=="Actualizar"){
                    $("#btn-save").html("Guardar");
                    $("#titulo").html("Nueva Asignatura");
                }
            },
            error: function(jqXHR, textStatus){
                alert(jqXHR.responseText);
            }
        });
    });
});

$("#frm-asignatura").submit(function(e){
    e.preventDefault();
    var url;
    var id_asignatura = $("#id_asignatura").val();
    var as_nombre = $.trim($("#as_nombre").val());
    var as_abreviatura = $.trim($("#as_abreviatura").val());
    var id_area = $("#cboAreas").val();
    var id_tipo_asignatura = $("#cboTipos").val();

    if(as_nombre==""){
        swal("Ocurrió un error inesperado!", "Debe ingresar el nombre de la asignatura.", "error");
    }else if(as_abreviatura==""){
        swal("Ocurrió un error inesperado!", "Debe ingresar la abreviatura de la asignatura.", "error");
    }else if(id_area=="0"){
        swal("Ocurrió un error inesperado!", "Debe seleccionar el área a la que pertenece la asignatura.", "error");
    }else if(id_tipo_asignatura=="0"){
        swal("Ocurrió un error inesperado!", "Debe seleccionar el tipo de la asignatura.", "error");
    }else{
        
        if($("#btn-save").html()=="Guardar")
            url = base_url+"asignaturas/store";
        else if($("#btn-save").html()=="Actualizar")
            url = base_url+"asignaturas/update/"+id_asignatura;
            
        $.ajax({
            url: url,
            method: "post",
            data: {
                as_nombre: as_nombre,
                as_abreviatura: as_abreviatura,
                id_area: id_area,
                id_tipo_asignatura: id_tipo_asignatura
            },
            dataType: "json",
            success: function(response){

                swal({
                    title: response.titulo,
                    text: response.mensaje,
                    type: response.tipo_mensaje,
                    confirmButtonText: 'Aceptar'
                });

                showAllAsignaturas();
                
                $("#frm-asignatura")[0].reset();

                if($("#btn-save").html()=="Actualizar"){
                    $("#btn-save").html("Guardar");
                    $("#titulo").html("Nueva Asignatura");
                }

            },
            error: function(jqXHR, textStatus){
                alert(jqXHR.responseText);
            }
        });
    }
});