var base_url = $("#base_url").val();

function showCursosPeriodoLectivo(){

    var request = $.ajax({
        url: base_url+"cursos/getCursosPeriodoLectivo",
        dataType: "json"
    });

    request.done(function(data){
        var html = '';
        var i, subir_disabled, bajar_disabled;
        if(data.length==0){
            html += '<tr><td colspan="4" align="center">No se han definido cursos todav&iacute;a...</tr></td>';
        }else{
            for(i = 0; i < data.length; i++){
                subir_disabled = i==0 ? ' disabled' : '';
                bajar_disabled = i==data.length - 1 ? ' disabled' : '';
                html += '<tr>' +
                            '<td>' + data[i].id_curso + '</td>' +
                            '<td>' + data[i].cu_nombre + '</td>' +
                            '<td>' + data[i].es_figura + '</td>' +
                            '<td>' + 
                                '<div class="btn-group">' +
                                    '<a href="javascript:;" class="btn btn-warning item-edit" data="' + data[i].id_curso + '" title="Editar"><span class="fa fa-pencil"></span></a>' +
                                    '<a href="javascript:;" class="btn btn-danger item-delete" data="' + data[i].id_curso + '" title="Eliminar"><span class="fa fa-remove"></span></a>' +
                                    '<a href="javascript:;" class="btn btn-info item-subir"' + subir_disabled + ' data="' + data[i].id_curso +'" title="Subir"><span class="fa fa-arrow-up"></span></a>' +
                                    '<a href="javascript:;" class="btn btn-primary item-bajar"' + bajar_disabled + ' data="' + data[i].id_curso +'" title="Bajar"><span class="fa fa-arrow-down"></span></a>' +
                                '</div>' +
                            '</td>' +
                        '</tr>';
            }
        }
        $("#tbody_cursos").html(html);
    });

    request.fail(function(jqXHR, textStatus){
        alert("Requerimiento fallido: " + jqXHR.responseText);
    });
}

function setearIndice(nombreCombo,indice)
{
    for (var i=0;i<document.getElementById(nombreCombo).options.length;i++)
        if (document.getElementById(nombreCombo).options[i].value == indice) {
            document.getElementById(nombreCombo).options[i].selected = indice;
        }
}

$("#btn-cancel").click(function(){
    $("#frm-curso")[0].reset();
    $("#titulo").html("Nuevo Curso");
    $("#btn-save").html("Guardar");
    $("#id_curso").val(0);
});

$('#tbody_cursos').on('click', '.item-subir', function(){
    var id_curso = $(this).attr('data');
    $.ajax({
        url: base_url+"cursos/subirCurso/"+id_curso,
        success: function(response){

            swal({
                title: response.titulo,
                text: response.mensaje,
                type: response.tipo_mensaje,
                confirmButtonText: 'Aceptar'
            });

            showCursosPeriodoLectivo();
        
        },
        error: function(jqXHR, textStatus){
            alert(jqXHR.responseText);
        }
    });
});

$('#tbody_cursos').on('click', '.item-bajar', function(){
    var id_curso = $(this).attr('data');
    $.ajax({
        url: base_url+"cursos/bajarCurso/"+id_curso,
        success: function(response){

            swal({
                title: response.titulo,
                text: response.mensaje,
                type: response.tipo_mensaje,
                confirmButtonText: 'Aceptar'
            });

            showCursosPeriodoLectivo();
        
        },
        error: function(jqXHR, textStatus){
            alert(jqXHR.responseText);
        }
    });
});

$('#tbody_cursos').on('click', '.item-edit', function(){
    var id_curso = $(this).attr('data');
    $("#titulo").html("Editar Curso");
    $("#btn-save").html("Actualizar");
    $.ajax({
        url: base_url+"cursos/getCurso/"+id_curso,
        dataType: "json",
        success: function(data){
            $("#id_curso").val(id_curso);
            $("#cu_nombre").val(data.cu_nombre);
            $("#cu_shortname").val(data.cu_shortname);
            $("#cu_abreviatura").val(data.cu_abreviatura);
            setearIndice("cboEspecialidades", data.id_especialidad);
        },
        error: function(jqXHR, textStatus){
            alert(jqXHR.responseText);
        }
    });
});

$('#tbody_cursos').on('click', '.item-delete', function(){
    var id_curso = $(this).attr('data');
    swal({
        title: "¿Está seguro que quiere eliminar el registro?",
        text: "No podrá recuperar el registro a ser eliminado!",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Sí, elimínelo!"
    },
    function(){
        $.ajax({
            url: base_url+"cursos/delete/"+id_curso,
            dataType: "json",
            success: function(data){
                swal({
                    title: data.titulo,
                    text: data.mensaje,
                    type: data.tipo_mensaje,
                    confirmButtonText: 'Aceptar'
                });
                
                showCursosPeriodoLectivo();
                
                $("#frm-curso")[0].reset();
                
                if($("#btn-save").html()=="Actualizar"){
                    $("#btn-save").html("Guardar");
                    $("#titulo").html("Nuevo Menu");
                }
            },
            error: function(jqXHR, textStatus){
                alert(jqXHR.responseText);
            }
        });
    });
});

$("#frm-curso").submit(function(e){
    e.preventDefault();
    var url;
    var id_curso = $("#id_curso").val();
    var id_especialidad = $("#cboEspecialidades").val();
    var cu_nombre = $.trim($("#cu_nombre").val());
    var cu_shortname = $.trim($("#cu_shortname").val());
    var cu_abreviatura = $.trim($("#cu_abreviatura").val());

    if(id_especialidad==0){
        swal("Ocurrió un error inesperado!", "Debe seleccionar una especialidad.", "error");
    }else if(cu_nombre==""){
        swal("Ocurrió un error inesperado!", "Debe ingresar el nombre del curso.", "error");
    }else if(cu_shortname==""){
        swal("Ocurrió un error inesperado!", "Debe ingresar el nombre corto del curso.", "error");
    }else if(cu_abreviatura==""){
        swal("Ocurrió un error inesperado!", "Debe ingresar la abreviatura del curso.", "error");
    }else{
        
        if($("#btn-save").html()=="Guardar")
            url = base_url+"cursos/store";
        else if($("#btn-save").html()=="Actualizar")
            url = base_url+"cursos/update/"+id_curso;
            
        $.ajax({
            url: url,
            method: "post",
            data: {
                id_especialidad: id_especialidad,
                cu_nombre: cu_nombre,
                cu_shortname: cu_shortname,
                cu_abreviatura: cu_abreviatura
            },
            dataType: "json",
            success: function(response){

                swal({
                    title: response.titulo,
                    text: response.mensaje,
                    type: response.tipo_mensaje,
                    confirmButtonText: 'Aceptar'
                });

                showCursosPeriodoLectivo();
                
                $("#frm-curso")[0].reset();

                if($("#btn-save").html()=="Actualizar"){
                    $("#btn-save").html("Guardar");
                    $("#titulo").html("Nuevo Curso");
                }

                $("#id_curso").val(0);

            },
            error: function(jqXHR, textStatus){
                alert(jqXHR.responseText);
                console.log(jqXHR.responseText);
            }
        });

    }
});
