var base_url = $("#base_url").val();

function showParalelosPeriodoLectivo(){

    var request = $.ajax({
        url: base_url+"paralelos/getParalelosPeriodoLectivo",
        dataType: "json"
    });

    request.done(function(data){
        var html = '';
        var i, subir_disabled, bajar_disabled;
        for(i = 0; i < data.length; i++){
            subir_disabled = i==0 ? ' disabled' : '';
            bajar_disabled = i==data.length - 1 ? ' disabled' : '';
            html += '<tr>' +
                        '<td>' + data[i].id_paralelo + '</td>' +
                        '<td>' + data[i].es_figura + '</td>' +
                        '<td>' + data[i].cu_nombre + '</td>' +
                        '<td>' + data[i].pa_nombre + '</td>' +
                        '<td>' + 
                            '<div class="btn-group">' +
                                '<a href="javascript:;" class="btn btn-warning item-edit" data="' + data[i].id_paralelo + '" title="Editar"><span class="fa fa-pencil"></span></a>' +
                                '<a href="javascript:;" class="btn btn-danger item-delete" data="' + data[i].id_paralelo + '" title="Eliminar"><span class="fa fa-remove"></span></a>' +
                                '<a href="javascript:;" class="btn btn-info item-subir"' + subir_disabled + ' data="' + data[i].id_paralelo +'" title="Subir"><span class="fa fa-arrow-up"></span></a>' +
                                '<a href="javascript:;" class="btn btn-primary item-bajar"' + bajar_disabled + ' data="' + data[i].id_paralelo +'" title="Bajar"><span class="fa fa-arrow-down"></span></a>' +
                            '</div>' +
                        '</td>' +
                    '</tr>';
        }
        $("#tbody_paralelos").html(html);
    });

    request.fail(function(jqXHR, textStatus){
        alert("Requerimiento fallido: " + jqXHR.responseText);
    });
}

function setearIndice(nombreCombo,indice)
{
    for (var i=0;i<document.getElementById(nombreCombo).options.length;i++)
        if (document.getElementById(nombreCombo).options[i].value == indice) {
            document.getElementById(nombreCombo).options[i].selected = indice;
        }
}

$("#btn-cancel").click(function(){
    $("#frm-paralelo")[0].reset();
    $("#titulo").html("Nuevo Paralelo");
    $("#btn-save").html("Guardar");
    $("#id_paralelo").val(0);
});

$('#tbody_paralelos').on('click', '.item-subir', function(){
    var id_paralelo = $(this).attr('data');
    $.ajax({
        url: base_url+"paralelos/subirParalelo/"+id_paralelo,
        success: function(response){
            showParalelosPeriodoLectivo();
        },
        error: function(jqXHR, textStatus){
            alert(jqXHR.responseText);
        }
    });
});

$('#tbody_paralelos').on('click', '.item-bajar', function(){
    var id_paralelo = $(this).attr('data');
    $.ajax({
        url: base_url+"paralelos/bajarParalelo/"+id_paralelo,
        success: function(response){

            swal({
                title: response.titulo,
                text: response.mensaje,
                type: response.tipo_mensaje,
                confirmButtonText: 'Aceptar'
            });

            showParalelosPeriodoLectivo();
        
        },
        error: function(jqXHR, textStatus){
            alert(jqXHR.responseText);
        }
    });
});

$('#tbody_paralelos').on('click', '.item-edit', function(){
    var id_paralelo = $(this).attr('data');
    $("#titulo").html("Editar Paralelo");
    $("#btn-save").html("Actualizar");
    $.ajax({
        url: base_url+"paralelos/getParalelo/"+id_paralelo,
        dataType: "json",
        success: function(data){
            $("#id_paralelo").val(id_paralelo);
            $("#pa_nombre").val(data.pa_nombre);
            setearIndice("cboCursos", data.id_curso);
        },
        error: function(jqXHR, textStatus){
            alert(jqXHR.responseText);
        }
    });
});

$('#tbody_paralelos').on('click', '.item-delete', function(){
    var id_paralelo = $(this).attr('data');
    swal({
        title: "¿Está seguro que quiere eliminar el registro?",
        text: "No podrá recuperar el registro a ser eliminado!",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Sí, elimínelo!"
    },
    function(){
        $.ajax({
            url: base_url+"paralelos/delete/"+id_paralelo,
            dataType: "json",
            success: function(data){
                swal({
                    title: data.titulo,
                    text: data.mensaje,
                    type: data.tipo_mensaje,
                    confirmButtonText: 'Aceptar'
                });
                
                showParalelosPeriodoLectivo();
                
                $("#frm-paralelo")[0].reset();
                
                if($("#btn-save").html()=="Actualizar"){
                    $("#btn-save").html("Guardar");
                    $("#titulo").html("Nuevo Paralelo");
                }
            },
            error: function(jqXHR, textStatus){
                alert(jqXHR.responseText);
            }
        });
    });
});

$("#frm-paralelo").submit(function(e){
    e.preventDefault();
    var url;
    var id_paralelo = $("#id_paralelo").val();
    var id_curso = $("#cboCursos").val();
    var pa_nombre = $.trim($("#pa_nombre").val());

    if(id_curso==0){
        swal("Ocurrió un error inesperado!", "Debe seleccionar un curso.", "error");
    }else if(pa_nombre==""){
        swal("Ocurrió un error inesperado!", "Debe ingresar el nombre del paralelo.", "error");
    }else{
        
        if($("#btn-save").html()=="Guardar")
            url = base_url+"paralelos/store";
        else if($("#btn-save").html()=="Actualizar")
            url = base_url+"paralelos/update/"+id_paralelo;
            
        $.ajax({
            url: url,
            method: "post",
            data: {
                id_curso: id_curso,
                pa_nombre: pa_nombre
            },
            dataType: "json",
            success: function(response){

                swal({
                    title: response.titulo,
                    text: response.mensaje,
                    type: response.tipo_mensaje,
                    confirmButtonText: 'Aceptar'
                });

                showParalelosPeriodoLectivo();
                
                $("#frm-paralelo")[0].reset();

                if($("#btn-save").html()=="Actualizar"){
                    $("#btn-save").html("Guardar");
                    $("#titulo").html("Nuevo Paralelo");
                }

                $("#id_paralelo").val(0);

            },
            error: function(jqXHR, textStatus){
                alert(jqXHR.responseText);
                console.log(jqXHR.responseText);
            }
        });

    }
});
