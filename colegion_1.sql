-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 14-11-2019 a las 18:18:38
-- Versión del servidor: 10.1.30-MariaDB
-- Versión de PHP: 7.2.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `colegion_1`
--
CREATE DATABASE IF NOT EXISTS `colegion_1` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `colegion_1`;

DELIMITER $$
--
-- Procedimientos
--
DROP PROCEDURE IF EXISTS `sp_actualizar_periodo_lectivo`$$
CREATE DEFINER=`colegion_1`@`localhost` PROCEDURE `sp_actualizar_periodo_lectivo` (IN `IdPeriodoLectivo` INT, IN `AnioInicial` INT, IN `AnioFinal` INT, IN `FechaInicial` INT)  NO SQL
UPDATE sw_periodo_lectivo
   SET pe_anio_inicio = AnioInicial,
       pe_anio_fin = AnioFinal,
       pe_fecha_inicio = FechaInicial
 WHERE id_periodo_lectivo = IdPeriodoLectivo$$

DROP PROCEDURE IF EXISTS `sp_actualizar_usuario`$$
CREATE DEFINER=`colegion_1`@`localhost` PROCEDURE `sp_actualizar_usuario` (IN `IdUsuario` INT, IN `IdPerfil` INT, IN `UsTitulo` VARCHAR(5), IN `UsApellidos` VARCHAR(32), IN `UsNombres` VARCHAR(32), IN `UsNombreCompleto` VARCHAR(64), IN `UsLogin` VARCHAR(24), IN `UsPassword` VARCHAR(64), IN `UsActivo` INT)  NO SQL
BEGIN
	UPDATE sw_usuario SET
	us_titulo = UsTitulo,
	us_apellidos = UsApellidos,
	us_nombres = UsNombres,
	us_fullname = UsNombreCompleto,
	us_login = UsLogin,
	us_password = UsPassword,
    us_activo = UsActivo
	WHERE id_usuario = IdUsuario;
END$$

DROP PROCEDURE IF EXISTS `sp_asociar_cierre_aporte_cursos`$$
CREATE DEFINER=`colegion_1`@`localhost` PROCEDURE `sp_asociar_cierre_aporte_cursos` (IN `IdPeriodoLectivo` INT, IN `IdAporteEvaluacion` INT)  NO SQL
BEGIN
    DECLARE done INT DEFAULT 0;
    DECLARE IdCurso INT;
    
    DECLARE cCursos CURSOR FOR
    SELECT id_curso 
      FROM sw_curso c, 
           sw_especialidad e, 
           sw_tipo_educacion t 
     WHERE c.id_especialidad = e.id_especialidad 
       AND e.id_tipo_educacion = t.id_tipo_educacion 
       AND t.id_periodo_lectivo = IdPeriodoLectivo 
     ORDER BY c.id_especialidad, id_curso ASC;
    
    DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;
    
    OPEN cCursos;
    
    Lazo: LOOP
    	FETCH cCursos INTO IdCurso;
        IF done THEN
        	CLOSE cCursos;
            LEAVE Lazo;
        END IF;
        
        IF (NOT EXISTS (SELECT * FROM sw_aporte_curso_cierre
                         WHERE id_aporte_evaluacion = IdAporteEvaluacion
                           AND id_curso = IdCurso)) 
                    THEN
        	INSERT INTO sw_aporte_curso_cierre
            SET id_aporte_evaluacion = IdAporteEvaluacion,
            	id_curso = IdCurso;
        END IF;
    END LOOP Lazo;
END$$

DROP PROCEDURE IF EXISTS `sp_calcular_prom_anual`$$
CREATE DEFINER=`colegion_1`@`localhost` PROCEDURE `sp_calcular_prom_anual` (IN `IdPeriodoLectivo` INT, IN `IdParalelo` INT)  BEGIN
    DECLARE done INT DEFAULT 0;
    DECLARE promedio_anual FLOAT;
    DECLARE IdEstudiante INT;
    
    DECLARE cEstudiantes CURSOR FOR
    SELECT id_estudiante
      FROM sw_estudiante_periodo_lectivo
     WHERE id_paralelo = IdParalelo;
    
    DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;
    
    OPEN cEstudiantes;
    
    Lazo: LOOP
    	FETCH cEstudiantes INTO IdEstudiante;
        IF done THEN
        	CLOSE cEstudiantes;
            LEAVE Lazo;
        END IF;
        
        SET promedio_anual = (SELECT calcular_promedio_general(
        						IdPeriodoLectivo, 
            					IdEstudiante, 
            					IdParalelo));
                                
        IF (EXISTS (SELECT * FROM sw_estudiante_prom_anual
                    WHERE id_paralelo = IdParalelo
                    AND id_estudiante = IdEstudiante
                    AND id_periodo_lectivo = IdPeriodoLectivo)) 
                    THEN
        	UPDATE sw_estudiante_prom_anual
            SET ea_promedio = promedio_anual
            WHERE id_paralelo = IdParalelo
            AND id_estudiante = IdEstudiante
            AND id_periodo_lectivo = IdPeriodoLectivo;
        ELSE
        	INSERT INTO sw_estudiante_prom_anual
            SET id_paralelo = IdParalelo,
            	id_estudiante = IdEstudiante,
                id_periodo_lectivo = IdPeriodoLectivo,
                ea_promedio = promedio_anual;
        END IF;
    END LOOP Lazo;
END$$

DROP PROCEDURE IF EXISTS `sp_calcular_prom_aporte`$$
CREATE DEFINER=`colegion_1`@`localhost` PROCEDURE `sp_calcular_prom_aporte` (IN `IdAporteEvaluacion` INT, IN `IdParalelo` INT)  BEGIN
    DECLARE done INT DEFAULT 0;
    DECLARE promedio_aporte FLOAT;
    DECLARE IdEstudiante INT;
    
    DECLARE cEstudiantes CURSOR FOR
    SELECT id_estudiante
      FROM sw_estudiante_periodo_lectivo
     WHERE id_paralelo = IdParalelo;
    
    DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;
    
    OPEN cEstudiantes;
    
    Lazo: LOOP
    	FETCH cEstudiantes INTO IdEstudiante;
        IF done THEN
        	CLOSE cEstudiantes;
            LEAVE Lazo;
        END IF;
        
        SET promedio_aporte = (SELECT calcular_prom_aporte_estudiante(
        						IdAporteEvaluacion, 
            					IdParalelo, 
            					IdEstudiante));
                                
        IF (EXISTS (SELECT * FROM sw_estudiante_promedio_parcial
                    WHERE id_paralelo = IdParalelo
                    AND id_estudiante = IdEstudiante
                    AND id_aporte_evaluacion = IdAporteEvaluacion)) 
                    THEN
        	UPDATE sw_estudiante_promedio_parcial
            SET ep_promedio = promedio_aporte
            WHERE id_paralelo = IdParalelo
            AND id_estudiante = IdEstudiante
            AND id_aporte_evaluacion = IdAporteEvaluacion;
        ELSE
        	INSERT INTO sw_estudiante_promedio_parcial
            SET id_paralelo = IdParalelo,
            	id_estudiante = IdEstudiante,
                id_aporte_evaluacion = IdAporteEvaluacion,
                ep_promedio = promedio_aporte;
        END IF;
    END LOOP Lazo;
END$$

DROP PROCEDURE IF EXISTS `sp_calcular_prom_quimestre`$$
CREATE DEFINER=`colegion_1`@`localhost` PROCEDURE `sp_calcular_prom_quimestre` (IN `IdPeriodoEvaluacion` INT, IN `IdParalelo` INT)  BEGIN
    DECLARE done INT DEFAULT 0;
    DECLARE promedio_quimestre FLOAT;
    DECLARE IdEstudiante INT;
    
    DECLARE cEstudiantes CURSOR FOR
    SELECT id_estudiante
      FROM sw_estudiante_periodo_lectivo
     WHERE id_paralelo = IdParalelo;
    
    DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;
    
    OPEN cEstudiantes;
    
    Lazo: LOOP
    	FETCH cEstudiantes INTO IdEstudiante;
        IF done THEN
        	CLOSE cEstudiantes;
            LEAVE Lazo;
        END IF;
        
        SET promedio_quimestre = (SELECT calcular_prom_quim_estudiante(
        						IdPeriodoEvaluacion, 
            					IdParalelo, 
            					IdEstudiante));
                                
        IF (EXISTS (SELECT * FROM sw_estudiante_prom_quimestral
                    WHERE id_paralelo = IdParalelo
                    AND id_estudiante = IdEstudiante
                    AND id_periodo_evaluacion = IdPeriodoEvaluacion)) 
                    THEN
        	UPDATE sw_estudiante_prom_quimestral
            SET eq_promedio = promedio_quimestre
            WHERE id_paralelo = IdParalelo
            AND id_estudiante = IdEstudiante
            AND id_periodo_evaluacion = IdPeriodoEvaluacion;
        ELSE
        	INSERT INTO sw_estudiante_prom_quimestral
            SET id_paralelo = IdParalelo,
            	id_estudiante = IdEstudiante,
                id_periodo_evaluacion = IdPeriodoEvaluacion,
                eq_promedio = promedio_quimestre;
        END IF;
    END LOOP Lazo;
END$$

DROP PROCEDURE IF EXISTS `sp_cerrar_periodo_lectivo`$$
CREATE DEFINER=`colegion_1`@`localhost` PROCEDURE `sp_cerrar_periodo_lectivo` (IN `IdPeriodoLectivo` INT)  NO SQL
BEGIN

	DECLARE done INT DEFAULT 0;
	DECLARE IdAporteEvaluacion INT;

	DECLARE cAportesEvaluacion CURSOR FOR 
		SELECT a.id_aporte_evaluacion
		  FROM sw_aporte_evaluacion a,
			   sw_periodo_evaluacion p,
			   sw_periodo_lectivo pl
		 WHERE a.id_periodo_evaluacion = p.id_periodo_evaluacion
		   AND p.id_periodo_lectivo = pl.id_periodo_lectivo
		   AND p.id_periodo_lectivo = IdPeriodoLectivo;

	DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;

	-- Actualizo el estado del periodo lectivo anterior
	UPDATE sw_periodo_lectivo
	   SET pe_estado = 'T',
           id_periodo_estado = 2
	 WHERE id_periodo_lectivo = IdPeriodoLectivo;

	-- Aqui actualizo a 'C' todos los periodos de evaluacion

	OPEN cAportesEvaluacion;

	REPEAT
		FETCH cAportesEvaluacion INTO IdAporteEvaluacion;
		UPDATE sw_aporte_curso_cierre
		   SET ap_estado = 'C'
		 WHERE id_aporte_evaluacion = IdAporteEvaluacion;
	UNTIL done END REPEAT;

	CLOSE cAportesEvaluacion;
	
END$$

DROP PROCEDURE IF EXISTS `sp_insertar_institucion`$$
CREATE DEFINER=`colegion_1`@`localhost` PROCEDURE `sp_insertar_institucion` (IN `In_nombre` VARCHAR(64), IN `In_direccion` VARCHAR(45), IN `In_telefono1` VARCHAR(12), IN `In_nom_rector` VARCHAR(45), IN `In_nom_secretario` VARCHAR(45))  NO SQL
BEGIN
	IF (EXISTS (SELECT * FROM sw_institucion)) THEN
		UPDATE sw_institucion
		SET in_nombre = In_nombre,
		in_direccion = In_direccion,
		in_telefono1 = In_telefono1,
		in_nom_rector = In_nom_rector,
		in_nom_secretario = In_nom_secretario;
	ELSE
		INSERT INTO sw_institucion
		SET in_nombre = In_nombre,
		in_direccion = In_direccion,
		in_telefono1 = In_telefono1,
		in_nom_rector = In_nom_rector,
		in_nom_secretario = In_nom_secretario;
	END IF;
END$$

DROP PROCEDURE IF EXISTS `sp_insertar_periodo_lectivo`$$
CREATE DEFINER=`colegion_1`@`localhost` PROCEDURE `sp_insertar_periodo_lectivo` (IN `AnioInicial` INT, IN `AnioFinal` INT, IN `FechaInicial` DATE)  NO SQL
BEGIN

	DECLARE done INT DEFAULT 0;
	DECLARE IdAporteEvaluacion INT;

	DECLARE cAportesEvaluacion CURSOR FOR 
		SELECT a.id_aporte_evaluacion
		  FROM sw_aporte_evaluacion a,
			   sw_periodo_evaluacion p,
			   sw_periodo_lectivo pl
		 WHERE a.id_periodo_evaluacion = p.id_periodo_evaluacion
		   AND p.id_periodo_lectivo = pl.id_periodo_lectivo
		   AND pl.pe_anio_inicio = AnioInicial - 1
		   AND a.ap_tipo < 3;

	DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;

	-- Primero debo verificar si hay un periodo lectivo anterior
	
	SET @IdPeriodoLectivoAnterior = (SELECT id_periodo_lectivo
                                      FROM sw_periodo_lectivo
                                     WHERE pe_anio_inicio = AnioInicial - 1);

	-- SELECT @IdPeriodoLectivoAnterior;

	IF @IdPeriodoLectivoAnterior IS NOT NULL THEN
		-- Actualizo el estado del periodo lectivo anterior
		UPDATE sw_periodo_lectivo
		   SET pe_estado = 'T',
               id_periodo_estado = 2
		 WHERE id_periodo_lectivo = @IdPeriodoLectivoAnterior;

		-- Aqui actualizo a 'C' todos los periodos de evaluacion
		-- menos el examen de gracia utilizando un cursor

		OPEN cAportesEvaluacion;

		REPEAT
			FETCH cAportesEvaluacion INTO IdAporteEvaluacion;
			UPDATE sw_aporte_curso_cierre
			   SET ap_estado = 'C'
			 WHERE id_aporte_evaluacion = IdAporteEvaluacion;
		UNTIL done END REPEAT;

		CLOSE cAportesEvaluacion;
	
	END IF;

	-- Finalmente inserto el nuevo periodo lectivo
	INSERT INTO sw_periodo_lectivo (id_periodo_estado,id_institucion,pe_anio_inicio, pe_anio_fin, pe_estado, pe_fecha_inicio)
	VALUES (1,1,AnioInicial, AnioFinal, 'A', FechaInicial);

END$$

DROP PROCEDURE IF EXISTS `sp_insertar_usuario`$$
CREATE DEFINER=`colegion_1`@`localhost` PROCEDURE `sp_insertar_usuario` (IN `IdPeriodoLectivo` INT, IN `IdPerfil` INT, IN `UsTitulo` VARCHAR(5), IN `UsApellidos` VARCHAR(32), IN `UsNombres` VARCHAR(32), IN `UsFullname` VARCHAR(64), IN `UsLogin` VARCHAR(24), IN `UsPassword` VARCHAR(64))  NO SQL
BEGIN
	DECLARE max_id INT;
	INSERT INTO sw_usuario (
		id_periodo_lectivo,
		id_perfil,
		us_titulo,
		us_apellidos,
		us_nombres,
		us_fullname,
		us_login,
		us_password,
        us_activo
	) VALUES (
		IdPeriodoLectivo,
		IdPerfil,
		UsTitulo,
		UsApellidos,
		UsNombres,
		UsFullname,
		UsLogin,
		UsPassword,
        1
	);
    SET max_id = (SELECT MAX(id_usuario) FROM sw_usuario);
    INSERT INTO sw_usuario_perfil SET id_usuario = max_id, id_perfil = IdPerfil;
END$$

--
-- Funciones
--
DROP FUNCTION IF EXISTS `aprueba_todas_asignaturas`$$
CREATE DEFINER=`colegion_1`@`localhost` FUNCTION `aprueba_todas_asignaturas` (`IdPeriodoLectivo` INT, `IdEstudiante` INT, `IdParalelo` INT) RETURNS TINYINT(1) NO SQL
BEGIN
	DECLARE IdAsignatura INT;
	DECLARE aprueba BOOL DEFAULT TRUE; -- variable de salida de la funcion
	DECLARE done INT DEFAULT 0;
	DECLARE promedio FLOAT DEFAULT 0;

	-- Aqui determino si el estudiante aprueba en todas las asignaturas
	DECLARE cAsignaturas CURSOR FOR
		SELECT a.id_asignatura
                  FROM sw_asignatura a, 
                       sw_asignatura_curso ac, 
                       sw_paralelo p 
                 WHERE a.id_asignatura = ac.id_asignatura 
                   AND p.id_curso = ac.id_curso 
                   AND a.id_tipo_asignatura = 1
                   AND p.id_paralelo = IdParalelo;
	
	DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;

	OPEN cAsignaturas;

	Lazo: LOOP
		FETCH cAsignaturas INTO IdAsignatura;
		IF done THEN
			CLOSE cAsignaturas;
			LEAVE Lazo;
		END IF;
		SET promedio = (SELECT calcular_promedio_anual(IdPeriodoLectivo,IdEstudiante,IdParalelo,IdAsignatura));
		IF promedio < 7 THEN
			SET done = 1;
			SET aprueba = FALSE;
		END IF;
	END LOOP Lazo;

	RETURN aprueba;
	
END$$

DROP FUNCTION IF EXISTS `aprueba_todos_remediales`$$
CREATE DEFINER=`colegion_1`@`localhost` FUNCTION `aprueba_todos_remediales` (`IdPeriodoLectivo` INT, `IdEstudiante` INT, `IdParalelo` INT) RETURNS TINYINT(4) NO SQL
BEGIN
	DECLARE IdAsignatura INT;
	DECLARE aprueba BOOL DEFAULT TRUE; -- variable de salida de la funcion
	DECLARE done INT DEFAULT 0;
	DECLARE promedio FLOAT DEFAULT 0;
	DECLARE examen_supletorio FLOAT DEFAULT 0;
	DECLARE examen_remedial FLOAT DEFAULT 0;

	-- Aqui determino si el estudiante aprueba en todas las asignaturas
	DECLARE cAsignaturas CURSOR FOR
		SELECT id_asignatura 
		  FROM sw_paralelo_asignatura 
		 WHERE id_paralelo = IdParalelo;
	
	DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;

	OPEN cAsignaturas;

	Lazo: LOOP
		FETCH cAsignaturas INTO IdAsignatura;
		IF done THEN
			CLOSE cAsignaturas;
			LEAVE Lazo;
		END IF;
		SET promedio = (SELECT calcular_promedio_anual(IdPeriodoLectivo,IdEstudiante,IdParalelo,IdAsignatura));
		IF (promedio >= 5 AND promedio < 7) AND (7 - promedio > 0.01) THEN -- tiene que rendir el examen supletorio
			SET examen_supletorio = (SELECT calcular_examen_supletorio(IdPeriodoLectivo,IdEstudiante,IdParalelo,IdAsignatura,2));
			IF examen_supletorio < 7 THEN
				-- tiene que rendir el examen remedial
				SET examen_remedial = (SELECT calcular_examen_supletorio(IdPeriodoLectivo,IdEstudiante,IdParalelo,IdAsignatura,3));
				IF examen_remedial < 7 THEN
					SET done = 1;
					SET aprueba = FALSE;
				END IF;
			END IF;
		ELSE 
			IF promedio > 0 AND promedio < 5 THEN -- tiene que rendir el examen remedial
				SET examen_remedial = (SELECT calcular_examen_supletorio(IdPeriodoLectivo,IdEstudiante,IdParalelo,IdAsignatura,3));
				IF examen_remedial < 7 THEN
					SET done = 1;
					SET aprueba = FALSE;
				END IF;
			END IF;
		END IF;
	END LOOP Lazo;

	RETURN aprueba;

END$$

DROP FUNCTION IF EXISTS `aprueba_todos_supletorios`$$
CREATE DEFINER=`colegion_1`@`localhost` FUNCTION `aprueba_todos_supletorios` (`IdPeriodoLectivo` INT, `IdEstudiante` INT, `IdParalelo` INT) RETURNS TINYINT(4) NO SQL
BEGIN
	DECLARE IdAsignatura INT;
	DECLARE aprueba BOOL DEFAULT TRUE; -- variable de salida de la funcion
	DECLARE done INT DEFAULT 0;
	DECLARE promedio FLOAT DEFAULT 0;
	DECLARE examen_supletorio FLOAT DEFAULT 0;

	-- Aqui determino si el estudiante aprueba en todas las asignaturas
	DECLARE cAsignaturas CURSOR FOR
		SELECT id_asignatura 
		  FROM sw_paralelo_asignatura 
		 WHERE id_paralelo = IdParalelo;
	
	DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;

	OPEN cAsignaturas;

	Lazo: LOOP
		FETCH cAsignaturas INTO IdAsignatura;
		IF done THEN
			CLOSE cAsignaturas;
			LEAVE Lazo;
		END IF;
		SET promedio = (SELECT calcular_promedio_anual(IdPeriodoLectivo,IdEstudiante,IdParalelo,IdAsignatura));
		IF (promedio >= 5 AND promedio < 7) AND (7 - promedio > 0.01) THEN -- tiene que rendir el examen supletorio
			SET examen_supletorio = (SELECT calcular_examen_supletorio(IdPeriodoLectivo,IdEstudiante,IdParalelo,IdAsignatura,2));
			IF examen_supletorio < 7 THEN
				SET done = 1;
				SET aprueba = FALSE;
			END IF;
		ELSE IF promedio < 5 THEN -- tiene que rendir el examen remedial
				SET done = 1;
				SET aprueba = FALSE;
			 END IF;
		END IF;
	END LOOP Lazo;

	RETURN aprueba;
	
END$$

DROP FUNCTION IF EXISTS `calcular_comp_anual`$$
CREATE DEFINER=`colegion_1`@`localhost` FUNCTION `calcular_comp_anual` (`IdPeriodoLectivo` INT, `IdEstudiante` INT, `IdParalelo` INT, `IdAsignatura` INT) RETURNS FLOAT NO SQL
BEGIN
	DECLARE done INT DEFAULT 0;     
	DECLARE promedio_quimestre FLOAT;
	DECLARE IdPeriodoEvaluacion INT;
	DECLARE Calificacion FLOAT;
	DECLARE Suma FLOAT DEFAULT 0;
	DECLARE Contador INT DEFAULT 0;
	DECLARE Promedio FLOAT DEFAULT 0;
	
	DECLARE cPeriodosEvaluacion CURSOR FOR
	SELECT id_periodo_evaluacion
	  FROM sw_periodo_evaluacion
	 WHERE id_periodo_lectivo = IdPeriodoLectivo
		   AND pe_principal = 1;

	DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;

	OPEN cPeriodosEvaluacion;

	Lazo: LOOP
		FETCH cPeriodosEvaluacion INTO IdPeriodoEvaluacion;
		IF done THEN
			CLOSE cPeriodosEvaluacion;
			LEAVE Lazo;
		END IF;
		SET promedio_quimestre = (SELECT calcular_comp_asignatura(IdPeriodoEvaluacion,IdEstudiante,IdParalelo,IdAsignatura));
		SET Suma = Suma + promedio_quimestre;
		SET Contador = Contador + 1;
	END LOOP Lazo;

	SELECT Suma / Contador INTO Promedio;

	RETURN Promedio;
END$$

DROP FUNCTION IF EXISTS `calcular_comp_asignatura`$$
CREATE DEFINER=`colegion_1`@`localhost` FUNCTION `calcular_comp_asignatura` (`IdPeriodoEvaluacion` INT, `IdEstudiante` INT, `IdParalelo` INT, `IdAsignatura` INT) RETURNS FLOAT NO SQL
BEGIN
	DECLARE done INT DEFAULT 0;     
	DECLARE promedio_aporte FLOAT;
	DECLARE IdAporteEvaluacion INT;
	DECLARE Calificacion FLOAT;
	DECLARE Suma FLOAT DEFAULT 0;
	DECLARE Contador INT DEFAULT 0;
	DECLARE Promedio FLOAT DEFAULT 0;
	
	DECLARE cAportesEvaluacion CURSOR FOR
	SELECT id_aporte_evaluacion
	  FROM sw_aporte_evaluacion
	 WHERE id_periodo_evaluacion = IdPeriodoEvaluacion
       AND ap_tipo = 1;

	DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;

	OPEN cAportesEvaluacion;

	Lazo1: LOOP
		FETCH cAportesEvaluacion INTO IdAporteEvaluacion;
		IF done THEN
			CLOSE cAportesEvaluacion;
			LEAVE Lazo1;
		END IF;
		
		SET Calificacion = (
		SELECT co_calificacion
		  FROM sw_calificacion_comportamiento
		 WHERE id_estudiante = IdEstudiante
		   AND id_paralelo = IdParalelo
		   AND id_asignatura = IdAsignatura
		   AND id_aporte_evaluacion = IdAporteEvaluacion);
           
        SET Calificacion = IFNULL(Calificacion, 0);

		SET Suma = Suma + Calificacion;
		SET Contador = Contador + 1;
	END LOOP Lazo1;

	SET Promedio = Suma / Contador;

	RETURN Promedio;
END$$

DROP FUNCTION IF EXISTS `calcular_comp_final`$$
CREATE DEFINER=`colegion_1`@`localhost` FUNCTION `calcular_comp_final` (`IdPeriodoLectivo` INT, `IdEstudiante` INT, `IdParalelo` INT) RETURNS FLOAT NO SQL
BEGIN
	DECLARE done INT DEFAULT 0;     
	DECLARE promedio_anual FLOAT;
	DECLARE Suma FLOAT DEFAULT 0;
	DECLARE Contador INT DEFAULT 0;
	DECLARE Promedio FLOAT DEFAULT 0;
    DECLARE IdAsignatura INT;
	
	DECLARE cAsignaturas CURSOR FOR
	SELECT a.id_asignatura
          FROM sw_asignatura a, 
               sw_asignatura_curso ac, 
               sw_paralelo p 
         WHERE a.id_asignatura = ac.id_asignatura 
           AND p.id_curso = ac.id_curso 
           AND p.id_paralelo = IdParalelo;

	DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;

	OPEN cAsignaturas;

	Lazo: LOOP
		FETCH cAsignaturas INTO IdAsignatura;
		IF done THEN
			CLOSE cAsignaturas;
			LEAVE Lazo;
		END IF;
		SET promedio_anual = (SELECT calcular_comp_anual(IdPeriodoLectivo,IdEstudiante,IdParalelo,IdAsignatura));
		SET Suma = Suma + promedio_anual;
		SET Contador = Contador + 1;
	END LOOP Lazo;

	SELECT Suma / Contador INTO Promedio;

	RETURN Promedio;
END$$

DROP FUNCTION IF EXISTS `calcular_comp_insp_quimestre`$$
CREATE DEFINER=`colegion_1`@`localhost` FUNCTION `calcular_comp_insp_quimestre` (`IdPeriodoEvaluacion` INT, `IdEstudiante` INT, `IdParalelo` INT) RETURNS FLOAT NO SQL
BEGIN
	DECLARE done INT DEFAULT 0;     
	DECLARE promedio_aporte FLOAT;
	DECLARE IdAporteEvaluacion INT;
	DECLARE Calificacion FLOAT;
	DECLARE Cualitativa VARCHAR(3);
	DECLARE Suma FLOAT DEFAULT 0;
	DECLARE Contador INT DEFAULT 0;
	DECLARE Promedio FLOAT DEFAULT 0;

	DECLARE cAportesEvaluacion CURSOR FOR
	SELECT id_aporte_evaluacion
	  FROM sw_aporte_evaluacion
	 WHERE id_periodo_evaluacion = IdPeriodoEvaluacion
       AND ap_tipo = 1;

	DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;

	OPEN cAportesEvaluacion;

	Lazo1: LOOP
		FETCH cAportesEvaluacion INTO IdAporteEvaluacion;
		IF done THEN
			CLOSE cAportesEvaluacion;
			LEAVE Lazo1;
		END IF;
		
		SET Cualitativa = (
		SELECT co_calificacion
		  FROM sw_comportamiento_inspector
		 WHERE id_estudiante = IdEstudiante
		   AND id_paralelo = IdParalelo
		   AND id_aporte_evaluacion = IdAporteEvaluacion);
           
        SET Cualitativa = IFNULL(Cualitativa, 'S/N');

		SET Calificacion = (
		SELECT ec_correlativa
		  FROM sw_escala_comportamiento
		 WHERE ec_equivalencia = Cualitativa);

		SET Suma = Suma + Calificacion;
		SET Contador = Contador + 1;
	END LOOP Lazo1;

	SET Promedio = Suma / Contador;

	RETURN Promedio;
END$$

DROP FUNCTION IF EXISTS `calcular_examen_supletorio`$$
CREATE DEFINER=`colegion_1`@`localhost` FUNCTION `calcular_examen_supletorio` (`IdPeriodoLectivo` INT, `IdEstudiante` INT, `IdParalelo` INT, `IdAsignatura` INT, `PePrincipal` INT) RETURNS FLOAT NO SQL
BEGIN
	DECLARE IdRubricaEvaluacion INT DEFAULT 0;
	DECLARE examen_supletorio FLOAT DEFAULT 0; -- variable de salida de la funcion

	-- Aqui obtengo el valor del examen supletorio, si existe
	SET IdRubricaEvaluacion = (SELECT id_rubrica_evaluacion 
								   FROM sw_rubrica_evaluacion r, 
									    sw_aporte_evaluacion a, 
										sw_periodo_evaluacion p 
								  WHERE r.id_aporte_evaluacion = a.id_aporte_evaluacion 
									AND a.id_periodo_evaluacion = p.id_periodo_evaluacion 
									AND p.pe_principal = PePrincipal AND p.id_periodo_lectivo = IdPeriodoLectivo);

	SET examen_supletorio = (SELECT re_calificacion
							   FROM sw_rubrica_estudiante 
							  WHERE id_estudiante = IdEstudiante 
								AND id_paralelo = IdParalelo 
								AND id_asignatura = IdAsignatura 
								AND id_rubrica_personalizada = IdRubricaEvaluacion);
	
	RETURN IFNULL(examen_supletorio, 0);
END$$

DROP FUNCTION IF EXISTS `calcular_promedio_anual`$$
CREATE DEFINER=`colegion_1`@`localhost` FUNCTION `calcular_promedio_anual` (`IdPeriodoLectivo` INT, `IdEstudiante` INT, `IdParalelo` INT, `IdAsignatura` INT) RETURNS FLOAT NO SQL
BEGIN
	DECLARE done INT DEFAULT 0;
	DECLARE promedio_anual FLOAT; -- variable de salida de la funcion
	DECLARE promedio_quimestre FLOAT;
	DECLARE IdPeriodoEvaluacion INT;
	DECLARE Suma FLOAT DEFAULT 0;
	DECLARE Contador INT DEFAULT 0;
	
	-- Aqui calculo el promedio anual utilizando un cursor
	DECLARE cPeriodosEvaluacion CURSOR FOR
		SELECT id_periodo_evaluacion
		  FROM sw_periodo_evaluacion 
		 WHERE id_periodo_lectivo = IdPeriodoLectivo
		   AND pe_principal = 1;

	DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;

	OPEN cPeriodosEvaluacion;

	Lazo: LOOP
		FETCH cPeriodosEvaluacion INTO IdPeriodoEvaluacion;
		IF done THEN
			CLOSE cPeriodosEvaluacion;
			LEAVE Lazo;
		END IF;
		SET promedio_quimestre = (SELECT calcular_promedio_quimestre(IdPeriodoEvaluacion,IdEstudiante,IdParalelo,IdAsignatura));
		SET Suma = Suma + promedio_quimestre;
		SET Contador = Contador + 1;
	END LOOP Lazo;

	SELECT Suma/Contador INTO promedio_anual;

	RETURN promedio_anual;
END$$

DROP FUNCTION IF EXISTS `calcular_promedio_anual_proyectos`$$
CREATE DEFINER=`colegion_1`@`localhost` FUNCTION `calcular_promedio_anual_proyectos` (`IdPeriodoLectivo` INT, `IdEstudiante` INT) RETURNS FLOAT NO SQL
BEGIN
	DECLARE done INT DEFAULT 0;
	DECLARE promedio_anual FLOAT; 	
	DECLARE promedio_quimestre FLOAT;
	DECLARE IdPeriodoEvaluacion INT;
	DECLARE Suma FLOAT DEFAULT 0;
	DECLARE Contador INT DEFAULT 0;
	DECLARE IdClub INT;
	
	DECLARE cPeriodosEvaluacion CURSOR FOR
	SELECT id_periodo_evaluacion
	  FROM sw_periodo_evaluacion 
	 WHERE id_periodo_lectivo = IdPeriodoLectivo
	   AND pe_principal = 1;

	DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;

	OPEN cPeriodosEvaluacion;

	-- Obtener el id_club correspondiente
	SET IdClub = (SELECT id_club FROM sw_estudiante_club
                   WHERE id_estudiante = IdEstudiante
                     AND id_periodo_lectivo = IdPeriodoLectivo);

	Lazo: LOOP
		FETCH cPeriodosEvaluacion INTO IdPeriodoEvaluacion;
		IF done THEN
			CLOSE cPeriodosEvaluacion;
			LEAVE Lazo;
		END IF;
		SET promedio_quimestre = (SELECT calcular_promedio_quimestre_club(
									IdPeriodoEvaluacion,IdEstudiante,IdClub));
		SET Suma = Suma + promedio_quimestre;
		SET Contador = Contador + 1;
	END LOOP Lazo;

	SELECT Suma / Contador INTO promedio_anual;

	RETURN promedio_anual;
END$$

DROP FUNCTION IF EXISTS `calcular_promedio_aporte`$$
CREATE DEFINER=`colegion_1`@`localhost` FUNCTION `calcular_promedio_aporte` (`IdAporteEvaluacion` INT, `IdEstudiante` INT, `IdParalelo` INT, `IdAsignatura` INT) RETURNS FLOAT NO SQL
BEGIN
	DECLARE done INT DEFAULT 0;
	DECLARE promedio_aporte FLOAT; 	DECLARE IdRubricaEvaluacion INT;
	DECLARE ReCalificacion FLOAT;
	DECLARE Suma FLOAT DEFAULT 0;
	DECLARE Contador INT DEFAULT 0;

	DECLARE cRubricasEvaluacion CURSOR FOR
	SELECT id_rubrica_evaluacion
	  FROM sw_rubrica_evaluacion r,
           sw_asignatura a
	 WHERE r.id_tipo_asignatura = a.id_tipo_asignatura
       AND a.id_asignatura = IdAsignatura
       AND id_aporte_evaluacion = IdAporteEvaluacion;

	DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;

	OPEN cRubricasEvaluacion;

	Lazo1: LOOP
		FETCH cRubricasEvaluacion INTO IdRubricaEvaluacion;
		IF done THEN
			CLOSE cRubricasEvaluacion;
			LEAVE Lazo1;
		END IF;

		SET ReCalificacion = (
		SELECT re_calificacion
		  FROM sw_rubrica_estudiante
		 WHERE id_estudiante = IdEstudiante
		   AND id_paralelo = IdParalelo
		   AND id_asignatura = IdAsignatura
		   AND id_rubrica_personalizada = IdRubricaEvaluacion);
           
        SET ReCalificacion = IFNULL(ReCalificacion, 0);

		SET Suma = Suma + ReCalificacion;
		SET Contador = Contador + 1;
	END LOOP Lazo1;

	SELECT Suma / Contador INTO promedio_aporte;
	
	RETURN promedio_aporte;
END$$

DROP FUNCTION IF EXISTS `calcular_promedio_aporte_club`$$
CREATE DEFINER=`colegion_1`@`localhost` FUNCTION `calcular_promedio_aporte_club` (`IdAporteEvaluacion` INT, `IdEstudiante` INT, `IdClub` INT) RETURNS FLOAT READS SQL DATA
    DETERMINISTIC
BEGIN
	DECLARE done INT DEFAULT 0;
	DECLARE promedio_aporte FLOAT;
    DECLARE IdRubricaEvaluacion INT;
	DECLARE ReCalificacion FLOAT;
	DECLARE Suma FLOAT DEFAULT 0;
	DECLARE Contador INT DEFAULT 0;

	DECLARE cRubricasEvaluacion CURSOR FOR
	SELECT id_rubrica_evaluacion
	  FROM sw_rubrica_evaluacion
	 WHERE id_aporte_evaluacion = IdAporteEvaluacion;

	DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;

	OPEN cRubricasEvaluacion;

	Lazo1: LOOP
		FETCH cRubricasEvaluacion INTO IdRubricaEvaluacion;
		IF done THEN
			CLOSE cRubricasEvaluacion;
			LEAVE Lazo1;
		END IF;

		SET ReCalificacion = (
		SELECT rc_calificacion
		  FROM sw_rubrica_club
		 WHERE id_estudiante = IdEstudiante
		   AND id_club = IdClub
		   AND id_rubrica_evaluacion = IdRubricaEvaluacion);
           
        SET ReCalificacion = IFNULL(ReCalificacion, 0);

		SET Suma = Suma + ReCalificacion;
		SET Contador = Contador + 1;
	END LOOP Lazo1;

	SELECT Suma / Contador INTO promedio_aporte;
	
	RETURN promedio_aporte;
END$$

DROP FUNCTION IF EXISTS `calcular_promedio_final`$$
CREATE DEFINER=`colegion_1`@`localhost` FUNCTION `calcular_promedio_final` (`IdPeriodoLectivo` INT, `IdEstudiante` INT, `IdParalelo` INT, `IdAsignatura` INT) RETURNS FLOAT NO SQL
BEGIN
	DECLARE promedio_final FLOAT DEFAULT 0; 	DECLARE examen_supletorio FLOAT DEFAULT 0;
	DECLARE examen_remedial FLOAT DEFAULT 0;
	DECLARE examen_de_gracia FLOAT DEFAULT 0;

	SET promedio_final = (SELECT calcular_promedio_anual(IdPeriodoLectivo,IdEstudiante,IdParalelo,IdAsignatura));
	IF promedio_final >= 5 AND promedio_final < 7 THEN 		
		SET examen_supletorio = (SELECT calcular_examen_supletorio(IdPeriodoLectivo,IdEstudiante,IdParalelo,IdAsignatura,2));
		IF examen_supletorio >= 7 THEN
			SET promedio_final = 7;
		ELSE
			SET examen_remedial = (SELECT calcular_examen_supletorio(IdPeriodoLectivo,IdEstudiante,IdParalelo,IdAsignatura,3));
			IF examen_remedial >= 7 THEN
				SET promedio_final = 7;
			ELSE
				SET examen_de_gracia = (SELECT calcular_examen_supletorio(IdPeriodoLectivo,IdEstudiante,IdParalelo,IdAsignatura,4));
				IF examen_de_gracia >= 7 THEN
					SET promedio_final = 7;
				END IF;
			END IF;
		END IF;
	ELSE 
		IF promedio_final > 0 AND promedio_final < 5 THEN
			SET examen_remedial = (SELECT calcular_examen_supletorio(IdPeriodoLectivo,IdEstudiante,IdParalelo,IdAsignatura,3));
			IF examen_remedial >= 7 THEN
				SET promedio_final = 7;
			ELSE
				SET examen_de_gracia = (SELECT calcular_examen_supletorio(IdPeriodoLectivo,IdEstudiante,IdParalelo,IdAsignatura,4));
				IF examen_de_gracia >= 7 THEN
					SET promedio_final = 7;
				END IF;
			END IF;
		END IF;
	END IF;

	RETURN promedio_final;

END$$

DROP FUNCTION IF EXISTS `calcular_promedio_general`$$
CREATE DEFINER=`colegion_1`@`localhost` FUNCTION `calcular_promedio_general` (`IdPeriodoLectivo` INT, `IdEstudiante` INT, `IdParalelo` INT) RETURNS FLOAT NO SQL
BEGIN
	DECLARE done INT DEFAULT 0;
	DECLARE promedio_general float DEFAULT 0; -- variable de salida de la funcion
	DECLARE suma FLOAT DEFAULT 0;
	DECLARE contador INT DEFAULT 0;
	DECLARE IdAsignatura INT;

	-- Aqui determino si el estudiante aprueba en todas las asignaturas
	DECLARE cAsignaturas CURSOR FOR
		SELECT id_asignatura 
		  FROM sw_paralelo_asignatura 
		 WHERE id_paralelo = IdParalelo;
	
	DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;

	OPEN cAsignaturas;

	Lazo: LOOP
		FETCH cAsignaturas INTO IdAsignatura;
		IF done THEN
			CLOSE cAsignaturas;
			LEAVE Lazo;
		END IF;
		SET suma = suma + (SELECT calcular_promedio_final(IdPeriodoLectivo,IdEstudiante,IdParalelo,IdAsignatura));
		SET contador = contador + 1;
	END LOOP Lazo;

	SET promedio_general = suma / contador;

	RETURN promedio_general;
END$$

DROP FUNCTION IF EXISTS `calcular_promedio_quimestre`$$
CREATE DEFINER=`colegion_1`@`localhost` FUNCTION `calcular_promedio_quimestre` (`IdPeriodoEvaluacion` INT, `IdEstudiante` INT, `IdParalelo` INT, `IdAsignatura` INT) RETURNS FLOAT NO SQL
BEGIN
    DECLARE done INT DEFAULT 0;
    DECLARE promedio_quimestre FLOAT; -- variable de salida de la funcion
    DECLARE promedio_aporte FLOAT;
    DECLARE IdAporteEvaluacion INT;
    DECLARE Suma FLOAT DEFAULT 0;
    DECLARE Contador INT DEFAULT 0;
    DECLARE Total_Aportes INT DEFAULT 0;
    DECLARE Examen FLOAT DEFAULT 0;
    DECLARE Promedio FLOAT DEFAULT 0;
    
    -- Declaracion del cursor que se va a utilizar
    DECLARE cAportesEvaluacion CURSOR FOR
    	SELECT id_aporte_evaluacion
          FROM sw_aporte_evaluacion
         WHERE id_periodo_evaluacion = IdPeriodoEvaluacion;
         
    DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;
    
    SET Total_Aportes = (SELECT COUNT(*) FROM sw_aporte_evaluacion WHERE id_periodo_evaluacion = IdPeriodoEvaluacion);
    
    OPEN cAportesEvaluacion;
    
    REPEAT
    	FETCH cAportesEvaluacion INTO IdAporteEvaluacion;
        
        SELECT calcular_promedio_aporte (IdAporteEvaluacion, IdEstudiante, IdParalelo, IdAsignatura) INTO promedio_aporte;
        
        SET Contador = Contador + 1;
        
        IF Contador <= Total_Aportes - 1 THEN
        	SET Suma = Suma + promedio_aporte;
        ELSE
        	SET Examen = promedio_aporte;
        END IF;
    UNTIL done END REPEAT;
    
    CLOSE cAportesEvaluacion;
    
    SET Promedio = Suma / (Total_Aportes - 1);
    
    SELECT 0.8 * Promedio + 0.2 * Examen INTO promedio_quimestre;
    
    RETURN promedio_quimestre;
    
END$$

DROP FUNCTION IF EXISTS `calcular_promedio_quimestre_club`$$
CREATE DEFINER=`colegion_1`@`localhost` FUNCTION `calcular_promedio_quimestre_club` (`IdPeriodoEvaluacion` INT, `IdEstudiante` INT, `IdClub` INT) RETURNS FLOAT NO SQL
BEGIN
    DECLARE done INT DEFAULT 0;
    DECLARE promedio_quimestre FLOAT;
	DECLARE promedio_aporte FLOAT;
    DECLARE IdAporteEvaluacion INT;
    DECLARE Suma FLOAT DEFAULT 0;
    DECLARE Contador INT DEFAULT 0;
    DECLARE Total_Aportes INT DEFAULT 0;
    DECLARE Examen FLOAT DEFAULT 0;
    DECLARE Promedio FLOAT DEFAULT 0;
    
        DECLARE cAportesEvaluacion CURSOR FOR
    	SELECT id_aporte_evaluacion
          FROM sw_aporte_evaluacion
         WHERE id_periodo_evaluacion = IdPeriodoEvaluacion;
         
    DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;
    
    SET Total_Aportes = (SELECT COUNT(*) FROM sw_aporte_evaluacion WHERE id_periodo_evaluacion = IdPeriodoEvaluacion);
    
    OPEN cAportesEvaluacion;
    
    REPEAT
    	FETCH cAportesEvaluacion INTO IdAporteEvaluacion;
        
        SELECT calcular_promedio_aporte_club (IdAporteEvaluacion, IdEstudiante, IdClub) INTO promedio_aporte;
        
        SET Contador = Contador + 1;
        
        IF Contador <= Total_Aportes - 1 THEN
        	SET Suma = Suma + promedio_aporte;
        ELSE
        	SET Examen = promedio_aporte;
        END IF;
    UNTIL done END REPEAT;
    
    CLOSE cAportesEvaluacion;
    
    SET Promedio = Suma / (Total_Aportes - 1);
    
    SELECT 0.8 * Promedio + 0.2 * Examen INTO promedio_quimestre;
    
    RETURN promedio_quimestre;
    
END$$

DROP FUNCTION IF EXISTS `calcular_prom_anual_estudiante`$$
CREATE DEFINER=`colegion_1`@`localhost` FUNCTION `calcular_prom_anual_estudiante` (`IdPeriodoLectivo` INT, `IdEstudiante` INT, `IdParalelo` INT) RETURNS FLOAT NO SQL
BEGIN
    DECLARE done INT DEFAULT 0;
    DECLARE promedio_anual FLOAT;     
    DECLARE promedio_quimestre FLOAT;
    DECLARE IdAsignatura INT;
    DECLARE Suma FLOAT DEFAULT 0;
    DECLARE Contador INT DEFAULT 0;
    
    DECLARE cAsignaturas CURSOR FOR
    SELECT id_asignatura
      FROM sw_asignatura_curso ac,
           sw_paralelo p
     WHERE p.id_curso = ac.id_curso
       AND id_paralelo = IdParalelo;
         
    DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;
    
    OPEN cAsignaturas;
    
    Lazo: LOOP
    	FETCH cAsignaturas INTO IdAsignatura;
        IF done THEN
        	CLOSE cAsignaturas;
            LEAVE Lazo;
        END IF;
        
        SET promedio_anual = (SELECT calcular_promedio_anual(
            						IdPeriodoLectivo, IdEstudiante,
            						IdParalelo, IdAsignatura));
        SET Suma = Suma + promedio_anual;
        SET Contador = Contador + 1;
    END LOOP Lazo;
    
    SELECT Suma / Contador INTO promedio_anual;
    
    RETURN promedio_anual;
    
END$$

DROP FUNCTION IF EXISTS `calcular_prom_aporte_estudiante`$$
CREATE DEFINER=`colegion_1`@`localhost` FUNCTION `calcular_prom_aporte_estudiante` (`IdAporteEvaluacion` INT, `IdParalelo` INT, `IdEstudiante` INT) RETURNS FLOAT NO SQL
BEGIN
    DECLARE done INT DEFAULT 0;
    DECLARE promedio_aporte FLOAT;
    DECLARE IdAsignatura INT;
    DECLARE Suma FLOAT DEFAULT 0;
    DECLARE Contador INT DEFAULT 0;
    DECLARE promedio_asignatura FLOAT;
    
    DECLARE cAsignaturas CURSOR FOR
    SELECT a.id_asignatura
          FROM sw_asignatura a, 
               sw_asignatura_curso ac, 
               sw_paralelo p 
         WHERE a.id_asignatura = ac.id_asignatura 
           AND p.id_curso = ac.id_curso 
           AND a.id_tipo_asignatura = 1
           AND p.id_paralelo = IdParalelo;
       
    DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;
    
    OPEN cAsignaturas;
    
    Lazo: LOOP
    	FETCH cAsignaturas INTO IdAsignatura;
        IF done THEN
        	CLOSE cAsignaturas;
            LEAVE Lazo;
        END IF;
        
        SET promedio_asignatura = (SELECT calcular_promedio_aporte(
            						IdAporteEvaluacion, IdEstudiante,
            						IdParalelo, IdAsignatura));
        SET Suma = Suma + promedio_asignatura;
        SET Contador = Contador + 1;
    END LOOP Lazo;
    
    SELECT Suma / Contador INTO promedio_aporte;
    
    RETURN promedio_aporte;
END$$

DROP FUNCTION IF EXISTS `calcular_prom_final_cualitativa`$$
CREATE DEFINER=`colegion_1`@`localhost` FUNCTION `calcular_prom_final_cualitativa` (`IdPeriodoLectivo` INT, `IdEstudiante` INT, `IdParalelo` INT, `IdAsignatura` INT) RETURNS FLOAT NO SQL
BEGIN
	DECLARE done INT DEFAULT 0;
	DECLARE prom_quim FLOAT;
	DECLARE Suma FLOAT DEFAULT 0;
	DECLARE Contador INT DEFAULT 0;
	DECLARE Promedio FLOAT DEFAULT 0;
	DECLARE IdPeriodoEval INT;

	DECLARE cPeriodos CURSOR FOR
	SELECT id_periodo_evaluacion
	  FROM sw_periodo_evaluacion
	 WHERE pe_principal = 1
	   AND id_periodo_lectivo = IdPeriodoLectivo;

	DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;

	OPEN cPeriodos;

	LAZO: LOOP
		FETCH cPeriodos INTO IdPeriodoEval;
		IF done THEN
			CLOSE cPeriodos;
			LEAVE LAZO;
		END IF;

		SET prom_quim = (
		SELECT calcular_prom_quim_cualitativa(
		  IdPeriodoEval,
		  IdEstudiante,
		  IdParalelo,
		  IdAsignatura));
           
		SET Suma = Suma + prom_quim;
		SET Contador = Contador + 1;
	END LOOP LAZO;

	SELECT Suma / Contador INTO Promedio;
	
	RETURN Promedio;
END$$

DROP FUNCTION IF EXISTS `calcular_prom_quim_cualitativa`$$
CREATE DEFINER=`colegion_1`@`localhost` FUNCTION `calcular_prom_quim_cualitativa` (`IdPeriodoEval` INT, `IdEstudiante` INT, `IdParalelo` INT, `IdAsignatura` INT) RETURNS FLOAT NO SQL
BEGIN
	DECLARE done INT DEFAULT 0;
	DECLARE Calificacion VARCHAR(2);
	DECLARE Cuantitativa FLOAT;
	DECLARE Suma FLOAT DEFAULT 0;
	DECLARE Contador INT DEFAULT 0;
	DECLARE Promedio FLOAT DEFAULT 0;
	DECLARE IdAporteEval INT;

	DECLARE cAportes CURSOR FOR
	SELECT id_aporte_evaluacion
	  FROM sw_aporte_evaluacion
	 WHERE ap_tipo = 1
	   AND id_periodo_evaluacion = IdPeriodoEval;

	DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;

	OPEN cAportes;

	LAZO: LOOP
		FETCH cAportes INTO IdAporteEval;
		IF done THEN
			CLOSE cAportes;
			LEAVE LAZO;
		END IF;

		SET Calificacion = (
		SELECT rc_calificacion
		  FROM sw_rubrica_cualitativa
		 WHERE id_asignatura = IdAsignatura
		   AND id_estudiante = IdEstudiante
		   AND id_paralelo = IdParalelo
		   AND id_aporte_evaluacion = IdAporteEval);
           
	    SET Calificacion = IFNULL(Calificacion, 'SN');

		SET Cuantitativa = (
		SELECT ec_correlativa
		  FROM sw_escala_proyectos
		 WHERE ec_abreviatura = Calificacion);

		SET Suma = Suma + Cuantitativa;
		SET Contador = Contador + 1;
	END LOOP LAZO;

	SELECT Suma / Contador INTO Promedio;
	
	RETURN Promedio;
END$$

DROP FUNCTION IF EXISTS `calcular_prom_quim_estudiante`$$
CREATE DEFINER=`colegion_1`@`localhost` FUNCTION `calcular_prom_quim_estudiante` (`IdPeriodoEvaluacion` INT, `IdParalelo` INT, `IdEstudiante` INT) RETURNS FLOAT NO SQL
BEGIN
    DECLARE done INT DEFAULT 0;
    DECLARE promedio_quimestre FLOAT;
    DECLARE IdAsignatura INT;
    DECLARE Suma FLOAT DEFAULT 0;
    DECLARE Contador INT DEFAULT 0;
    DECLARE promedio_asignatura FLOAT;
    
    DECLARE cAsignaturas CURSOR FOR
    SELECT a.id_asignatura
          FROM sw_asignatura a, 
               sw_asignatura_curso ac, 
               sw_paralelo p 
         WHERE a.id_asignatura = ac.id_asignatura 
           AND p.id_curso = ac.id_curso 
           AND a.id_tipo_asignatura = 1
           AND p.id_paralelo = IdParalelo;
       
    DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;
    
    OPEN cAsignaturas;
    
    Lazo: LOOP
    	FETCH cAsignaturas INTO IdAsignatura;
        IF done THEN
        	CLOSE cAsignaturas;
            LEAVE Lazo;
        END IF;
        
        SET promedio_asignatura = (SELECT calcular_promedio_quimestre(
            						IdPeriodoEvaluacion, IdEstudiante,
            						IdParalelo, IdAsignatura));
        SET Suma = Suma + promedio_asignatura;
        SET Contador = Contador + 1;
    END LOOP Lazo;
    
    SELECT Suma / Contador INTO promedio_quimestre;
    
    RETURN promedio_quimestre;
END$$

DROP FUNCTION IF EXISTS `calc_max_nro_matricula`$$
CREATE DEFINER=`colegion_1`@`localhost` FUNCTION `calc_max_nro_matricula` () RETURNS VARCHAR(4) CHARSET ascii NO SQL
BEGIN
	DECLARE max_nro_matricula VARCHAR(4);

	SET max_nro_matricula = (SELECT LPAD(MAX(es_nro_matricula)+1,4,'0') FROM sw_estudiante);
	RETURN IFNULL(max_nro_matricula,'0001');

END$$

DROP FUNCTION IF EXISTS `contar_remediales`$$
CREATE DEFINER=`colegion_1`@`localhost` FUNCTION `contar_remediales` (`IdPeriodoLectivo` INT, `IdEstudiante` INT, `IdParalelo` INT) RETURNS INT(11) NO SQL
BEGIN
	DECLARE IdAsignatura INT;
	DECLARE contador INT DEFAULT 0; 	
	DECLARE done INT DEFAULT 0;
	DECLARE promedio FLOAT DEFAULT 0;
	DECLARE supletorio FLOAT DEFAULT 0;

	DECLARE cAsignaturas CURSOR FOR
	SELECT id_asignatura 
	FROM sw_paralelo_asignatura 
	WHERE id_paralelo = IdParalelo;
	
	DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;

	OPEN cAsignaturas;

	Lazo: LOOP
		FETCH cAsignaturas INTO IdAsignatura;
		IF done THEN
			CLOSE cAsignaturas;
			LEAVE Lazo;
		END IF;
		SET promedio = (SELECT calcular_promedio_anual(IdPeriodoLectivo,IdEstudiante,IdParalelo,IdAsignatura));
		SET supletorio = (SELECT calcular_examen_supletorio(IdPeriodoLectivo,IdEstudiante,IdParalelo,IdAsignatura,2));

		IF (promedio > 0 AND promedio < 5) OR (promedio >= 5 AND promedio < 7 AND supletorio < 7) THEN 			
			SET contador = contador + 1;
		END IF;
	END LOOP Lazo;

	RETURN contador;

END$$

DROP FUNCTION IF EXISTS `contar_remediales_no_aprobados`$$
CREATE DEFINER=`colegion_1`@`localhost` FUNCTION `contar_remediales_no_aprobados` (`IdPeriodoLectivo` INT, `IdEstudiante` INT, `IdParalelo` INT) RETURNS INT(11) NO SQL
BEGIN
	DECLARE IdAsignatura INT;
	DECLARE contador INT DEFAULT 0; -- variable de salida de la funcion
	DECLARE done INT DEFAULT 0;
	DECLARE promedio FLOAT DEFAULT 0;
	DECLARE examen_supletorio FLOAT DEFAULT 0;
	DECLARE examen_remedial FLOAT DEFAULT 0;

	-- Aqui determino si el estudiante aprueba en todas las asignaturas
	DECLARE cAsignaturas CURSOR FOR
		SELECT id_asignatura 
		  FROM sw_paralelo_asignatura 
		 WHERE id_paralelo = IdParalelo;
	
	DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;

	OPEN cAsignaturas;

	Lazo: LOOP
		FETCH cAsignaturas INTO IdAsignatura;
		IF done THEN
			CLOSE cAsignaturas;
			LEAVE Lazo;
		END IF;
		SET promedio = (SELECT calcular_promedio_anual(IdPeriodoLectivo,IdEstudiante,IdParalelo,IdAsignatura));
		IF (promedio >= 5 AND promedio < 7) AND (7 - promedio > 0.01) THEN -- tiene que rendir el examen supletorio
			SET examen_supletorio = (SELECT calcular_examen_supletorio(IdPeriodoLectivo,IdEstudiante,IdParalelo,IdAsignatura,2));
			IF examen_supletorio < 7 THEN
				-- tiene que rendir el examen remedial
				SET examen_remedial = (SELECT calcular_examen_supletorio(IdPeriodoLectivo,IdEstudiante,IdParalelo,IdAsignatura,3));
				IF examen_remedial < 7 THEN
					SET contador = contador + 1;
				END IF;
			END IF;
		ELSE 
			IF promedio > 0 AND promedio < 5 THEN -- tiene que rendir el examen remedial
				SET examen_remedial = (SELECT calcular_examen_supletorio(IdPeriodoLectivo,IdEstudiante,IdParalelo,IdAsignatura,3));
				IF examen_remedial < 7 THEN
					SET contador = contador + 1;
				END IF;
			END IF;
		END IF;
	END LOOP Lazo;

	RETURN contador;

END$$

DROP FUNCTION IF EXISTS `contar_supletorios`$$
CREATE DEFINER=`colegion_1`@`localhost` FUNCTION `contar_supletorios` (`IdPeriodoLectivo` INT, `IdEstudiante` INT, `IdParalelo` INT) RETURNS INT(11) NO SQL
BEGIN
	DECLARE IdAsignatura INT;
	DECLARE contador INT DEFAULT 0; 	
	DECLARE done INT DEFAULT 0;
	DECLARE promedio FLOAT DEFAULT 0;

	DECLARE cAsignaturas CURSOR FOR
	SELECT id_asignatura 
	FROM sw_paralelo_asignatura 
	WHERE id_paralelo = IdParalelo;
	
	DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;

	OPEN cAsignaturas;

	Lazo: LOOP
		FETCH cAsignaturas INTO IdAsignatura;
		IF done THEN
			CLOSE cAsignaturas;
			LEAVE Lazo;
		END IF;
		SET promedio = (SELECT calcular_promedio_anual(IdPeriodoLectivo,IdEstudiante,IdParalelo,IdAsignatura));
		IF promedio > 5 AND promedio < 7 THEN 			
			SET contador = contador + 1;
		END IF;
	END LOOP Lazo;

	RETURN contador;

END$$

DROP FUNCTION IF EXISTS `determinar_asignatura_de_gracia`$$
CREATE DEFINER=`colegion_1`@`localhost` FUNCTION `determinar_asignatura_de_gracia` (`IdPeriodoLectivo` INT, `IdEstudiante` INT, `IdParalelo` INT) RETURNS INT(11) NO SQL
BEGIN
	DECLARE IdAsignatura INT;
	DECLARE vid_asignatura INT DEFAULT 0; -- variable de salida de la funcion
	DECLARE contador INT DEFAULT 0;
	DECLARE done INT DEFAULT 0;
	DECLARE promedio FLOAT DEFAULT 0;
	DECLARE examen_supletorio FLOAT DEFAULT 0;
	DECLARE examen_remedial FLOAT DEFAULT 0;

	-- Aqui determino si el estudiante aprueba en todas las asignaturas
	DECLARE cAsignaturas CURSOR FOR
		SELECT id_asignatura 
		  FROM sw_paralelo_asignatura 
		 WHERE id_paralelo = IdParalelo;
	
	DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;

	SET contador = (SELECT contar_remediales_no_aprobados(IdPeriodoLectivo,IdEstudiante,IdParalelo));

	IF contador = 1 THEN

		OPEN cAsignaturas;

		Lazo: LOOP
			FETCH cAsignaturas INTO IdAsignatura;
			IF done THEN
				CLOSE cAsignaturas;
				LEAVE Lazo;
			END IF;
			SET promedio = (SELECT calcular_promedio_anual(IdPeriodoLectivo,IdEstudiante,IdParalelo,IdAsignatura));
			IF promedio > 5 AND promedio < 7 THEN -- tiene que rendir el examen supletorio
				SET examen_supletorio = (SELECT calcular_examen_supletorio(IdPeriodoLectivo,IdEstudiante,IdParalelo,IdAsignatura,2));
				IF examen_supletorio < 7 THEN
					-- tiene que rendir el examen remedial
					SET examen_remedial = (SELECT calcular_examen_supletorio(IdPeriodoLectivo,IdEstudiante,IdParalelo,IdAsignatura,3));
					IF examen_remedial < 7 THEN
						SET vid_asignatura = IdAsignatura;
                        SET done = 1;
					END IF;
				END IF;
			ELSE 
				IF promedio > 0 AND promedio < 5 THEN -- tiene que rendir el examen remedial
					SET examen_remedial = (SELECT calcular_examen_supletorio(IdPeriodoLectivo,IdEstudiante,IdParalelo,IdAsignatura,3));
					IF examen_remedial < 7 THEN
						SET vid_asignatura = IdAsignatura;
                        SET done = 1;
					END IF;
				END IF;
			END IF;
		END LOOP Lazo;

	END IF;

	RETURN vid_asignatura;

END$$

DROP FUNCTION IF EXISTS `es_promocionado`$$
CREATE DEFINER=`colegion_1`@`localhost` FUNCTION `es_promocionado` (`IdEstudiante` INT, `IdPeriodoLectivo` INT, `IdParalelo` INT) RETURNS TINYINT(4) NO SQL
BEGIN
	DECLARE aprueba BOOL DEFAULT TRUE; -- variable de salida de la funcion
	-- DECLARE IdParalelo INT DEFAULT 0;
	DECLARE promedio FLOAT DEFAULT 0;
	DECLARE done INT DEFAULT 0;
	DECLARE IdAsignatura INT;

	DECLARE cAsignaturas CURSOR FOR
	 SELECT id_asignatura
	   FROM sw_paralelo_asignatura
	  WHERE id_paralelo = IdParalelo;

	DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;

	OPEN cAsignaturas;

	Lazo: LOOP
		FETCH cAsignaturas INTO IdAsignatura;
		IF done THEN
			CLOSE cAsignaturas;
			LEAVE Lazo;
		END IF;
		SET promedio = (SELECT calcular_promedio_final(IdPeriodoLectivo,IdEstudiante,IdParalelo,IdAsignatura));
		IF promedio < 7 THEN
			SET done = 1;
			SET aprueba = FALSE;
		END IF;
	END LOOP Lazo;

	RETURN aprueba;

END$$

DROP FUNCTION IF EXISTS `secuencial_curso_asignatura`$$
CREATE DEFINER=`colegion_1`@`localhost` FUNCTION `secuencial_curso_asignatura` (`IdCurso` INT) RETURNS INT(11) NO SQL
BEGIN
	DECLARE Secuencial INT;
	
	SET Secuencial = (
		SELECT MAX(ac_orden)
		  FROM sw_asignatura_curso
		 WHERE id_curso = IdCurso);
           
    SET Secuencial = IFNULL(Secuencial, 0);

	RETURN Secuencial + 1;
END$$

DROP FUNCTION IF EXISTS `secuencial_curso_especialidad`$$
CREATE DEFINER=`colegion_1`@`localhost` FUNCTION `secuencial_curso_especialidad` (`IdEspecialidad` INT) RETURNS INT(11) NO SQL
BEGIN

	DECLARE Secuencial INT;
	
	SET Secuencial = (
		SELECT MAX(cu_orden)
		  FROM sw_curso
		 WHERE id_especialidad = IdEspecialidad);
           
    SET Secuencial = IFNULL(Secuencial, 0);

	RETURN Secuencial + 1;

END$$

DROP FUNCTION IF EXISTS `secuencial_hora_clase_dia_semana`$$
CREATE DEFINER=`colegion_1`@`localhost` FUNCTION `secuencial_hora_clase_dia_semana` (`IdDiaSemana` INT) RETURNS INT(11) NO SQL
BEGIN

	DECLARE Secuencial INT;
	
	SET Secuencial = (
		SELECT MAX(hc_ordinal)
		  FROM sw_hora_clase
		 WHERE id_dia_semana = IdDiaSemana);
           
    SET Secuencial = IFNULL(Secuencial, 0);

	RETURN Secuencial + 1;

END$$

DROP FUNCTION IF EXISTS `secuencial_menu_nivel_perfil_padre`$$
CREATE DEFINER=`colegion_1`@`localhost` FUNCTION `secuencial_menu_nivel_perfil_padre` (`Nivel` INT, `IdPerfil` INT, `Padre` INT) RETURNS INT(11) NO SQL
BEGIN
	DECLARE Secuencial INT;
	
	SET Secuencial = (
		SELECT MAX(mnu_orden)
		  FROM sw_menu
		 WHERE id_perfil = IdPerfil
           AND mnu_nivel = Nivel
           AND mnu_padre = Padre);
           
    SET Secuencial = IFNULL(Secuencial, 0);

	RETURN Secuencial + 1;
END$$

DROP FUNCTION IF EXISTS `secuencial_paralelo_periodo_lectivo`$$
CREATE DEFINER=`colegion_1`@`localhost` FUNCTION `secuencial_paralelo_periodo_lectivo` (`IdPeriodoLectivo` INT) RETURNS INT(11) NO SQL
BEGIN

	DECLARE Secuencial INT;
	
	SET Secuencial = (
		SELECT MAX(pa_orden) AS secuencial 
          FROM sw_periodo_lectivo pe, 
               sw_paralelo p, 
               sw_curso c, 
               sw_especialidad e,
               sw_tipo_educacion te
         WHERE pe.id_periodo_lectivo = te.id_periodo_lectivo
           AND te.id_tipo_educacion = e.id_tipo_educacion
           AND e.id_especialidad = c.id_especialidad 
           AND c.id_curso = p.id_curso
           AND pe.id_periodo_lectivo = IdPeriodoLectivo);
           
    SET Secuencial = IFNULL(Secuencial, 0);

	RETURN Secuencial + 1;

END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_aporte_curso_cierre`
--

DROP TABLE IF EXISTS `sw_aporte_curso_cierre`;
CREATE TABLE `sw_aporte_curso_cierre` (
  `id_aporte_evaluacion` int(11) NOT NULL,
  `id_curso` int(11) NOT NULL,
  `ap_fecha_apertura` date NOT NULL,
  `ap_fecha_cierre` date NOT NULL,
  `ap_estado` varchar(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_aporte_evaluacion`
--

DROP TABLE IF EXISTS `sw_aporte_evaluacion`;
CREATE TABLE `sw_aporte_evaluacion` (
  `id_aporte_evaluacion` int(11) NOT NULL,
  `id_periodo_evaluacion` int(11) NOT NULL,
  `id_tipo_aporte` int(11) NOT NULL,
  `ap_nombre` varchar(24) CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL,
  `ap_shortname` varchar(45) NOT NULL,
  `ap_abreviatura` varchar(8) NOT NULL,
  `ap_tipo` tinyint(4) NOT NULL,
  `ap_estado` varchar(1) NOT NULL,
  `ap_fecha_apertura` date NOT NULL,
  `ap_fecha_cierre` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_area`
--

DROP TABLE IF EXISTS `sw_area`;
CREATE TABLE `sw_area` (
  `id_area` int(11) NOT NULL,
  `ar_nombre` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_asignatura`
--

DROP TABLE IF EXISTS `sw_asignatura`;
CREATE TABLE `sw_asignatura` (
  `id_asignatura` int(11) NOT NULL,
  `id_area` int(11) NOT NULL,
  `id_tipo_asignatura` int(11) NOT NULL,
  `as_nombre` varchar(84) COLLATE latin1_spanish_ci NOT NULL,
  `as_abreviatura` varchar(12) COLLATE latin1_spanish_ci NOT NULL,
  `as_shortname` varchar(45) COLLATE latin1_spanish_ci NOT NULL,
  `as_curricular` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_asignatura_curso`
--

DROP TABLE IF EXISTS `sw_asignatura_curso`;
CREATE TABLE `sw_asignatura_curso` (
  `id_asignatura_curso` int(11) NOT NULL,
  `id_curso` int(11) NOT NULL,
  `id_asignatura` int(11) NOT NULL,
  `id_periodo_lectivo` int(11) NOT NULL,
  `ac_orden` int(11) NOT NULL,
  `ac_num_horas` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_asistencia_estudiante`
--

DROP TABLE IF EXISTS `sw_asistencia_estudiante`;
CREATE TABLE `sw_asistencia_estudiante` (
  `id_asistencia_estudiante` int(11) NOT NULL,
  `id_dia_semana` int(11) NOT NULL,
  `id_hora_clase` int(11) NOT NULL,
  `id_estudiante` int(11) NOT NULL,
  `id_asignatura` int(11) NOT NULL,
  `id_paralelo` int(11) NOT NULL,
  `id_inasistencia` int(11) NOT NULL,
  `ae_fecha` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_asociar_curso_superior`
--

DROP TABLE IF EXISTS `sw_asociar_curso_superior`;
CREATE TABLE `sw_asociar_curso_superior` (
  `id_asociar_curso_superior` int(11) NOT NULL,
  `id_curso_inferior` int(11) DEFAULT NULL,
  `id_curso_superior` int(11) DEFAULT NULL,
  `id_periodo_lectivo` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_calificacion_comportamiento`
--

DROP TABLE IF EXISTS `sw_calificacion_comportamiento`;
CREATE TABLE `sw_calificacion_comportamiento` (
  `id_paralelo` int(11) NOT NULL,
  `id_estudiante` int(11) NOT NULL,
  `id_aporte_evaluacion` int(11) NOT NULL,
  `id_asignatura` int(11) NOT NULL,
  `co_calificacion` int(11) NOT NULL,
  `co_cualitativa` varchar(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_club`
--

DROP TABLE IF EXISTS `sw_club`;
CREATE TABLE `sw_club` (
  `id_club` int(11) NOT NULL,
  `cl_nombre` varchar(32) NOT NULL,
  `cl_abreviatura` varchar(6) NOT NULL,
  `cl_carga_horaria` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_club_docente`
--

DROP TABLE IF EXISTS `sw_club_docente`;
CREATE TABLE `sw_club_docente` (
  `id_club_docente` int(11) NOT NULL,
  `id_club` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `id_periodo_lectivo` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_comentario`
--

DROP TABLE IF EXISTS `sw_comentario`;
CREATE TABLE `sw_comentario` (
  `id_comentario` int(11) NOT NULL,
  `co_id_usuario` int(11) NOT NULL,
  `co_tipo` tinyint(4) NOT NULL,
  `co_perfil` varchar(16) NOT NULL,
  `co_nombre` varchar(64) NOT NULL,
  `co_texto` varchar(250) NOT NULL,
  `co_fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_comportamiento`
--

DROP TABLE IF EXISTS `sw_comportamiento`;
CREATE TABLE `sw_comportamiento` (
  `id_paralelo` int(11) NOT NULL,
  `id_estudiante` int(11) NOT NULL,
  `id_periodo_evaluacion` int(11) NOT NULL,
  `id_indice_evaluacion` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_comportamiento_inspector`
--

DROP TABLE IF EXISTS `sw_comportamiento_inspector`;
CREATE TABLE `sw_comportamiento_inspector` (
  `id_comportamiento_inspector` int(11) NOT NULL,
  `id_paralelo` int(11) NOT NULL,
  `id_estudiante` int(11) NOT NULL,
  `id_aporte_evaluacion` int(11) NOT NULL,
  `id_escala_comportamiento` int(11) NOT NULL,
  `co_calificacion` varchar(1) NOT NULL,
  `nro_faltas` int(11) NOT NULL,
  `justificadas` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_curso`
--

DROP TABLE IF EXISTS `sw_curso`;
CREATE TABLE `sw_curso` (
  `id_curso` int(11) NOT NULL,
  `id_especialidad` int(11) NOT NULL,
  `cu_nombre` varchar(128) CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL,
  `cu_shortname` varchar(45) NOT NULL,
  `cu_orden` int(11) NOT NULL,
  `id_curso_superior` int(11) NOT NULL,
  `bol_proyectos` tinyint(1) NOT NULL,
  `cu_abreviatura` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `sw_curso`
--

INSERT INTO `sw_curso` (`id_curso`, `id_especialidad`, `cu_nombre`, `cu_shortname`, `cu_orden`, `id_curso_superior`, `bol_proyectos`, `cu_abreviatura`) VALUES
(1, 1, 'Octavo Grado', 'OCTAVO', 1, 0, 0, '8vo.'),
(2, 1, 'Noveno Grado', 'NOVENO', 2, 0, 0, '9no.'),
(3, 1, 'Décimo Grado', 'DÉCIMO', 3, 0, 0, '10mo.'),
(4, 4, 'Primer Curso', 'PRIMERO CIENCIAS', 4, 0, 0, '1ro.'),
(5, 4, 'Segundo Curso', 'SEGUNDO CIENCIAS', 5, 0, 0, '2do.'),
(6, 4, 'Tercer Curso', 'TERCERO CIENCIAS', 6, 0, 0, '3ro.'),
(7, 2, 'Primer Curso', 'PRIMERO CONTABILIDAD', 7, 0, 0, '1ro.'),
(8, 2, 'Segundo Curso', 'SEGUNDO CONTABILIDAD', 8, 0, 0, '2do.'),
(9, 3, 'Primer Curso', 'PRIMERO INFORMATICA', 9, 0, 0, '1ro.'),
(10, 3, 'Segundo Curso', 'SEGUNDO INFORMATICA', 10, 0, 0, '2do.'),
(11, 3, 'Tercer Curso', 'TERCERO INFORMATICA', 11, 0, 0, '3ro.');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_curso_superior`
--

DROP TABLE IF EXISTS `sw_curso_superior`;
CREATE TABLE `sw_curso_superior` (
  `id_curso_superior` int(11) NOT NULL,
  `id_periodo_lectivo` int(11) NOT NULL,
  `cs_nombre` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_dia_feriado`
--

DROP TABLE IF EXISTS `sw_dia_feriado`;
CREATE TABLE `sw_dia_feriado` (
  `id_dia_feriado` int(11) NOT NULL,
  `id_periodo_lectivo` int(11) NOT NULL,
  `df_fecha` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_dia_semana`
--

DROP TABLE IF EXISTS `sw_dia_semana`;
CREATE TABLE `sw_dia_semana` (
  `id_dia_semana` int(11) NOT NULL,
  `id_periodo_lectivo` int(11) NOT NULL,
  `ds_nombre` varchar(10) NOT NULL,
  `ds_ordinal` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_distributivo`
--

DROP TABLE IF EXISTS `sw_distributivo`;
CREATE TABLE `sw_distributivo` (
  `id_distributivo` int(11) NOT NULL,
  `id_periodo_lectivo` int(11) NOT NULL,
  `id_malla_curricular` int(11) NOT NULL,
  `id_paralelo` int(11) NOT NULL,
  `id_asignatura` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_escala_calificaciones`
--

DROP TABLE IF EXISTS `sw_escala_calificaciones`;
CREATE TABLE `sw_escala_calificaciones` (
  `id_escala_calificaciones` int(11) NOT NULL,
  `id_periodo_lectivo` int(11) NOT NULL,
  `ec_cualitativa` varchar(64) NOT NULL,
  `ec_cuantitativa` varchar(16) NOT NULL,
  `ec_nota_minima` float NOT NULL,
  `ec_nota_maxima` float NOT NULL,
  `ec_orden` tinyint(4) NOT NULL,
  `ec_equivalencia` varchar(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_escala_comportamiento`
--

DROP TABLE IF EXISTS `sw_escala_comportamiento`;
CREATE TABLE `sw_escala_comportamiento` (
  `id_escala_comportamiento` int(11) NOT NULL,
  `ec_relacion` varchar(32) NOT NULL,
  `ec_cualitativa` varchar(164) NOT NULL,
  `ec_cuantitativa` varchar(16) NOT NULL,
  `ec_nota_minima` float NOT NULL,
  `ec_nota_maxima` float NOT NULL,
  `ec_equivalencia` varchar(3) NOT NULL,
  `ec_correlativa` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_escala_proyectos`
--

DROP TABLE IF EXISTS `sw_escala_proyectos`;
CREATE TABLE `sw_escala_proyectos` (
  `id_escala_proyectos` int(11) NOT NULL,
  `ec_cualitativa` varchar(256) NOT NULL,
  `ec_cuantitativa` varchar(16) NOT NULL,
  `ec_nota_minima` float NOT NULL,
  `ec_nota_maxima` float NOT NULL,
  `ec_orden` tinyint(4) NOT NULL,
  `ec_equivalencia` varchar(16) NOT NULL,
  `ec_abreviatura` varchar(2) NOT NULL,
  `ec_correlativa` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_especialidad`
--

DROP TABLE IF EXISTS `sw_especialidad`;
CREATE TABLE `sw_especialidad` (
  `id_especialidad` int(11) NOT NULL,
  `id_tipo_educacion` int(11) NOT NULL,
  `es_nombre` varchar(64) CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL,
  `es_figura` varchar(50) NOT NULL,
  `es_abreviatura` varchar(15) NOT NULL,
  `es_orden` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `sw_especialidad`
--

INSERT INTO `sw_especialidad` (`id_especialidad`, `id_tipo_educacion`, `es_nombre`, `es_figura`, `es_abreviatura`, `es_orden`) VALUES
(1, 1, 'EGB SUPERIOR', 'Educación General Básica Superior', 'EGBS', 1),
(2, 2, 'Área Técnica de Servicios', 'Contabilidad', 'CONTA', 3),
(3, 2, 'Área Técnica de Servicios', 'Informática', 'INFO', 4),
(4, 3, 'BACHILLERATO GENERAL UNIFICADO', 'Bachillerato General Unificado', 'BGU', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_estudiante`
--

DROP TABLE IF EXISTS `sw_estudiante`;
CREATE TABLE `sw_estudiante` (
  `id_estudiante` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `es_nro_matricula` int(11) NOT NULL,
  `es_apellidos` varchar(32) NOT NULL,
  `es_nombres` varchar(32) NOT NULL,
  `es_nombre_completo` varchar(64) NOT NULL,
  `es_cedula` varchar(10) NOT NULL,
  `es_genero` varchar(1) NOT NULL,
  `es_email` varchar(64) NOT NULL,
  `es_sector` varchar(36) NOT NULL,
  `es_direccion` varchar(64) NOT NULL,
  `es_telefono` varchar(16) NOT NULL,
  `es_fec_nacim` date NOT NULL,
  `es_cod_uni_elec_nac` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_estudiante_club`
--

DROP TABLE IF EXISTS `sw_estudiante_club`;
CREATE TABLE `sw_estudiante_club` (
  `id_estudiante_club` int(11) NOT NULL,
  `id_estudiante` int(11) NOT NULL,
  `id_periodo_lectivo` int(11) NOT NULL,
  `id_club` int(11) NOT NULL,
  `es_retirado` varchar(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_estudiante_periodo_lectivo`
--

DROP TABLE IF EXISTS `sw_estudiante_periodo_lectivo`;
CREATE TABLE `sw_estudiante_periodo_lectivo` (
  `id_estudiante_periodo_lectivo` int(11) NOT NULL,
  `id_estudiante` int(11) NOT NULL,
  `id_periodo_lectivo` int(11) NOT NULL,
  `id_paralelo` int(11) NOT NULL,
  `es_estado` char(1) CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL,
  `es_retirado` varchar(1) NOT NULL DEFAULT 'N'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Disparadores `sw_estudiante_periodo_lectivo`
--
DROP TRIGGER IF EXISTS `tg_update_estudiante_periodo_lectivo`;
DELIMITER $$
CREATE TRIGGER `tg_update_estudiante_periodo_lectivo` AFTER UPDATE ON `sw_estudiante_periodo_lectivo` FOR EACH ROW UPDATE sw_rubrica_estudiante 
   SET id_paralelo = new.id_paralelo
 WHERE id_estudiante = new.id_estudiante
   AND id_paralelo = old.id_paralelo
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_estudiante_promedio_parcial`
--

DROP TABLE IF EXISTS `sw_estudiante_promedio_parcial`;
CREATE TABLE `sw_estudiante_promedio_parcial` (
  `id` int(11) NOT NULL,
  `id_paralelo` int(11) NOT NULL,
  `id_estudiante` int(11) NOT NULL,
  `id_aporte_evaluacion` int(11) NOT NULL,
  `ep_promedio` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_estudiante_prom_anual`
--

DROP TABLE IF EXISTS `sw_estudiante_prom_anual`;
CREATE TABLE `sw_estudiante_prom_anual` (
  `id` int(11) NOT NULL,
  `id_paralelo` int(11) NOT NULL,
  `id_estudiante` int(11) NOT NULL,
  `id_periodo_lectivo` int(11) NOT NULL,
  `ea_promedio` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_estudiante_prom_quimestral`
--

DROP TABLE IF EXISTS `sw_estudiante_prom_quimestral`;
CREATE TABLE `sw_estudiante_prom_quimestral` (
  `id` int(11) NOT NULL,
  `id_paralelo` int(11) NOT NULL,
  `id_estudiante` int(11) NOT NULL,
  `id_periodo_evaluacion` int(11) NOT NULL,
  `eq_promedio` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_feriado`
--

DROP TABLE IF EXISTS `sw_feriado`;
CREATE TABLE `sw_feriado` (
  `id_feriado` int(11) NOT NULL,
  `id_periodo_lectivo` int(11) NOT NULL,
  `fe_fecha` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_foro`
--

DROP TABLE IF EXISTS `sw_foro`;
CREATE TABLE `sw_foro` (
  `id_foro` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `fo_titulo` varchar(50) NOT NULL,
  `fo_descripcion` varchar(250) NOT NULL,
  `fo_fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_horario`
--

DROP TABLE IF EXISTS `sw_horario`;
CREATE TABLE `sw_horario` (
  `id_horario` int(11) NOT NULL,
  `id_asignatura` int(11) NOT NULL,
  `id_paralelo` int(11) NOT NULL,
  `id_dia_semana` int(11) NOT NULL,
  `id_hora_clase` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_horario_examen`
--

DROP TABLE IF EXISTS `sw_horario_examen`;
CREATE TABLE `sw_horario_examen` (
  `id_horario_examen` int(11) NOT NULL,
  `id_periodo_lectivo` int(11) NOT NULL,
  `id_dia_semana` int(11) NOT NULL,
  `id_hora_examen` int(11) NOT NULL,
  `id_paralelo` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `he_fecha` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_hora_clase`
--

DROP TABLE IF EXISTS `sw_hora_clase`;
CREATE TABLE `sw_hora_clase` (
  `id_hora_clase` int(11) NOT NULL,
  `id_periodo_lectivo` int(11) NOT NULL,
  `hc_nombre` varchar(10) NOT NULL,
  `hc_hora_inicio` time NOT NULL,
  `hc_hora_fin` time NOT NULL,
  `hc_ordinal` int(11) NOT NULL,
  `hc_tipo` char(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_hora_dia`
--

DROP TABLE IF EXISTS `sw_hora_dia`;
CREATE TABLE `sw_hora_dia` (
  `id_hora_dia` int(11) NOT NULL,
  `id_dia_semana` int(11) NOT NULL,
  `id_hora_clase` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_inasistencia`
--

DROP TABLE IF EXISTS `sw_inasistencia`;
CREATE TABLE `sw_inasistencia` (
  `id_inasistencia` int(11) NOT NULL,
  `in_nombre` varchar(32) NOT NULL,
  `in_abreviatura` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_indice_evaluacion`
--

DROP TABLE IF EXISTS `sw_indice_evaluacion`;
CREATE TABLE `sw_indice_evaluacion` (
  `id_indice_evaluacion` int(11) NOT NULL,
  `valores_t` float NOT NULL,
  `cum_norma_t` float NOT NULL,
  `pun_asiste_t` float NOT NULL,
  `presentacion_t` float NOT NULL,
  `valores_i` float NOT NULL,
  `cum_norma_i` float NOT NULL,
  `pun_asiste_i` float NOT NULL,
  `presentacion_i` float NOT NULL,
  `total` float NOT NULL,
  `promedio` float NOT NULL,
  `equivalencia` varchar(1) COLLATE latin1_spanish_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_indice_evaluacion_def`
--

DROP TABLE IF EXISTS `sw_indice_evaluacion_def`;
CREATE TABLE `sw_indice_evaluacion_def` (
  `id_indice_evaluacion` int(11) NOT NULL,
  `ie_descripcion` varchar(64) NOT NULL,
  `ie_abreviatura` varchar(8) NOT NULL,
  `ie_orden` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_institucion`
--

DROP TABLE IF EXISTS `sw_institucion`;
CREATE TABLE `sw_institucion` (
  `id_institucion` int(11) NOT NULL,
  `in_nombre` varchar(64) NOT NULL,
  `in_direccion` varchar(45) NOT NULL,
  `in_telefono1` varchar(12) NOT NULL,
  `in_nom_rector` varchar(45) NOT NULL,
  `in_nom_vicerrector` varchar(45) NOT NULL,
  `in_nom_secretario` varchar(45) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `sw_institucion`
--

INSERT INTO `sw_institucion` (`id_institucion`, `in_nombre`, `in_direccion`, `in_telefono1`, `in_nom_rector`, `in_nom_vicerrector`, `in_nom_secretario`) VALUES
(1, 'UNIDAD EDUCATIVA PCEI FISCAL SALAMANCA', 'Calle el Tiempo y Pasaje Mónaco', '2256-104', 'DR. RAMIRO CASTILLO', 'Lic. Rómulo Mejía', 'Msc. CARLOS CARAGUAY');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_malla_curricular`
--

DROP TABLE IF EXISTS `sw_malla_curricular`;
CREATE TABLE `sw_malla_curricular` (
  `id_malla_curricular` int(11) NOT NULL,
  `id_periodo_lectivo` int(11) NOT NULL,
  `id_paralelo` int(11) NOT NULL,
  `id_asignatura` int(11) NOT NULL,
  `ma_horas_presenciales` int(11) NOT NULL,
  `ma_horas_autonomas` int(11) NOT NULL,
  `ma_horas_tutorias` int(11) NOT NULL,
  `ma_subtotal` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_mensaje`
--

DROP TABLE IF EXISTS `sw_mensaje`;
CREATE TABLE `sw_mensaje` (
  `id_mensaje` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `id_perfil` int(11) NOT NULL,
  `me_texto` varchar(250) NOT NULL,
  `me_fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_menu`
--

DROP TABLE IF EXISTS `sw_menu`;
CREATE TABLE `sw_menu` (
  `id_menu` int(11) NOT NULL,
  `id_perfil` int(11) NOT NULL,
  `mnu_texto` varchar(32) NOT NULL,
  `mnu_enlace` varchar(64) NOT NULL,
  `mnu_link` varchar(64) NOT NULL,
  `mnu_nivel` int(11) NOT NULL,
  `mnu_orden` int(11) NOT NULL,
  `mnu_padre` int(11) NOT NULL,
  `mnu_publicado` int(11) NOT NULL,
  `mnu_icono` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `sw_menu`
--

INSERT INTO `sw_menu` (`id_menu`, `id_perfil`, `mnu_texto`, `mnu_enlace`, `mnu_link`, `mnu_nivel`, `mnu_orden`, `mnu_padre`, `mnu_publicado`, `mnu_icono`) VALUES
(1, 1, 'Administración', '#', '', 1, 1, 0, 1, ''),
(2, 1, 'Definiciones', '#', '', 1, 2, 0, 1, ''),
(3, 1, 'Especificaciones', '#', '', 1, 3, 0, 1, ''),
(4, 1, 'Asociar', '#', '', 1, 4, 0, 1, ''),
(5, 1, 'Cierres', '#', '', 1, 5, 0, 1, ''),
(6, 5, 'Definiciones', '#', '', 1, 1, 0, 1, ''),
(7, 5, 'Horarios', '#', '', 1, 1, 0, 1, ''),
(8, 5, 'Reportes', '#', '', 1, 1, 0, 1, ''),
(9, 5, 'Consultas', '#', '', 1, 1, 0, 1, ''),
(10, 5, 'Estadísticas', '#', '', 1, 1, 0, 1, ''),
(11, 7, 'Reportes', '#', '', 1, 1, 0, 1, ''),
(12, 2, 'Ingresar calificaciones', '#', '', 1, 1, 0, 1, ''),
(13, 2, 'Asistencia', '#', '', 1, 1, 0, 1, ''),
(14, 2, 'Informes', '#', '', 1, 1, 0, 1, ''),
(15, 2, 'Reportes', '#', '', 1, 1, 0, 1, ''),
(16, 2, 'Listas', '#', '', 1, 1, 0, 1, ''),
(17, 4, 'Definiciones', '#', '', 1, 1, 0, 1, ''),
(18, 4, 'Comportamiento', '#', '', 1, 1, 0, 1, ''),
(19, 4, 'Horarios', '#', '', 1, 1, 0, 1, ''),
(20, 4, 'Consultas', '#', '', 1, 1, 0, 1, ''),
(21, 3, 'Definiciones', '#', '', 1, 1, 0, 1, ''),
(22, 3, 'Matriculación', '#', '', 1, 1, 0, 1, ''),
(23, 3, 'Libretación', '#', '', 1, 1, 0, 0, ''),
(24, 3, 'Reporte', '#', '', 1, 1, 0, 1, ''),
(25, 3, 'A Excel', '#', '', 1, 1, 0, 1, ''),
(26, 3, 'Promoción', 'promociones', '', 1, 1, 0, 1, ''),
(27, 6, 'Comportamiento', '#', '', 1, 1, 0, 1, ''),
(28, 6, 'Reportes', '#', '', 1, 1, 0, 1, ''),
(29, 6, 'Horario', 'horario_paralelo', '', 1, 1, 0, 1, ''),
(30, 6, 'Consultas', '#', '', 1, 1, 0, 1, ''),
(34, 1, 'Periodos Lectivos', 'periodos_lectivos', '', 2, 1, 1, 1, ''),
(35, 1, 'Perfiles', 'perfiles', '', 2, 2, 1, 1, ''),
(36, 1, 'Menus', 'menus', '', 2, 3, 1, 1, ''),
(37, 1, 'Usuarios', 'usuarios', '', 2, 4, 1, 1, ''),
(38, 1, 'Niveles de Educación', 'niveles_educacion', '', 2, 1, 2, 1, ''),
(39, 1, 'Especialidades', 'especialidades', '', 2, 2, 2, 1, ''),
(40, 1, 'Cursos', 'cursos', '', 2, 3, 2, 1, ''),
(41, 1, 'Paralelos', 'paralelos', '', 2, 4, 2, 1, ''),
(42, 1, 'Areas', 'areas', '', 2, 6, 2, 1, ''),
(44, 1, 'Asignaturas', 'asignaturas', '', 2, 7, 2, 1, ''),
(45, 1, 'Periodos de Evaluación', 'periodos_evaluacion', '', 2, 1, 3, 1, ''),
(46, 1, 'Aportes de Evaluación', 'aportes_evaluacion', '', 2, 2, 3, 1, ''),
(47, 1, 'Rúbricas de Evaluación', 'rubricas_evaluacion', '', 2, 3, 3, 1, ''),
(48, 1, 'Rubricas de Proyectos', 'rubricas_proyectos', '', 2, 4, 3, 1, ''),
(49, 1, 'Escalas de Calificaciones', 'escalas_calificaciones', '', 2, 5, 3, 1, ''),
(50, 1, 'Asignaturas Cursos', 'asignaturas_cursos', '', 2, 1, 4, 1, ''),
(51, 1, 'Paralelos Tutores', 'paralelos_tutores', '', 2, 2, 4, 1, ''),
(52, 1, 'Paralelos Inspectores', 'paralelos_inspectores', '', 2, 3, 4, 1, ''),
(53, 1, 'Asociar', 'asociar_cierres', '', 2, 1, 5, 1, ''),
(54, 1, 'Periodos', 'cierre_periodos', '', 2, 2, 5, 1, ''),
(55, 5, 'Escalas de Calificaciones', 'escalas_calificaciones', '', 2, 1, 6, 1, ''),
(56, 5, 'Malla Curricular', 'malla_curricular', '', 2, 2, 6, 1, ''),
(57, 5, 'Distributivo', 'distributivo', '', 2, 3, 6, 1, ''),
(58, 5, 'Días de la Semana', 'dias_semana', '', 2, 1, 7, 1, ''),
(59, 5, 'Horas Clase', 'horas_clase', '', 2, 2, 7, 1, ''),
(60, 5, 'Asociar Día-Hora', 'asociar_dia_hora', '', 2, 3, 7, 1, ''),
(61, 5, 'Horario Semanal', 'horario_semanal', '', 2, 4, 7, 1, ''),
(62, 5, 'Parciales', 'reporte_parciales_autoridad', '', 2, 1, 8, 1, ''),
(63, 5, 'Quimestral', 'reporte_quimestral_autoridad', '', 2, 2, 8, 1, ''),
(64, 5, 'Anual', 'reporte_anual_autoridad', '', 2, 3, 8, 1, ''),
(65, 5, 'Lista de Docentes', 'lista_docentes_autoridad', '', 2, 1, 9, 1, ''),
(66, 5, 'Horarios de Clase', 'horarios_clase_autoridad', '', 2, 2, 9, 1, ''),
(67, 5, 'Aprobados por Paralelo', 'aprobados_paralelo', '', 2, 1, 10, 1, ''),
(68, 7, 'Parciales', 'reporte_parciales_autoridad', '', 2, 1, 11, 1, ''),
(69, 7, 'Quimestrales', 'reporte_quimestral_autoridad', '', 2, 2, 11, 1, ''),
(70, 7, 'Anuales', 'reporte_anual_autoridad', '', 2, 3, 11, 1, ''),
(71, 2, 'Parciales', 'calificaciones_parciales', '', 2, 1, 12, 1, ''),
(72, 2, 'Supletorios', 'calificaciones_supletorios', '', 2, 2, 12, 1, ''),
(73, 2, 'Remediales', 'calificaciones_remediales', '', 2, 3, 12, 1, ''),
(75, 2, 'De Gracia', 'calificaciones_de_gracia', '', 2, 4, 12, 1, ''),
(76, 2, 'Proyectos', 'calificaciones_proyectos', '', 2, 5, 12, 1, ''),
(78, 2, 'Ingresar Asistencia', 'asistencia', '', 2, 1, 13, 1, ''),
(80, 2, 'Ver horario', 'ver_horario', '', 2, 2, 13, 1, ''),
(81, 2, 'Parciales', 'informe_parciales', '', 2, 1, 14, 1, ''),
(82, 2, 'Quimestrales', 'informe_quimestral', '', 2, 2, 14, 1, ''),
(83, 2, 'Anuales', 'informe_anual', '', 2, 3, 14, 1, ''),
(84, 2, 'Quimestrales', 'reporte_quimestral_docente', '', 2, 1, 15, 1, ''),
(85, 2, 'Anuales', 'reporte_anual_docente', '', 2, 2, 15, 1, ''),
(86, 2, 'Por Parcial', 'listas_por_parcial', '', 2, 1, 16, 1, ''),
(87, 2, 'Por Quimestre', 'listas_por_quimestre', '', 2, 2, 16, 1, ''),
(88, 2, 'Proyectos', 'listas_proyectos', '', 2, 3, 16, 1, ''),
(89, 4, 'Inasistencias', 'inasistencias', '', 2, 1, 17, 1, ''),
(90, 4, 'Valor del Mes', 'valor_mes', '', 2, 2, 17, 1, ''),
(91, 4, 'Parciales', 'comportamiento_parciales', '', 2, 1, 18, 1, ''),
(92, 4, 'Quimestral', 'comportamiento_quimestral', '', 2, 2, 18, 1, ''),
(93, 4, 'Leccionario', 'leccionario', '', 2, 1, 19, 1, ''),
(94, 4, 'Docentes', 'horario_docentes', '', 2, 2, 19, 1, ''),
(96, 4, 'Lista de Docentes', 'lista_docentes_autoridad', '', 2, 1, 20, 1, ''),
(97, 4, 'Horario de Clases', 'horarios_clase_autoridad', '', 2, 2, 20, 1, ''),
(98, 3, 'Institución', 'institucion', '', 2, 1, 21, 1, ''),
(99, 3, 'Proyectos', 'proyectos_escolares', '', 2, 2, 21, 1, ''),
(100, 3, 'Paralelos', 'matriculacion_paralelos', '', 2, 1, 22, 1, ''),
(101, 3, 'Proyectos Escolares', 'matriculacion_proyectos', '', 2, 2, 22, 1, ''),
(102, 3, 'Validar calificaciones', 'validar_calificaciones', '', 2, 1, 23, 1, ''),
(103, 3, 'Libretas', 'libretas', '', 2, 2, 23, 1, ''),
(104, 3, 'Por Asignatura', 'reporte_por_asignatura', '', 2, 1, 24, 1, ''),
(105, 3, 'Parciales', 'reporte_por_parcial', '', 2, 2, 24, 1, ''),
(106, 3, 'Quimestral', 'reporte_quimestral', '', 2, 3, 24, 1, ''),
(107, 3, 'Anual', 'reporte_anual', '', 2, 4, 24, 1, ''),
(108, 3, 'Supletorios', 'reporte_supletorios', '', 2, 5, 24, 1, ''),
(109, 3, 'Remediales', 'reporte_remediales', '', 2, 6, 24, 1, ''),
(110, 3, 'De Gracia', 'reporte_de_gracia', '', 2, 7, 24, 1, ''),
(111, 3, 'Proyectos Escolares', 'reporte_proyectos_escolares', '', 2, 8, 24, 1, ''),
(112, 3, 'Nómina de Matriculados', 'nomina_matriculados', '', 2, 1, 25, 1, ''),
(113, 3, 'Padrón Electoral', 'padron_electoral', '', 2, 2, 25, 1, ''),
(114, 3, 'Cuadro Final', 'cuadro_final', '', 2, 3, 25, 1, ''),
(115, 3, 'Cuadro Remediales', 'cuadro_remediales', '', 2, 4, 25, 1, ''),
(116, 3, 'Cuadro de Gracia', 'cuadro_de_gracia', '', 2, 5, 25, 1, ''),
(117, 6, 'De Parciales', 'comportamiento_parciales', '', 2, 1, 27, 1, ''),
(118, 6, 'De Quimestrales', 'comportamiento_quimestral', '', 2, 2, 27, 1, ''),
(119, 6, 'Anual', 'comportamiento_anual', '', 2, 3, 27, 1, ''),
(120, 6, 'Parciales', 'reporte_parciales_tutor', '', 2, 1, 28, 1, ''),
(121, 6, 'Quimestrales', 'reporte_quimestral_tutor', '', 2, 2, 28, 1, ''),
(122, 6, 'Anuales', 'reporte_anual_tutor', '', 2, 3, 28, 1, ''),
(123, 6, 'Supletorios', 'reporte_supletorios_tutor', '', 2, 4, 28, 1, ''),
(124, 6, 'Remediales', 'reporte_remediales_tutor', '', 2, 5, 28, 1, ''),
(125, 6, 'De Gracia', 'reporte_de_gracia_tutor', '', 2, 6, 28, 1, ''),
(126, 6, 'Lista de Docentes', 'lista_docentes_paralelo', '', 2, 1, 30, 1, ''),
(127, 6, 'Horario de Clases', 'horario_clases_paralelo', '', 2, 2, 30, 1, ''),
(128, 4, 'Feriados', 'feriados', '', 2, 3, 17, 1, '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_modalidad`
--

DROP TABLE IF EXISTS `sw_modalidad`;
CREATE TABLE `sw_modalidad` (
  `id_modalidad` int(11) NOT NULL,
  `mo_nombre` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_paralelo`
--

DROP TABLE IF EXISTS `sw_paralelo`;
CREATE TABLE `sw_paralelo` (
  `id_paralelo` int(11) NOT NULL,
  `id_curso` int(11) NOT NULL,
  `pa_nombre` varchar(5) NOT NULL,
  `pa_orden` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_paralelo_asignatura`
--

DROP TABLE IF EXISTS `sw_paralelo_asignatura`;
CREATE TABLE `sw_paralelo_asignatura` (
  `id_paralelo_asignatura` int(11) NOT NULL,
  `id_paralelo` int(11) NOT NULL,
  `id_asignatura` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `id_periodo_lectivo` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_paralelo_inspector`
--

DROP TABLE IF EXISTS `sw_paralelo_inspector`;
CREATE TABLE `sw_paralelo_inspector` (
  `id_paralelo_inspector` int(11) NOT NULL,
  `id_paralelo` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `id_periodo_lectivo` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_paralelo_tutor`
--

DROP TABLE IF EXISTS `sw_paralelo_tutor`;
CREATE TABLE `sw_paralelo_tutor` (
  `id_paralelo_tutor` int(11) NOT NULL,
  `id_paralelo` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `id_periodo_lectivo` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_perfil`
--

DROP TABLE IF EXISTS `sw_perfil`;
CREATE TABLE `sw_perfil` (
  `id_perfil` int(11) NOT NULL,
  `pe_nombre` varchar(16) CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `sw_perfil`
--

INSERT INTO `sw_perfil` (`id_perfil`, `pe_nombre`) VALUES
(1, 'Administrador'),
(2, 'Docente'),
(3, 'Secretaría'),
(4, 'Inspección'),
(5, 'Autoridad'),
(6, 'Tutor'),
(7, 'DECE');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_periodo_estado`
--

DROP TABLE IF EXISTS `sw_periodo_estado`;
CREATE TABLE `sw_periodo_estado` (
  `id_periodo_estado` int(11) NOT NULL,
  `pe_descripcion` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `sw_periodo_estado`
--

INSERT INTO `sw_periodo_estado` (`id_periodo_estado`, `pe_descripcion`) VALUES
(1, 'ACTUAL'),
(2, 'TERMINADO');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_periodo_evaluacion`
--

DROP TABLE IF EXISTS `sw_periodo_evaluacion`;
CREATE TABLE `sw_periodo_evaluacion` (
  `id_periodo_evaluacion` int(11) NOT NULL,
  `id_periodo_lectivo` int(11) NOT NULL,
  `id_tipo_periodo` int(11) NOT NULL,
  `pe_nombre` varchar(24) CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL,
  `pe_abreviatura` varchar(6) NOT NULL,
  `pe_shortname` varchar(15) NOT NULL,
  `pe_principal` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_periodo_lectivo`
--

DROP TABLE IF EXISTS `sw_periodo_lectivo`;
CREATE TABLE `sw_periodo_lectivo` (
  `id_periodo_lectivo` int(11) NOT NULL,
  `id_periodo_estado` int(11) NOT NULL,
  `id_institucion` int(11) NOT NULL,
  `pe_anio_inicio` int(11) NOT NULL,
  `pe_anio_fin` int(11) NOT NULL,
  `pe_estado` char(1) CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL,
  `pe_fecha_inicio` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `sw_periodo_lectivo`
--

INSERT INTO `sw_periodo_lectivo` (`id_periodo_lectivo`, `id_periodo_estado`, `id_institucion`, `pe_anio_inicio`, `pe_anio_fin`, `pe_estado`, `pe_fecha_inicio`) VALUES
(1, 1, 1, 2018, 2019, 'A', '2018-09-03');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_permiso`
--

DROP TABLE IF EXISTS `sw_permiso`;
CREATE TABLE `sw_permiso` (
  `id_permiso` int(11) NOT NULL,
  `id_menu` int(11) NOT NULL,
  `id_perfil` int(11) NOT NULL,
  `read` int(11) NOT NULL,
  `insert` int(11) NOT NULL,
  `update` int(11) NOT NULL,
  `delete` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_plan_rubrica`
--

DROP TABLE IF EXISTS `sw_plan_rubrica`;
CREATE TABLE `sw_plan_rubrica` (
  `id_plan_rubrica` int(11) NOT NULL,
  `pr_tema` varchar(50) NOT NULL,
  `pr_descripcion` varchar(250) NOT NULL,
  `pr_fecha_elab` datetime NOT NULL,
  `pr_fecha_eval` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_representante`
--

DROP TABLE IF EXISTS `sw_representante`;
CREATE TABLE `sw_representante` (
  `id_representante` int(11) NOT NULL,
  `id_estudiante` int(11) DEFAULT NULL,
  `re_apellidos` varchar(32) DEFAULT NULL,
  `re_nombres` varchar(32) DEFAULT NULL,
  `re_nombre_completo` varchar(64) DEFAULT NULL,
  `re_cedula` varchar(10) DEFAULT NULL,
  `re_genero` varchar(1) DEFAULT NULL,
  `re_email` varchar(64) DEFAULT NULL,
  `re_sector` varchar(36) DEFAULT NULL,
  `re_direccion` varchar(64) DEFAULT NULL,
  `re_telefono` varchar(16) DEFAULT NULL,
  `re_observacion` varchar(256) DEFAULT NULL,
  `re_parentesco` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='					';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_respuesta`
--

DROP TABLE IF EXISTS `sw_respuesta`;
CREATE TABLE `sw_respuesta` (
  `id_respuesta` int(11) NOT NULL,
  `id_tema` int(11) NOT NULL,
  `re_texto` text NOT NULL,
  `re_fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `re_autor` int(11) NOT NULL,
  `re_perfil` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_rubrica_club`
--

DROP TABLE IF EXISTS `sw_rubrica_club`;
CREATE TABLE `sw_rubrica_club` (
  `id_rubrica_club` int(11) NOT NULL,
  `id_estudiante` int(11) NOT NULL,
  `id_club` int(11) NOT NULL,
  `id_rubrica_evaluacion` int(11) NOT NULL,
  `rc_calificacion` float NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_rubrica_cualitativa`
--

DROP TABLE IF EXISTS `sw_rubrica_cualitativa`;
CREATE TABLE `sw_rubrica_cualitativa` (
  `id_paralelo` int(11) NOT NULL,
  `id_estudiante` int(11) NOT NULL,
  `id_aporte_evaluacion` int(11) NOT NULL,
  `id_asignatura` int(11) NOT NULL,
  `rc_calificacion` varchar(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_rubrica_docente`
--

DROP TABLE IF EXISTS `sw_rubrica_docente`;
CREATE TABLE `sw_rubrica_docente` (
  `id_rubrica_evaluacion` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `id_asignatura` int(11) NOT NULL,
  `id_paralelo` int(11) NOT NULL,
  `rd_nombre` varchar(64) NOT NULL,
  `rd_descripcion` varchar(256) NOT NULL,
  `rd_fecha_envio` date NOT NULL,
  `rd_fecha_revision` date NOT NULL,
  `rd_observacion` varchar(128) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_rubrica_estudiante`
--

DROP TABLE IF EXISTS `sw_rubrica_estudiante`;
CREATE TABLE `sw_rubrica_estudiante` (
  `id_rubrica_estudiante` int(11) NOT NULL,
  `id_estudiante` int(11) NOT NULL,
  `id_paralelo` int(11) NOT NULL,
  `id_asignatura` int(11) NOT NULL,
  `id_rubrica_personalizada` int(11) NOT NULL,
  `re_calificacion` float NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_rubrica_evaluacion`
--

DROP TABLE IF EXISTS `sw_rubrica_evaluacion`;
CREATE TABLE `sw_rubrica_evaluacion` (
  `id_rubrica_evaluacion` int(11) NOT NULL,
  `id_aporte_evaluacion` int(11) NOT NULL,
  `id_tipo_rubrica` int(11) NOT NULL,
  `id_tipo_asignatura` int(11) NOT NULL DEFAULT '1',
  `ru_nombre` varchar(24) CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL,
  `ru_abreviatura` varchar(6) CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_rubrica_evaluacion_club`
--

DROP TABLE IF EXISTS `sw_rubrica_evaluacion_club`;
CREATE TABLE `sw_rubrica_evaluacion_club` (
  `id_rubrica_evaluacion_club` int(11) NOT NULL,
  `id_aporte_evaluacion` int(11) NOT NULL,
  `rc_nombre` varchar(24) NOT NULL,
  `rc_abreviatura` varchar(7) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_rubrica_personalizada`
--

DROP TABLE IF EXISTS `sw_rubrica_personalizada`;
CREATE TABLE `sw_rubrica_personalizada` (
  `id_rubrica_personalizada` int(11) NOT NULL,
  `id_rubrica_evaluacion` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `id_asignatura` int(11) NOT NULL,
  `id_paralelo` int(11) NOT NULL,
  `rp_tema` varchar(64) CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL,
  `rp_fec_envio` date NOT NULL,
  `rp_fec_evaluacion` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_rubrica_proyecto`
--

DROP TABLE IF EXISTS `sw_rubrica_proyecto`;
CREATE TABLE `sw_rubrica_proyecto` (
  `id_rubrica_proyecto` int(11) NOT NULL,
  `id_aporte_evaluacion` int(11) DEFAULT NULL,
  `rp_nombre` varchar(36) DEFAULT NULL,
  `rp_abreviatura` varchar(7) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_tarea`
--

DROP TABLE IF EXISTS `sw_tarea`;
CREATE TABLE `sw_tarea` (
  `id` int(11) NOT NULL,
  `tarea` varchar(255) NOT NULL,
  `hecho` tinyint(1) NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_tema`
--

DROP TABLE IF EXISTS `sw_tema`;
CREATE TABLE `sw_tema` (
  `id_tema` int(11) NOT NULL,
  `id_foro` int(11) NOT NULL,
  `te_titulo` varchar(50) NOT NULL,
  `te_descripcion` text NOT NULL,
  `te_fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_tipo_aporte`
--

DROP TABLE IF EXISTS `sw_tipo_aporte`;
CREATE TABLE `sw_tipo_aporte` (
  `id_tipo_aporte` int(11) NOT NULL,
  `ta_descripcion` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_tipo_asignatura`
--

DROP TABLE IF EXISTS `sw_tipo_asignatura`;
CREATE TABLE `sw_tipo_asignatura` (
  `id_tipo_asignatura` int(11) NOT NULL,
  `ta_descripcion` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_tipo_educacion`
--

DROP TABLE IF EXISTS `sw_tipo_educacion`;
CREATE TABLE `sw_tipo_educacion` (
  `id_tipo_educacion` int(11) NOT NULL,
  `id_periodo_lectivo` int(11) NOT NULL,
  `te_nombre` varchar(48) CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL,
  `te_bachillerato` tinyint(11) NOT NULL,
  `te_orden` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `sw_tipo_educacion`
--

INSERT INTO `sw_tipo_educacion` (`id_tipo_educacion`, `id_periodo_lectivo`, `te_nombre`, `te_bachillerato`, `te_orden`) VALUES
(1, 1, 'Educación General Básica Superior', 0, 1),
(2, 1, 'Bachillerato Técnico', 1, 3),
(3, 1, 'Bachillerato General Unificado', 1, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_tipo_periodo`
--

DROP TABLE IF EXISTS `sw_tipo_periodo`;
CREATE TABLE `sw_tipo_periodo` (
  `id_tipo_periodo` int(11) NOT NULL,
  `tp_descripcion` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_tipo_rubrica`
--

DROP TABLE IF EXISTS `sw_tipo_rubrica`;
CREATE TABLE `sw_tipo_rubrica` (
  `id_tipo_rubrica` int(11) NOT NULL,
  `tr_descripcion` varchar(16) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_usuario`
--

DROP TABLE IF EXISTS `sw_usuario`;
CREATE TABLE `sw_usuario` (
  `id_usuario` int(11) NOT NULL,
  `id_periodo_lectivo` int(11) NOT NULL,
  `id_perfil` int(11) NOT NULL,
  `us_titulo` varchar(6) CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL,
  `us_apellidos` varchar(32) CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL,
  `us_nombres` varchar(32) CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL,
  `us_shortname` varchar(45) NOT NULL,
  `us_fullname` varchar(64) CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL,
  `us_login` varchar(24) CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL,
  `us_password` varchar(535) CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL,
  `us_foto` varchar(100) NOT NULL,
  `us_genero` varchar(1) NOT NULL,
  `us_activo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `sw_usuario`
--

INSERT INTO `sw_usuario` (`id_usuario`, `id_periodo_lectivo`, `id_perfil`, `us_titulo`, `us_apellidos`, `us_nombres`, `us_shortname`, `us_fullname`, `us_login`, `us_password`, `us_foto`, `us_genero`, `us_activo`) VALUES
(1, 1, 1, 'Ing.', 'Peñaherrera Escobar', 'Gonzalo Nicolás', '  Ing. Gonzalo Peñaherrera', 'Peñaherrera Escobar Gonzalo Nicolás ', 'administrador', '07b476488fc3e56db688ec3a0f7e41a2bcf647ff8a97867f5482b5f983921433fe7b1c972c6ae5a6e889a18f178b3d42d8517fcf1bb35d8bf7e7f87ebbe997dfqEgN80SawNCk9GE/KIfIiyeiIeGLtlyGQuiTx3p8S4U=', 'gonzalofoto.jpg', 'M', 1),
(2, 1, 5, 'Dr.', 'Castillo Cabay', 'Ramiro Vicente', 'Dr. Ramiro Castillo', 'Castillo Cabay Ramiro Vicente', 'ramiroc', 'b77c9853274734eb8cdec7c4ffd65f0c643b09e996dfa2351d075c07dae2abdcea83450ebc6d8530f12027b42c36d955fe60e295a5a92d298029e52c3cd95283HSB5v9193LHyS/g+FhxCycpt6BYYrkNUinal9OA/X0U=', 'rector.png', 'M', 1),
(3, 1, 5, 'Lic.', 'Mejía Segarra', 'Rómulo Oswaldo', 'Lic. Rómulo Mejía', 'Mejía Segarra Rómulo Oswaldo', 'romulom', '78a860b4e4a14ed47ef8f46154ba1f672648c6066cae96eff90dfb693a5a4117d6db3c50a633a063dfa0e82312c27ee27cc4c843c11d8bf318e9fac73137c4cbIIlm3QXXtbwasI5RnSN1mijwfQ+MR17ZUgE74VO4lzE=', 'avatar5.png', 'M', 1),
(4, 1, 7, 'Ps.', 'Suasnavas', 'Iván', 'Ps. Iván Suasnavas', 'Suasnavas Iván', 'ivans', '6ecfefda2b0dd6d419f8f048296bcd85e4e53adb1e29187e8726e180fd52aaaebfa1113b1ecc5aaa6913f22ce61a265b5543fc11549d6ce633dbfebb26762fe0Y9xro8K75K4tnJvUpaYjdaRMY75WYIf1Jivp6Ytzk58=', 'free_person_male_avatar.png', 'M', 1),
(5, 1, 2, 'Mg.', 'Barragán García', 'Ana Mirian', 'Mg. Ana Barragán', 'Barragán García Ana Mirian', 'mirianb', 'b6a1f6613a4cea5b0c4137a37c62228c81f52dbe2ddaf03bd52dfc89ebf4087e285f427ba7883e356a0bd6ab2763d9d0aa10b3e4e4c858d02ff3e9bce7664d9aN4HEVcbx2aJYqfEqbLkzHWf6qVuMY42tzOBWM/h3Cqo=', 'avatar2.png', 'F', 1),
(6, 1, 2, 'Tlgo.', 'Cabascango Herrera', 'Milton Fabián', 'Tlgo. Milton Cabascango', 'Cabascango Herrera Milton Fabián', 'miltonc', 'cb4c49d4af4283991c6248bfd5c88c645853b45b5a67f5ae29a0c02721e06f60371c259f7afbbbaa94f554b71df50f7133379b7f79563e6a7b6e54c6416d654fs9pxdMDfxVd+x2O0o1Q+MjfPtf1nmdUSs/o+aGjlhr0=', 'student_male_avatar.png', 'M', 1),
(7, 1, 2, 'Lic.', 'Calero Navarrete', 'Elmo Eduardo', 'Lic. Elmo Calero', 'Calero Navarrete Elmo Eduardo', 'elmoc', '75db214b8faff92f7787dc10795a25ba9bb307a2ebc299029e42f7a0972a5c67023ffd1e1e3e64687f953a821f81ee2cd8c951f2c83b998893886698902ed528QurXvubot11FL28Xh9BGzrxcNx0SiLk2hHf5gWHnTv8=', 'student_male_avatar.png', 'M', 1),
(8, 1, 2, 'Msc.', 'Caraguay Prócel', 'Carlos Hugo', 'Msc. Carlos Caraguay', 'Caraguay Prócel Carlos Hugo', 'carlosc', '977ecb14d1e42c5dcd599266d5ad22313b9b9c91ca0f59d10a202a230fed77f8d36d552689abef6803d2e0e92f896be37e1bdd57a5ab55c256528018ce915f07oVO6MZmlB9mdgXx1P3bB2404UhGqOW765USqZsq4xP0=', 'teacher-male-avatar.png', 'M', 1),
(9, 1, 2, 'Lic.', 'Cedeño Zambrano', 'Edith Monserrate', 'Lic. Edith Cedeño', 'Cedeño Zambrano Edith Monserrate', 'edithc', '806fd59abfd999a2fdf6d3ee5b2dd6bdca6b8ffb4a8de55cbc0424673a974a8a10c6c527a33c1658612d37860ac5418fdaf1729014bafe62907463546a3c2403F/8Mi6wXafeO2rRhivuP/2VVVMRyGEmIWuqPV1sNa+U=', 'teacher-female-avatar.png', 'F', 1),
(10, 1, 2, 'Dr.', 'Enríquez Martínez', 'Carlos Alberto', 'Dr. Carlos Enríquez', 'Enríquez Martínez Carlos Alberto', 'carlose', 'a637154bab30c3027e25bcce4efb6b1eb27059b2672e4f5c2680fc2a8a9cc378ee14864e5f6ea8381b1fd0ef8325ef7fe21df2d5a5e7bcc3d4eea04dc0ceb58fsogN5etLOfodn5x3LM3LjabyYXJfOQO8GQgHG7a83o4=', 'teacher-male-avatar.png', 'M', 1),
(11, 1, 2, 'Dr.', 'Guamán Calderón', 'Luis Alfredo', 'Dr. Luis Guamán', 'Guamán Calderón Luis Alfredo', 'luisg', 'e93bcae00e1d8e208b2d3c1a4fc4440808550aca3d51baf9197acec178a5fc08df00d2543e054dd82cd284783d9069a57276aee491479934959edc3fcb9af55aTEAkl8FGShDUE960ddxpDtKvgQipYEcCS87/ruseVGw=', 'teacher-male-avatar.png', 'M', 1),
(12, 1, 2, 'Lic.', 'Montenegro Yépez', 'Efrén', 'Lic. Efrén Montenegro', 'Montenegro Yépez Efrén', 'efrenm', '3218e7e839f146c42c86c7b7d0f3fc4aa7bbece837fb359ce65eaee028e7e92280425e280134067c41c7c3dfa0181cfc87013cc1953709bf4b7999e42d0ef513XgzFUiDavWUv3WBaR8DX/M4vIHu0MhQlLasfNRvT5D4=', 'teacher-male-avatar.png', 'M', 1),
(13, 1, 2, 'Lic.', 'Noguera Moscoso', 'Patricia Gimena', 'Lic. Patricia Noguera', 'Noguera Moscoso Patricia Gimena', 'patrician', 'ce80bc34df5bfdf1a98be94ecfea04b927f2287726861c7cc68e074386e8480f1fac4f5b29f41bda19bf609435bb9d073daeda303b6aec612ab9db77d47ac109cYNEtqzfcO/FbI5iq4obOONs9SsPI5fpusT3iTojKlE=', 'teacher-female-avatar.png', 'F', 1),
(14, 1, 2, 'Ing.', 'Peñaherrera Escobar', 'Gonzalo Nicolás', 'Ing. Gonzalo Peñaherrera', 'Peñaherrera Escobar Gonzalo Nicolás', 'gonzalop', '07b476488fc3e56db688ec3a0f7e41a2bcf647ff8a97867f5482b5f983921433fe7b1c972c6ae5a6e889a18f178b3d42d8517fcf1bb35d8bf7e7f87ebbe997dfqEgN80SawNCk9GE/KIfIiyeiIeGLtlyGQuiTx3p8S4U=', 'teacher-male-avatar.png', 'M', 1),
(15, 1, 2, 'Lic.', 'Proaño Estrella', 'Wilson Eduardo', 'Lic. Wilson Proaño', 'Proaño Estrella Wilson Eduardo', 'wilsonp', 'da771093b7b062ac00e97f2a6fba18a5a9b14f0c855ac38c176a3191d9e49fc1120a5b480c66a97406d74068e540341a09dcf4b713c80721f17b69432101e82bmVlcjOJ6EMi8b2TiB8bJfEx0gR1v5FdZs9StkbMudBU=', 'teacher-male-avatar.png', 'M', 1),
(16, 1, 2, 'Lic.', 'Quevedo Barrezueta', 'Alfonso Miguel', 'Lic. Alfonso Quevedo', 'Quevedo Barrezueta Alfonso Miguel', 'alfonsoq', '7db4a1813a169c19e37b6f0ae7cfa3fdbc4bef1e70652def5df6be0e11c04e97df0074ee3d6cb4bb72899f94a3b6027755bce33278addb0edf1d8b8065c42770mC5nRE1+FsCyFek2nv7C/ehsaKeRIlrGqohFoTtCuSM=', 'teacher-male-avatar.png', 'M', 1),
(17, 1, 2, 'Msc.', 'Rosero Medina', 'Roberto Hernán', 'Msc. Roberto Rosero', 'Rosero Medina Roberto Hernán', 'robertos', '628dad180d3fbdffdf1b038453e9fefaabd2b988a07dd01199cdeccd6af399c84b9ab77225a9e409307bf6be12a5ae595e8628b25528eb7f75612f12dbb6250bVyen1Qg0arXbKxTSKmx364gsfljsjhECL5m7B/v1AUU=', 'teacher-male-avatar.png', 'M', 1),
(18, 1, 2, 'Lic.', 'Salazar Ordóñez', 'Alicia', 'Lic. Alicia Salazar', 'Salazar Ordóñez Alicia', 'alicias', '91810a1ab5dca0844b21c693b43ac4c7d30d4e60536e586bccb5c206eef8ef63072c33c4f57638bad081b92554bc636a9d3d80321e369079abd08fba21ea0ed9k8rISGU+qSBQXYmfI6jL7W0grg9S6FrKLPAe9zcfQEQ=', 'teacher-female-avatar.png', 'F', 1),
(19, 1, 2, 'Lic.', 'Salgado Araujo', 'Rosario', 'Lic. Rosario Salgado', 'Salgado Araujo Rosario', 'rosarios', '532fc15d777a51d5a759c759c6d698c0b4f5900073c21ba22cf0de57b5bffae6504c6138b877389df3c6923e6d86d1393e3d5f54ea82c7ad5b9ea62e8669baafv7RP5LkqgLDIPGO3K3cW65MDMnjgGjgjtR1rdmFlfBI=', 'teacher-female-avatar.png', 'F', 1),
(20, 1, 2, 'Lic.', 'Zambrano Cedeño', 'Walter Adbón', 'Lic. Walter Zambrano', 'Zambrano Cedeño Walter Adbón', 'walterz', '1bd7b8f1b74f8047fe24ffe6dc8921cce89d0c7145b25fce906a04972bb48169310f71a58e84b08d253a16012af3b58e396d31fea315362096bcde44097c420dKAadhXnUwnVZQ4mdJ3QzblWeysFbE4N2cgH5tUPZD+8=', 'teacher_male_avatar.png', 'M', 1),
(21, 1, 3, 'Mr.', 'Secretaría', 'Personal de', 'Mr. Personal Secretaría', 'Secretaría Personal de', 'secretaria', '4c03aeed7e5aeeaa2e4f7a0f4af8e7844252a81bea19fcb45e4ac81def3944991700b16f0038e713bc0178175693e1151a84e81bded2c0464c0078d07265626ahwPMBdSk5wb8E9In/ooZg1J5BMlF0e1StmPxPuujDGc=', 'teacher-male-avatar.png', 'M', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_usuario_perfil`
--

DROP TABLE IF EXISTS `sw_usuario_perfil`;
CREATE TABLE `sw_usuario_perfil` (
  `id_usuario` int(11) NOT NULL,
  `id_perfil` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `sw_usuario_perfil`
--

INSERT INTO `sw_usuario_perfil` (`id_usuario`, `id_perfil`) VALUES
(1, 1),
(2, 2),
(2, 5),
(2, 6),
(3, 2),
(3, 5),
(3, 6),
(4, 7),
(5, 2),
(5, 6),
(6, 2),
(6, 6),
(7, 2),
(7, 6),
(8, 2),
(9, 2),
(9, 6),
(10, 2),
(10, 6),
(11, 2),
(11, 6),
(12, 2),
(12, 6),
(13, 2),
(13, 6),
(14, 2),
(14, 6),
(15, 2),
(15, 6),
(16, 2),
(16, 4),
(16, 6),
(17, 2),
(17, 6),
(18, 2),
(18, 6),
(19, 2),
(19, 6),
(20, 2),
(20, 6),
(21, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_valor_mes`
--

DROP TABLE IF EXISTS `sw_valor_mes`;
CREATE TABLE `sw_valor_mes` (
  `id_valor_mes` int(11) NOT NULL,
  `id_periodo_lectivo` int(11) NOT NULL,
  `vm_mes` int(11) NOT NULL,
  `vm_valor` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `sw_aporte_curso_cierre`
--
ALTER TABLE `sw_aporte_curso_cierre`
  ADD KEY `id_aporte_evaluacion` (`id_aporte_evaluacion`),
  ADD KEY `id_curso` (`id_curso`);

--
-- Indices de la tabla `sw_aporte_evaluacion`
--
ALTER TABLE `sw_aporte_evaluacion`
  ADD PRIMARY KEY (`id_aporte_evaluacion`),
  ADD KEY `id_periodo_evaluacion` (`id_periodo_evaluacion`);

--
-- Indices de la tabla `sw_area`
--
ALTER TABLE `sw_area`
  ADD PRIMARY KEY (`id_area`);

--
-- Indices de la tabla `sw_asignatura`
--
ALTER TABLE `sw_asignatura`
  ADD PRIMARY KEY (`id_asignatura`),
  ADD KEY `id_area` (`id_area`);

--
-- Indices de la tabla `sw_asignatura_curso`
--
ALTER TABLE `sw_asignatura_curso`
  ADD PRIMARY KEY (`id_asignatura_curso`),
  ADD KEY `id_curso` (`id_curso`),
  ADD KEY `id_asignatura` (`id_asignatura`);

--
-- Indices de la tabla `sw_asistencia_estudiante`
--
ALTER TABLE `sw_asistencia_estudiante`
  ADD PRIMARY KEY (`id_asistencia_estudiante`),
  ADD KEY `id_estudiante` (`id_estudiante`,`id_asignatura`,`id_paralelo`,`id_inasistencia`),
  ADD KEY `id_asignatura` (`id_asignatura`),
  ADD KEY `id_paralelo` (`id_paralelo`),
  ADD KEY `id_inasistencia` (`id_inasistencia`),
  ADD KEY `sw_asistencia_estudiante_ibfk_1` (`id_hora_clase`),
  ADD KEY `id_dia_semana` (`id_dia_semana`);

--
-- Indices de la tabla `sw_asociar_curso_superior`
--
ALTER TABLE `sw_asociar_curso_superior`
  ADD PRIMARY KEY (`id_asociar_curso_superior`),
  ADD KEY `fk_curso_superior_periodo_lectivo_idx` (`id_periodo_lectivo`),
  ADD KEY `fk_curso_inferior_curso_idx` (`id_curso_inferior`),
  ADD KEY `fk_curso_superior_curso_idx` (`id_curso_superior`);

--
-- Indices de la tabla `sw_calificacion_comportamiento`
--
ALTER TABLE `sw_calificacion_comportamiento`
  ADD KEY `id_paralelo` (`id_paralelo`,`id_estudiante`,`id_aporte_evaluacion`,`id_asignatura`);

--
-- Indices de la tabla `sw_club`
--
ALTER TABLE `sw_club`
  ADD PRIMARY KEY (`id_club`);

--
-- Indices de la tabla `sw_club_docente`
--
ALTER TABLE `sw_club_docente`
  ADD PRIMARY KEY (`id_club_docente`);

--
-- Indices de la tabla `sw_comentario`
--
ALTER TABLE `sw_comentario`
  ADD PRIMARY KEY (`id_comentario`);

--
-- Indices de la tabla `sw_comportamiento_inspector`
--
ALTER TABLE `sw_comportamiento_inspector`
  ADD PRIMARY KEY (`id_comportamiento_inspector`),
  ADD KEY `id_paralelo` (`id_paralelo`),
  ADD KEY `id_estudiante` (`id_estudiante`),
  ADD KEY `id_aporte_evaluacion` (`id_aporte_evaluacion`),
  ADD KEY `id_indice_evaluacion` (`id_escala_comportamiento`);

--
-- Indices de la tabla `sw_curso`
--
ALTER TABLE `sw_curso`
  ADD PRIMARY KEY (`id_curso`),
  ADD KEY `id_especialidad` (`id_especialidad`);

--
-- Indices de la tabla `sw_curso_superior`
--
ALTER TABLE `sw_curso_superior`
  ADD PRIMARY KEY (`id_curso_superior`);

--
-- Indices de la tabla `sw_dia_feriado`
--
ALTER TABLE `sw_dia_feriado`
  ADD PRIMARY KEY (`id_dia_feriado`),
  ADD KEY `id_periodo_lectivo` (`id_periodo_lectivo`);

--
-- Indices de la tabla `sw_dia_semana`
--
ALTER TABLE `sw_dia_semana`
  ADD PRIMARY KEY (`id_dia_semana`),
  ADD KEY `id_periodo_lectivo` (`id_periodo_lectivo`);

--
-- Indices de la tabla `sw_distributivo`
--
ALTER TABLE `sw_distributivo`
  ADD PRIMARY KEY (`id_distributivo`),
  ADD KEY `id_asignatura` (`id_asignatura`),
  ADD KEY `id_malla_curricular` (`id_malla_curricular`),
  ADD KEY `id_paralelo` (`id_paralelo`),
  ADD KEY `id_usuario` (`id_usuario`),
  ADD KEY `id_periodo_lectivo` (`id_periodo_lectivo`);

--
-- Indices de la tabla `sw_escala_calificaciones`
--
ALTER TABLE `sw_escala_calificaciones`
  ADD PRIMARY KEY (`id_escala_calificaciones`);

--
-- Indices de la tabla `sw_escala_comportamiento`
--
ALTER TABLE `sw_escala_comportamiento`
  ADD PRIMARY KEY (`id_escala_comportamiento`);

--
-- Indices de la tabla `sw_escala_proyectos`
--
ALTER TABLE `sw_escala_proyectos`
  ADD PRIMARY KEY (`id_escala_proyectos`);

--
-- Indices de la tabla `sw_especialidad`
--
ALTER TABLE `sw_especialidad`
  ADD PRIMARY KEY (`id_especialidad`),
  ADD KEY `id_tipo_educacion` (`id_tipo_educacion`);

--
-- Indices de la tabla `sw_estudiante`
--
ALTER TABLE `sw_estudiante`
  ADD PRIMARY KEY (`id_estudiante`);

--
-- Indices de la tabla `sw_estudiante_club`
--
ALTER TABLE `sw_estudiante_club`
  ADD PRIMARY KEY (`id_estudiante_club`);

--
-- Indices de la tabla `sw_estudiante_periodo_lectivo`
--
ALTER TABLE `sw_estudiante_periodo_lectivo`
  ADD PRIMARY KEY (`id_estudiante_periodo_lectivo`),
  ADD KEY `id_estudiante` (`id_estudiante`,`id_periodo_lectivo`,`id_paralelo`),
  ADD KEY `id_periodo_lectivo` (`id_periodo_lectivo`),
  ADD KEY `id_paralelo` (`id_paralelo`);

--
-- Indices de la tabla `sw_estudiante_promedio_parcial`
--
ALTER TABLE `sw_estudiante_promedio_parcial`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_estudiante` (`id_estudiante`),
  ADD KEY `id_aporte_evaluacion` (`id_aporte_evaluacion`),
  ADD KEY `id_paralelo` (`id_paralelo`);

--
-- Indices de la tabla `sw_estudiante_prom_anual`
--
ALTER TABLE `sw_estudiante_prom_anual`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `sw_estudiante_prom_quimestral`
--
ALTER TABLE `sw_estudiante_prom_quimestral`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_estudiante` (`id_estudiante`),
  ADD KEY `id_paralelo` (`id_paralelo`),
  ADD KEY `id_periodo_evaluacion` (`id_periodo_evaluacion`);

--
-- Indices de la tabla `sw_feriado`
--
ALTER TABLE `sw_feriado`
  ADD PRIMARY KEY (`id_feriado`),
  ADD KEY `id_periodo_lectivo` (`id_periodo_lectivo`);

--
-- Indices de la tabla `sw_foro`
--
ALTER TABLE `sw_foro`
  ADD PRIMARY KEY (`id_foro`);

--
-- Indices de la tabla `sw_horario`
--
ALTER TABLE `sw_horario`
  ADD PRIMARY KEY (`id_horario`),
  ADD KEY `id_hora_clase` (`id_hora_clase`),
  ADD KEY `sw_horario_ibfk_1` (`id_asignatura`),
  ADD KEY `sw_horario_ibfk_2` (`id_dia_semana`),
  ADD KEY `sw_horario_ibfk_4` (`id_paralelo`);

--
-- Indices de la tabla `sw_horario_examen`
--
ALTER TABLE `sw_horario_examen`
  ADD PRIMARY KEY (`id_horario_examen`);

--
-- Indices de la tabla `sw_hora_clase`
--
ALTER TABLE `sw_hora_clase`
  ADD PRIMARY KEY (`id_hora_clase`),
  ADD KEY `id_periodo_lectivo` (`id_periodo_lectivo`);

--
-- Indices de la tabla `sw_hora_dia`
--
ALTER TABLE `sw_hora_dia`
  ADD PRIMARY KEY (`id_hora_dia`),
  ADD KEY `sw_hora_dia_ibfk_1` (`id_dia_semana`),
  ADD KEY `sw_hora_dia_ibfk_2` (`id_hora_clase`);

--
-- Indices de la tabla `sw_inasistencia`
--
ALTER TABLE `sw_inasistencia`
  ADD PRIMARY KEY (`id_inasistencia`);

--
-- Indices de la tabla `sw_indice_evaluacion`
--
ALTER TABLE `sw_indice_evaluacion`
  ADD PRIMARY KEY (`id_indice_evaluacion`);

--
-- Indices de la tabla `sw_indice_evaluacion_def`
--
ALTER TABLE `sw_indice_evaluacion_def`
  ADD PRIMARY KEY (`id_indice_evaluacion`);

--
-- Indices de la tabla `sw_institucion`
--
ALTER TABLE `sw_institucion`
  ADD PRIMARY KEY (`id_institucion`);

--
-- Indices de la tabla `sw_malla_curricular`
--
ALTER TABLE `sw_malla_curricular`
  ADD PRIMARY KEY (`id_malla_curricular`),
  ADD KEY `id_asignatura` (`id_asignatura`),
  ADD KEY `id_paralelo` (`id_paralelo`),
  ADD KEY `id_periodo_lectivo` (`id_periodo_lectivo`);

--
-- Indices de la tabla `sw_mensaje`
--
ALTER TABLE `sw_mensaje`
  ADD PRIMARY KEY (`id_mensaje`);

--
-- Indices de la tabla `sw_menu`
--
ALTER TABLE `sw_menu`
  ADD PRIMARY KEY (`id_menu`),
  ADD KEY `id_perfil` (`id_perfil`);

--
-- Indices de la tabla `sw_modalidad`
--
ALTER TABLE `sw_modalidad`
  ADD PRIMARY KEY (`id_modalidad`);

--
-- Indices de la tabla `sw_paralelo`
--
ALTER TABLE `sw_paralelo`
  ADD PRIMARY KEY (`id_paralelo`),
  ADD KEY `id_curso` (`id_curso`);

--
-- Indices de la tabla `sw_paralelo_asignatura`
--
ALTER TABLE `sw_paralelo_asignatura`
  ADD PRIMARY KEY (`id_paralelo_asignatura`),
  ADD KEY `id_paralelo` (`id_paralelo`,`id_asignatura`),
  ADD KEY `id_asignatura` (`id_asignatura`),
  ADD KEY `id_usuario` (`id_usuario`),
  ADD KEY `id_periodo_lectivo` (`id_periodo_lectivo`);

--
-- Indices de la tabla `sw_paralelo_inspector`
--
ALTER TABLE `sw_paralelo_inspector`
  ADD PRIMARY KEY (`id_paralelo_inspector`);

--
-- Indices de la tabla `sw_paralelo_tutor`
--
ALTER TABLE `sw_paralelo_tutor`
  ADD PRIMARY KEY (`id_paralelo_tutor`);

--
-- Indices de la tabla `sw_perfil`
--
ALTER TABLE `sw_perfil`
  ADD PRIMARY KEY (`id_perfil`);

--
-- Indices de la tabla `sw_periodo_estado`
--
ALTER TABLE `sw_periodo_estado`
  ADD PRIMARY KEY (`id_periodo_estado`);

--
-- Indices de la tabla `sw_periodo_evaluacion`
--
ALTER TABLE `sw_periodo_evaluacion`
  ADD PRIMARY KEY (`id_periodo_evaluacion`),
  ADD KEY `id_periodo_lectivo` (`id_periodo_lectivo`);

--
-- Indices de la tabla `sw_periodo_lectivo`
--
ALTER TABLE `sw_periodo_lectivo`
  ADD PRIMARY KEY (`id_periodo_lectivo`),
  ADD KEY `id_institucion` (`id_institucion`);

--
-- Indices de la tabla `sw_permiso`
--
ALTER TABLE `sw_permiso`
  ADD PRIMARY KEY (`id_permiso`),
  ADD KEY `fk_menus_idx` (`id_menu`),
  ADD KEY `fk_perfiles_idx` (`id_perfil`);

--
-- Indices de la tabla `sw_plan_rubrica`
--
ALTER TABLE `sw_plan_rubrica`
  ADD PRIMARY KEY (`id_plan_rubrica`);

--
-- Indices de la tabla `sw_representante`
--
ALTER TABLE `sw_representante`
  ADD PRIMARY KEY (`id_representante`),
  ADD KEY `fk_representante_estudiante_idx` (`id_estudiante`);

--
-- Indices de la tabla `sw_respuesta`
--
ALTER TABLE `sw_respuesta`
  ADD PRIMARY KEY (`id_respuesta`),
  ADD KEY `id_tema` (`id_tema`);

--
-- Indices de la tabla `sw_rubrica_club`
--
ALTER TABLE `sw_rubrica_club`
  ADD PRIMARY KEY (`id_rubrica_club`);

--
-- Indices de la tabla `sw_rubrica_docente`
--
ALTER TABLE `sw_rubrica_docente`
  ADD KEY `id_rubrica_evaluacion` (`id_rubrica_evaluacion`,`id_usuario`),
  ADD KEY `id_docente` (`id_usuario`),
  ADD KEY `id_asignatura` (`id_asignatura`),
  ADD KEY `id_paralelo` (`id_paralelo`);

--
-- Indices de la tabla `sw_rubrica_estudiante`
--
ALTER TABLE `sw_rubrica_estudiante`
  ADD PRIMARY KEY (`id_rubrica_estudiante`),
  ADD KEY `id_estudiante` (`id_estudiante`),
  ADD KEY `id_paralelo` (`id_paralelo`),
  ADD KEY `id_asignatura` (`id_asignatura`),
  ADD KEY `id_rubrica_personalizada` (`id_rubrica_personalizada`);

--
-- Indices de la tabla `sw_rubrica_evaluacion`
--
ALTER TABLE `sw_rubrica_evaluacion`
  ADD PRIMARY KEY (`id_rubrica_evaluacion`),
  ADD KEY `id_aporte_evaluacion` (`id_aporte_evaluacion`),
  ADD KEY `id_tipo_asignatura` (`id_tipo_asignatura`);

--
-- Indices de la tabla `sw_rubrica_evaluacion_club`
--
ALTER TABLE `sw_rubrica_evaluacion_club`
  ADD PRIMARY KEY (`id_rubrica_evaluacion_club`);

--
-- Indices de la tabla `sw_rubrica_personalizada`
--
ALTER TABLE `sw_rubrica_personalizada`
  ADD PRIMARY KEY (`id_rubrica_personalizada`),
  ADD KEY `id_rubrica_evaluacion` (`id_rubrica_evaluacion`,`id_usuario`),
  ADD KEY `id_asignatura` (`id_asignatura`),
  ADD KEY `id_usuario` (`id_usuario`),
  ADD KEY `id_paralelo` (`id_paralelo`);

--
-- Indices de la tabla `sw_rubrica_proyecto`
--
ALTER TABLE `sw_rubrica_proyecto`
  ADD PRIMARY KEY (`id_rubrica_proyecto`),
  ADD KEY `fk_rubrica_proyecto_id_aporte_evaluacion_idx` (`id_aporte_evaluacion`);

--
-- Indices de la tabla `sw_tarea`
--
ALTER TABLE `sw_tarea`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `sw_tema`
--
ALTER TABLE `sw_tema`
  ADD PRIMARY KEY (`id_tema`),
  ADD KEY `id_foro` (`id_foro`);

--
-- Indices de la tabla `sw_tipo_aporte`
--
ALTER TABLE `sw_tipo_aporte`
  ADD PRIMARY KEY (`id_tipo_aporte`);

--
-- Indices de la tabla `sw_tipo_asignatura`
--
ALTER TABLE `sw_tipo_asignatura`
  ADD PRIMARY KEY (`id_tipo_asignatura`);

--
-- Indices de la tabla `sw_tipo_educacion`
--
ALTER TABLE `sw_tipo_educacion`
  ADD PRIMARY KEY (`id_tipo_educacion`),
  ADD KEY `id_periodo_lectivo` (`id_periodo_lectivo`);

--
-- Indices de la tabla `sw_tipo_periodo`
--
ALTER TABLE `sw_tipo_periodo`
  ADD PRIMARY KEY (`id_tipo_periodo`);

--
-- Indices de la tabla `sw_tipo_rubrica`
--
ALTER TABLE `sw_tipo_rubrica`
  ADD PRIMARY KEY (`id_tipo_rubrica`);

--
-- Indices de la tabla `sw_usuario`
--
ALTER TABLE `sw_usuario`
  ADD PRIMARY KEY (`id_usuario`),
  ADD UNIQUE KEY `us_login` (`us_login`),
  ADD KEY `id_perfil` (`id_perfil`),
  ADD KEY `id_periodo_lectivo` (`id_periodo_lectivo`);

--
-- Indices de la tabla `sw_usuario_perfil`
--
ALTER TABLE `sw_usuario_perfil`
  ADD PRIMARY KEY (`id_usuario`,`id_perfil`),
  ADD KEY `id_usuario` (`id_usuario`),
  ADD KEY `id_perfil` (`id_perfil`);

--
-- Indices de la tabla `sw_valor_mes`
--
ALTER TABLE `sw_valor_mes`
  ADD PRIMARY KEY (`id_valor_mes`),
  ADD KEY `id_periodo_lectivo` (`id_periodo_lectivo`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `sw_aporte_evaluacion`
--
ALTER TABLE `sw_aporte_evaluacion`
  MODIFY `id_aporte_evaluacion` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `sw_area`
--
ALTER TABLE `sw_area`
  MODIFY `id_area` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `sw_asignatura`
--
ALTER TABLE `sw_asignatura`
  MODIFY `id_asignatura` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `sw_asignatura_curso`
--
ALTER TABLE `sw_asignatura_curso`
  MODIFY `id_asignatura_curso` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `sw_asistencia_estudiante`
--
ALTER TABLE `sw_asistencia_estudiante`
  MODIFY `id_asistencia_estudiante` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `sw_asociar_curso_superior`
--
ALTER TABLE `sw_asociar_curso_superior`
  MODIFY `id_asociar_curso_superior` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `sw_club`
--
ALTER TABLE `sw_club`
  MODIFY `id_club` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `sw_club_docente`
--
ALTER TABLE `sw_club_docente`
  MODIFY `id_club_docente` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `sw_comentario`
--
ALTER TABLE `sw_comentario`
  MODIFY `id_comentario` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `sw_comportamiento_inspector`
--
ALTER TABLE `sw_comportamiento_inspector`
  MODIFY `id_comportamiento_inspector` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `sw_curso`
--
ALTER TABLE `sw_curso`
  MODIFY `id_curso` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `sw_curso_superior`
--
ALTER TABLE `sw_curso_superior`
  MODIFY `id_curso_superior` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `sw_dia_feriado`
--
ALTER TABLE `sw_dia_feriado`
  MODIFY `id_dia_feriado` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `sw_dia_semana`
--
ALTER TABLE `sw_dia_semana`
  MODIFY `id_dia_semana` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `sw_distributivo`
--
ALTER TABLE `sw_distributivo`
  MODIFY `id_distributivo` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `sw_escala_calificaciones`
--
ALTER TABLE `sw_escala_calificaciones`
  MODIFY `id_escala_calificaciones` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `sw_escala_comportamiento`
--
ALTER TABLE `sw_escala_comportamiento`
  MODIFY `id_escala_comportamiento` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `sw_escala_proyectos`
--
ALTER TABLE `sw_escala_proyectos`
  MODIFY `id_escala_proyectos` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `sw_especialidad`
--
ALTER TABLE `sw_especialidad`
  MODIFY `id_especialidad` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `sw_estudiante`
--
ALTER TABLE `sw_estudiante`
  MODIFY `id_estudiante` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `sw_estudiante_club`
--
ALTER TABLE `sw_estudiante_club`
  MODIFY `id_estudiante_club` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `sw_estudiante_periodo_lectivo`
--
ALTER TABLE `sw_estudiante_periodo_lectivo`
  MODIFY `id_estudiante_periodo_lectivo` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `sw_estudiante_promedio_parcial`
--
ALTER TABLE `sw_estudiante_promedio_parcial`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `sw_estudiante_prom_anual`
--
ALTER TABLE `sw_estudiante_prom_anual`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `sw_estudiante_prom_quimestral`
--
ALTER TABLE `sw_estudiante_prom_quimestral`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `sw_feriado`
--
ALTER TABLE `sw_feriado`
  MODIFY `id_feriado` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `sw_foro`
--
ALTER TABLE `sw_foro`
  MODIFY `id_foro` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `sw_horario`
--
ALTER TABLE `sw_horario`
  MODIFY `id_horario` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `sw_horario_examen`
--
ALTER TABLE `sw_horario_examen`
  MODIFY `id_horario_examen` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `sw_hora_clase`
--
ALTER TABLE `sw_hora_clase`
  MODIFY `id_hora_clase` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `sw_hora_dia`
--
ALTER TABLE `sw_hora_dia`
  MODIFY `id_hora_dia` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `sw_inasistencia`
--
ALTER TABLE `sw_inasistencia`
  MODIFY `id_inasistencia` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `sw_indice_evaluacion`
--
ALTER TABLE `sw_indice_evaluacion`
  MODIFY `id_indice_evaluacion` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `sw_indice_evaluacion_def`
--
ALTER TABLE `sw_indice_evaluacion_def`
  MODIFY `id_indice_evaluacion` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `sw_institucion`
--
ALTER TABLE `sw_institucion`
  MODIFY `id_institucion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `sw_malla_curricular`
--
ALTER TABLE `sw_malla_curricular`
  MODIFY `id_malla_curricular` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `sw_mensaje`
--
ALTER TABLE `sw_mensaje`
  MODIFY `id_mensaje` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `sw_menu`
--
ALTER TABLE `sw_menu`
  MODIFY `id_menu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=129;

--
-- AUTO_INCREMENT de la tabla `sw_modalidad`
--
ALTER TABLE `sw_modalidad`
  MODIFY `id_modalidad` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `sw_paralelo`
--
ALTER TABLE `sw_paralelo`
  MODIFY `id_paralelo` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `sw_paralelo_asignatura`
--
ALTER TABLE `sw_paralelo_asignatura`
  MODIFY `id_paralelo_asignatura` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `sw_paralelo_inspector`
--
ALTER TABLE `sw_paralelo_inspector`
  MODIFY `id_paralelo_inspector` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `sw_paralelo_tutor`
--
ALTER TABLE `sw_paralelo_tutor`
  MODIFY `id_paralelo_tutor` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `sw_perfil`
--
ALTER TABLE `sw_perfil`
  MODIFY `id_perfil` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `sw_periodo_estado`
--
ALTER TABLE `sw_periodo_estado`
  MODIFY `id_periodo_estado` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `sw_periodo_evaluacion`
--
ALTER TABLE `sw_periodo_evaluacion`
  MODIFY `id_periodo_evaluacion` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `sw_periodo_lectivo`
--
ALTER TABLE `sw_periodo_lectivo`
  MODIFY `id_periodo_lectivo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `sw_permiso`
--
ALTER TABLE `sw_permiso`
  MODIFY `id_permiso` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `sw_plan_rubrica`
--
ALTER TABLE `sw_plan_rubrica`
  MODIFY `id_plan_rubrica` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `sw_representante`
--
ALTER TABLE `sw_representante`
  MODIFY `id_representante` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `sw_respuesta`
--
ALTER TABLE `sw_respuesta`
  MODIFY `id_respuesta` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `sw_rubrica_club`
--
ALTER TABLE `sw_rubrica_club`
  MODIFY `id_rubrica_club` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `sw_rubrica_estudiante`
--
ALTER TABLE `sw_rubrica_estudiante`
  MODIFY `id_rubrica_estudiante` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `sw_rubrica_evaluacion`
--
ALTER TABLE `sw_rubrica_evaluacion`
  MODIFY `id_rubrica_evaluacion` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `sw_rubrica_evaluacion_club`
--
ALTER TABLE `sw_rubrica_evaluacion_club`
  MODIFY `id_rubrica_evaluacion_club` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `sw_rubrica_personalizada`
--
ALTER TABLE `sw_rubrica_personalizada`
  MODIFY `id_rubrica_personalizada` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `sw_rubrica_proyecto`
--
ALTER TABLE `sw_rubrica_proyecto`
  MODIFY `id_rubrica_proyecto` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `sw_tarea`
--
ALTER TABLE `sw_tarea`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `sw_tema`
--
ALTER TABLE `sw_tema`
  MODIFY `id_tema` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `sw_tipo_aporte`
--
ALTER TABLE `sw_tipo_aporte`
  MODIFY `id_tipo_aporte` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `sw_tipo_asignatura`
--
ALTER TABLE `sw_tipo_asignatura`
  MODIFY `id_tipo_asignatura` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `sw_tipo_educacion`
--
ALTER TABLE `sw_tipo_educacion`
  MODIFY `id_tipo_educacion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `sw_tipo_periodo`
--
ALTER TABLE `sw_tipo_periodo`
  MODIFY `id_tipo_periodo` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `sw_tipo_rubrica`
--
ALTER TABLE `sw_tipo_rubrica`
  MODIFY `id_tipo_rubrica` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `sw_usuario`
--
ALTER TABLE `sw_usuario`
  MODIFY `id_usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT de la tabla `sw_valor_mes`
--
ALTER TABLE `sw_valor_mes`
  MODIFY `id_valor_mes` int(11) NOT NULL AUTO_INCREMENT;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `sw_aporte_evaluacion`
--
ALTER TABLE `sw_aporte_evaluacion`
  ADD CONSTRAINT `sw_aporte_evaluacion_ibfk_1` FOREIGN KEY (`id_periodo_evaluacion`) REFERENCES `sw_periodo_evaluacion` (`id_periodo_evaluacion`);

--
-- Filtros para la tabla `sw_asignatura`
--
ALTER TABLE `sw_asignatura`
  ADD CONSTRAINT `sw_asignatura_ibfk_1` FOREIGN KEY (`id_area`) REFERENCES `sw_area` (`id_area`);

--
-- Filtros para la tabla `sw_asistencia_estudiante`
--
ALTER TABLE `sw_asistencia_estudiante`
  ADD CONSTRAINT `sw_asistencia_estudiante_ibfk_1` FOREIGN KEY (`id_hora_clase`) REFERENCES `sw_hora_clase` (`id_hora_clase`),
  ADD CONSTRAINT `sw_asistencia_estudiante_ibfk_2` FOREIGN KEY (`id_dia_semana`) REFERENCES `sw_dia_semana` (`id_dia_semana`);

--
-- Filtros para la tabla `sw_curso`
--
ALTER TABLE `sw_curso`
  ADD CONSTRAINT `sw_curso_ibfk_1` FOREIGN KEY (`id_especialidad`) REFERENCES `sw_especialidad` (`id_especialidad`);

--
-- Filtros para la tabla `sw_dia_feriado`
--
ALTER TABLE `sw_dia_feriado`
  ADD CONSTRAINT `sw_dia_feriado_ibfk_1` FOREIGN KEY (`id_periodo_lectivo`) REFERENCES `sw_periodo_lectivo` (`id_periodo_lectivo`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `sw_distributivo`
--
ALTER TABLE `sw_distributivo`
  ADD CONSTRAINT `sw_distributivo_ibfk_1` FOREIGN KEY (`id_asignatura`) REFERENCES `sw_asignatura` (`id_asignatura`),
  ADD CONSTRAINT `sw_distributivo_ibfk_2` FOREIGN KEY (`id_malla_curricular`) REFERENCES `sw_malla_curricular` (`id_malla_curricular`),
  ADD CONSTRAINT `sw_distributivo_ibfk_3` FOREIGN KEY (`id_paralelo`) REFERENCES `sw_paralelo` (`id_paralelo`),
  ADD CONSTRAINT `sw_distributivo_ibfk_4` FOREIGN KEY (`id_usuario`) REFERENCES `sw_usuario` (`id_usuario`),
  ADD CONSTRAINT `sw_distributivo_ibfk_5` FOREIGN KEY (`id_periodo_lectivo`) REFERENCES `sw_periodo_lectivo` (`id_periodo_lectivo`);

--
-- Filtros para la tabla `sw_especialidad`
--
ALTER TABLE `sw_especialidad`
  ADD CONSTRAINT `sw_especialidad_ibfk_1` FOREIGN KEY (`id_tipo_educacion`) REFERENCES `sw_tipo_educacion` (`id_tipo_educacion`);

--
-- Filtros para la tabla `sw_estudiante_periodo_lectivo`
--
ALTER TABLE `sw_estudiante_periodo_lectivo`
  ADD CONSTRAINT `sw_estudiante_periodo_lectivo_ibfk_1` FOREIGN KEY (`id_paralelo`) REFERENCES `sw_paralelo` (`id_paralelo`);

--
-- Filtros para la tabla `sw_estudiante_promedio_parcial`
--
ALTER TABLE `sw_estudiante_promedio_parcial`
  ADD CONSTRAINT `sw_estudiante_promedio_parcial_ibfk_1` FOREIGN KEY (`id_estudiante`) REFERENCES `sw_estudiante` (`id_estudiante`),
  ADD CONSTRAINT `sw_estudiante_promedio_parcial_ibfk_2` FOREIGN KEY (`id_aporte_evaluacion`) REFERENCES `sw_aporte_evaluacion` (`id_aporte_evaluacion`),
  ADD CONSTRAINT `sw_estudiante_promedio_parcial_ibfk_3` FOREIGN KEY (`id_paralelo`) REFERENCES `sw_paralelo` (`id_paralelo`);

--
-- Filtros para la tabla `sw_feriado`
--
ALTER TABLE `sw_feriado`
  ADD CONSTRAINT `sw_feriado_ibfk_1` FOREIGN KEY (`id_periodo_lectivo`) REFERENCES `sw_periodo_lectivo` (`id_periodo_lectivo`);

--
-- Filtros para la tabla `sw_horario`
--
ALTER TABLE `sw_horario`
  ADD CONSTRAINT `sw_horario_ibfk_1` FOREIGN KEY (`id_asignatura`) REFERENCES `sw_asignatura` (`id_asignatura`),
  ADD CONSTRAINT `sw_horario_ibfk_2` FOREIGN KEY (`id_dia_semana`) REFERENCES `sw_dia_semana` (`id_dia_semana`),
  ADD CONSTRAINT `sw_horario_ibfk_3` FOREIGN KEY (`id_hora_clase`) REFERENCES `sw_hora_clase` (`id_hora_clase`),
  ADD CONSTRAINT `sw_horario_ibfk_4` FOREIGN KEY (`id_paralelo`) REFERENCES `sw_paralelo` (`id_paralelo`);

--
-- Filtros para la tabla `sw_hora_clase`
--
ALTER TABLE `sw_hora_clase`
  ADD CONSTRAINT `sw_hora_clase_ibfk_1` FOREIGN KEY (`id_periodo_lectivo`) REFERENCES `sw_periodo_lectivo` (`id_periodo_lectivo`);

--
-- Filtros para la tabla `sw_hora_dia`
--
ALTER TABLE `sw_hora_dia`
  ADD CONSTRAINT `sw_hora_dia_ibfk_1` FOREIGN KEY (`id_dia_semana`) REFERENCES `sw_dia_semana` (`id_dia_semana`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `sw_hora_dia_ibfk_2` FOREIGN KEY (`id_hora_clase`) REFERENCES `sw_hora_clase` (`id_hora_clase`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `sw_malla_curricular`
--
ALTER TABLE `sw_malla_curricular`
  ADD CONSTRAINT `sw_malla_curricular_ibfk_1` FOREIGN KEY (`id_asignatura`) REFERENCES `sw_asignatura` (`id_asignatura`),
  ADD CONSTRAINT `sw_malla_curricular_ibfk_2` FOREIGN KEY (`id_paralelo`) REFERENCES `sw_paralelo` (`id_paralelo`),
  ADD CONSTRAINT `sw_malla_curricular_ibfk_3` FOREIGN KEY (`id_periodo_lectivo`) REFERENCES `sw_periodo_lectivo` (`id_periodo_lectivo`);

--
-- Filtros para la tabla `sw_paralelo`
--
ALTER TABLE `sw_paralelo`
  ADD CONSTRAINT `sw_paralelo_ibfk_1` FOREIGN KEY (`id_curso`) REFERENCES `sw_curso` (`id_curso`);

--
-- Filtros para la tabla `sw_rubrica_evaluacion`
--
ALTER TABLE `sw_rubrica_evaluacion`
  ADD CONSTRAINT `sw_rubrica_evaluacion_ibfk_1` FOREIGN KEY (`id_aporte_evaluacion`) REFERENCES `sw_aporte_evaluacion` (`id_aporte_evaluacion`),
  ADD CONSTRAINT `sw_rubrica_evaluacion_ibfk_2` FOREIGN KEY (`id_tipo_asignatura`) REFERENCES `sw_tipo_asignatura` (`id_tipo_asignatura`);

--
-- Filtros para la tabla `sw_tipo_educacion`
--
ALTER TABLE `sw_tipo_educacion`
  ADD CONSTRAINT `sw_tipo_educacion_ibfk_1` FOREIGN KEY (`id_periodo_lectivo`) REFERENCES `sw_periodo_lectivo` (`id_periodo_lectivo`);

--
-- Filtros para la tabla `sw_usuario_perfil`
--
ALTER TABLE `sw_usuario_perfil`
  ADD CONSTRAINT `sw_usuario_perfil_ibfk_1` FOREIGN KEY (`id_usuario`) REFERENCES `sw_usuario` (`id_usuario`) ON UPDATE CASCADE,
  ADD CONSTRAINT `sw_usuario_perfil_ibfk_2` FOREIGN KEY (`id_perfil`) REFERENCES `sw_perfil` (`id_perfil`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `sw_valor_mes`
--
ALTER TABLE `sw_valor_mes`
  ADD CONSTRAINT `sw_valor_mes_ibfk_1` FOREIGN KEY (`id_periodo_lectivo`) REFERENCES `sw_periodo_lectivo` (`id_periodo_lectivo`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
